<?php

include_once(dirname(__FILE__) . "/includes/global_deploy_config.php");
include_once(dirname(__FILE__) . "/includes/common.inc.php");
include_once(dirname(__FILE__) . "/includes/auth.inc.php");
include_once(dirname(__FILE__) . "/database/users.php");

include_once("$folderPreTrail/config/clas/$configFolderName/db_config.php");

if(isset($_POST["request"]))
{
	//=======================Connect to OVAL and Wordpress databases==================================
	$ovaldb = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database);

	if(mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: (" . $ovaldb->connect_errno . ") " . $ovaldb->connect_error;
	}

	$wpdb = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database_wp);

	if(mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: (" . $wpdb->connect_errno . ") " . $wpdb->connect_error;
	}
	//================================================================================================

	$request = $_POST["request"];
	
	if($request == "checkClassUsers")
	{
		$classChosen = $_POST["classChosen"];
		
		$checkUserType = $_POST["checkUserType"];
		
		$users = getUsers($classChosen, $checkUserType);
		
		if(mysqli_num_rows($users) > 0)
		{
			$response = '<span value="">Select user(s):</span>
					<ul>
						<li><input type="checkbox" id="selectall"/>Select/Deselect all</li>';
						while($user = $users->fetch_assoc()):
							$response .= '<li><input class="user_checkbox" type="checkbox" name="users[]" value="' . $user["ID"] .'">'. $user["user_nicename"] .'</li>';
						endwhile;
			$response .=	'</ul>
							<input type="submit" value="Submit"/>';
							
			echo $response;
		}
		else
		{
			if($checkUserType == "addUsersToClass")
				echo "There are no users that can be added.";
			else if($checkUserType == "removeUsersFromClass")
				echo "There are no users that can be removed.";
		}
	}	
	elseif($request == "addUsersToClass")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			//$comma_separated = implode(",", $users);
			
			if(addUsersToClass($users, $chosenClass))
			{
				echo "Users Added to ".$chosenClass;
			}
			else
			{
				echo "Error adding users to ".$chosenClass;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "removeUsersFromClass")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			//$comma_separated = implode(",", $users);
			
			if(removeUsersFromClass($users, $chosenClass))
			{
				echo "Users Removed from ".$chosenClass;
			}
			else
			{
				echo "Error removing users from ".$chosenClass;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "checkGroups")
	{
		$checkGroupType = $_POST["checkGroupType"];
		$chosenClass = $_POST["chosenClass"];
		if(isset($_POST["chosenGroup"]))
			$chosenGroup = $_POST["chosenGroup"];
		
		if($checkGroupType == "addGroup")
		{
			echo '<span>Group Name:</span>
			<input type="text" name="groupName"/>
			<input type="submit" value="Submit"/>';
		}
		elseif($checkGroupType == "removeGroup" && !isset($_POST["chosenGroup"]))
		{
			$groups = getGroups($ovaldb, $chosenClass);
			
			$response = '<select name="chosenGroup" id="selectGroup">
							<option value="" selected="selected"></option>';
			
			while($group = $groups->fetch_assoc()):
				$response .= '<option value="' . $group["id"] . '">' . $group["name"] . '</option>';
			endwhile;
			$response .= '</select>';
			$response .= '<input type="submit" value="Remove"/>';
			
			echo $response;
		}
		else
		{
			if(!isset($_POST["chosenGroup"]))
			{
				$groups = getGroups($ovaldb, $chosenClass);
			
				$response .= '<select name="chosenGroup" id="selectGroup">
								<option value="" selected="selected"></option>';
				
				while($group = $groups->fetch_assoc()):
					$response .= '<option value="' . $group["id"] . '">' . $group["name"] . '</option>';
				endwhile;
				$response .= '</select>';
				
				echo $response;
			}
			else
			{
				if($checkGroupType == "addOwnersToGroup" || $checkGroupType == "removeOwnersFromGroup")
				{
					$users = getGroupOwners($chosenGroup, $chosenClass, $checkGroupType);
				}
				elseif($checkGroupType == "addUsersToGroup" || $checkGroupType == "removeUsersFromGroup")
				{
					$users = getGroupUsers($chosenGroup, $chosenClass, $checkGroupType);
				}
				
				if(mysqli_num_rows($users) > 0)
				{
					$response .= '</br></br><span value="">Select user(s):</span>
							<ul>
								<li><input type="checkbox" id="selectallGroupMembers"/>Select/Deselect all</li>';
								while($user = $users->fetch_assoc()):
									$response .= '<li><input class="member_checkbox" type="checkbox" name="users[]" value="' . $user["ID"] .'">'. $user["user_nicename"] .'</li>';
								endwhile;
					$response .=	'</ul>
									<input type="submit" value="Submit"/>';
									
					echo $response;
				}
				else
				{
					if($checkGroupType == "addUsersToGroup" || $checkGroupType == "addOwnersToGroup")
						echo "There are no users that can be added.";
					else if($checkGroupType == "removeUsersFromGroup" || $checkGroupType == "removeOwnersFromGroup")
						echo "There are no users that can be removed.";
				}
			}
		}
	}
	elseif($request == "addGroup")
	{
		$chosenClass = $_POST["chosenClass"];
		
		if(isset($_POST["groupName"]))
		{
			$groupName = $_POST["groupName"];
			
			$result = addGroup($chosenClass, $groupName);
			
			if($result)
				echo "<span>Group added Successfully!</span>";
		}
	}
	elseif($request == "removeGroup")
	{		
		if(isset($_POST["chosenClass"]))
		{
			$chosenClass = $_POST["chosenClass"];
			$chosenGroup = $_POST["chosenGroup"];
			
			$result = removeGroup($chosenClass, $chosenGroup);
			
			if($result)
				echo "<span>Group Removed Successfully. All associated users and content are removed as well.</span>";
			else
				echo "<span>Error removing Group.</span>";
		}
	}
	elseif($request == "addOwnersToGroup")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			$chosenGroup = $_POST["chosenGroup"];
			
			if(addOwnersToGroup($users, $chosenClass, $chosenGroup))
			{
				echo "Owners Added to Class: ".$chosenClass." Group: ".$chosenGroup;
			}
			else
			{
				echo "Error adding Owners to Class: ".$chosenClass." Group: ".$chosenGroup;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "removeOwnersFromGroup")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			$chosenGroup = $_POST["chosenGroup"];
			
			if(removeOwnersFromGroup($users, $chosenClass, $chosenGroup))
			{
				echo "Owners Removed from Class: ".$chosenClass." Group: ".$chosenGroup;
			}
			else
			{
				echo "Error removing Owners from Class: ".$chosenClass." Group: ".$chosenGroup;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "addUsersToGroup")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			$chosenGroup = $_POST["chosenGroup"];
			
			if(addUsersToGroup($users, $chosenClass, $chosenGroup))
			{
				echo "Members Added to Class: ".$chosenClass." Group: ".$chosenGroup;
			}
			else
			{
				echo "Error adding Members to Class: ".$chosenClass." Group: ".$chosenGroup;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "removeUsersFromGroup")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			$chosenGroup = $_POST["chosenGroup"];
			
			if(removeUsersFromGroup($users, $chosenClass, $chosenGroup))
			{
				echo "Members Removed from Class: ".$chosenClass." Group: ".$chosenGroup;
			}
			else
			{
				echo "Error removing Members from Class: ".$chosenClass." Group: ".$chosenGroup;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "checkClassInstructors")
	{
		$classChosen = $_POST["classChosen"];
		
		$checkInstructorType = $_POST["checkInstructorType"];
		
		$users = getInstructors($classChosen, $checkInstructorType);
		
		if(mysqli_num_rows($users) > 0)
		{
			$response = '<span value="">Select user(s):</span>
					<ul>
						<li><input type="checkbox" id="selectallInstructor"/>Select/Deselect all</li>';
						while($user = $users->fetch_assoc()):
							$response .= '<li><input class="instructor_checkbox" type="checkbox" name="users[]" value="' . $user["ID"] .'">'. $user["user_nicename"] .'</li>';
						endwhile;
			$response .=	'</ul>
							<input type="submit" value="Submit"/>';
							
			echo $response;
		}
		else
		{
			if($checkInstructorType == "addInstructorsToClass")
				echo "There are no users that can be added as instructor. Please add them as members of the class first.";
			else if($checkInstructorType == "removeInstructorsFromClass")
				echo "There are no instructors that can be removed.";
		}
	}
	elseif($request == "addInstructorsToClass")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			//$comma_separated = implode(",", $users);
			
			if(addInstructorsToClass($users, $chosenClass))
			{
				echo "Instructors Added to ".$chosenClass;
			}
			else
			{
				echo "Error adding instructors to ".$chosenClass;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "removeInstructorsFromClass")
	{
		if(isset($_POST["users"]))
		{
			$users = $_POST["users"];
			$chosenClass = $_POST["chosenClass"];
			//$comma_separated = implode(",", $users);
			
			if(removeInstructorsFromClass($users, $chosenClass))
			{
				echo "Instructors Removed from ".$chosenClass;
			}
			else
			{
				echo "Error removing instructors from ".$chosenClass;
			}
		}
		else
		{
			echo "No user selected.";
		}
	}
	elseif($request == "checkClass")
	{
		$checkClassType = $_POST["checkClassType"];
		
		if($checkClassType == "addClass")
		{
			echo '<span>Class Name:</span>
			<input type="text" name="className"/>
			<input type="submit" value="Submit"/>';
		}
		elseif($checkClassType == "removeClass")
		{
			$classes = getClasses($ovaldb);
			
			$response = '<select name="chosenClass" id="selectClass">';
			
			while($class = $classes->fetch_assoc()):
				$response .= '<option value="' . $class["id"] . '">' . $class["name"] . '</option>';
			endwhile;
			$response .= '</select><input type="submit" value="Remove"/>';
			
			echo $response;
		}
	}
	elseif($request == "addClass")
	{
		if(isset($_POST["className"]))
		{
			$className = $_POST["className"];
			
			$result = addClass($className);
			
			if($result)
				echo "<span>Class added Successfully!</span>";
		}
	}
	elseif($request == "removeClass")
	{
		if(isset($_POST["chosenClass"]))
		{
			$chosenClass = $_POST["chosenClass"];
			
			$result = removeClass($chosenClass);
			
			if($result)
				echo "<span>Class Removed Successfully. All associated users and content are removed as well.</span>";
			else
				echo "<span>Error removing class.</span>";
		}
	}
	else
	{
		echo "Request Error!";
	}
}
else
{
	echo "Unauthorised!";
}

//getClasses will retrieve all the existing classes in the OVAL database
function getClasses($dbConn)
{	
    //list class that have already been created
    $query  = "SELECT * FROM class ORDER BY name ASC";
    $result = mysqli_query($dbConn, $query);

    return $result;
}

function addClass($className)
{
	global $ovaldb;
	
	if(preg_match('/[^0-9A-Za-z\_\-]/', $className))
	{
		echo "<span>Invalid Class Name, please try again. Only Alphanumeric, '_' and '-' characters are allowed</span></br>";
		return false;
	}
	
	$query = "SELECT * FROM class WHERE UPPER(name) = UPPER('$className')";
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_num_rows($result) > 0)
	{
		echo "<span>Class Name already exist, please use a different name.</span></br>";
		return false;
	}
	else
	{
		$query = "INSERT INTO class (name) VALUES ('$className')";
		
		$result = mysqli_query($ovaldb, $query);
		
		if($result)			
			return true;
		else
		{
			echo "<span>Error inserting into DB.</span></br>";
			return false;
		}
	}		
}

function removeClass($chosenClass)
{
	global $ovaldb;
	
	/*To-Do - Delete all Associated Groups, Group Members, Group Owners from those 3 tables.
	$query = "DELETE gm.*, go.*, g.* 
				FROM groups g
				LEFT JOIN groupOwners go
				ON go.group_id = g.id
				LEFT JOIN groupMembers gm
				ON gm.group_id = g.id
				WHERE g.class_id = $chosenClass";
	
	$result = mysqli_query($ovaldb, $query); */
	
	/*To-Do - Instructors and users, Keep or Delete?
	$query = "DELETE ilist.*, list.*
				FROM classEnrollmentLists list
				LEFT JOIN classInstructorsAndTAs ilist
				ON list.class_id = ilist.class_id
				WHERE list.class_id = $chosenClass";
	
	$result = mysqli_query($ovaldb, $query);*/
	
	$query = "DELETE FROM class WHERE id = $chosenClass";
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_affected_rows($ovaldb) > 0)
		return true;
	else
		return false;
}

function getGroups($dbConn, $classChosen)
{
	//list class that have already been created
    $query  = "SELECT * FROM groups WHERE class_id = $classChosen ORDER BY name ASC";
    $result = mysqli_query($dbConn, $query);

    return $result;
}

function getGroupOwners($chosenGroup, $classChosen, $checkGroupType)
{
	global $wpdb;
				
	$query = "SELECT * FROM 
				covaa21_covaawp.wp_users wp
			  JOIN
				covaa21_CLAS.classEnrollmentLists list
			  ON list.user_id = wp.ID
			  AND list.class_id IN ($classChosen)
			  JOIN
				covaa21_CLAS.groups g
			  ON list.class_id = g.class_id
			  AND g.id IN ($chosenGroup)
			  JOIN
				covaa21_CLAS.groupMembers gm
			  ON gm.group_id = g.id
			  AND gm.user_id = wp.ID
			  JOIN
				covaa21_CLAS.groupOwners go
			  ON go.group_id = gm.group_id
			  AND go.user_id = wp.ID
			  ORDER BY wp.ID";
			  
	$result = mysqli_query($wpdb, $query);
	
	if($checkGroupType == "removeOwnersFromGroup")
	{
		return $result;
	}
	
	if(mysqli_num_rows($result) > 0)
	{
		$users = array();
		
		 while ($row = $result->fetch_assoc()) {
			array_push($users, $row["ID"]);
		}
		
		//print_r($users);
		
		$users = implode(",", $users);
		
		//print_r($users);
	
		$query = "SELECT * FROM 
					covaa21_covaawp.wp_users wp
				  JOIN
					covaa21_CLAS.classEnrollmentLists list
				  ON list.user_id = wp.ID
				  AND list.class_id IN ($classChosen)
				  JOIN
					covaa21_CLAS.groups g
				  ON list.class_id = g.class_id
				  AND g.id IN ($chosenGroup)
				  JOIN
					covaa21_CLAS.groupMembers gm
				  ON gm.group_id = g.id
				  AND gm.user_id = wp.ID
				  WHERE wp.ID NOT IN ($users)
				  ORDER BY wp.ID";
	}
	else
	{
		$query = "SELECT
					* FROM covaa21_covaawp.wp_users wp
				  JOIN
					covaa21_CLAS.classEnrollmentLists list
				  ON list.user_id = wp.ID
				  AND list.class_id IN ($classChosen)
				  JOIN
					covaa21_CLAS.groups g
				  ON list.class_id = g.class_id
				  AND g.id IN ($chosenGroup)
				  JOIN
					covaa21_CLAS.groupMembers gm
				  ON gm.group_id = g.id
				  AND gm.user_id = wp.ID";
	}	
	
	$result = mysqli_query($wpdb, $query); 
	
    return $result;
}

function getGroupUsers($chosenGroup, $classChosen, $checkGroupType)
{
	global $wpdb;
				
	$query = "SELECT * FROM 
				covaa21_covaawp.wp_users wp
			  JOIN
				covaa21_CLAS.classEnrollmentLists list
			  ON list.user_id = wp.ID
			  AND list.class_id IN ($classChosen)
			  JOIN
				covaa21_CLAS.groups g
			  ON list.class_id = g.class_id
			  AND g.id IN ($chosenGroup)
			  JOIN
				covaa21_CLAS.groupMembers gm
			  ON gm.group_id = g.id
			  AND gm.user_id = wp.ID
			  ORDER BY wp.ID";
			  
	$result = mysqli_query($wpdb, $query);
	
	if($checkGroupType == "removeUsersFromGroup")
	{
		return $result;
	}
	
	if(mysqli_num_rows($result) > 0)
	{
		$users = array();
		
		 while ($row = $result->fetch_assoc()) {
			array_push($users, $row["ID"]);
		}
		
		//print_r($users);
		
		$users = implode(",", $users);
		
		//print_r($users);
	
		$query = "SELECT * FROM 
					covaa21_covaawp.wp_users wp
				  JOIN
					covaa21_CLAS.classEnrollmentLists list
				  ON list.user_id = wp.ID
				  AND list.class_id IN ($classChosen)
				  JOIN
					covaa21_CLAS.groups g
				  ON list.class_id = g.class_id
				  AND g.id IN ($chosenGroup)
				  WHERE wp.ID NOT IN ($users)
				  ORDER BY wp.ID";
				  
		//echo $query;
	}
	else
	{
		$query = "SELECT
					* FROM covaa21_covaawp.wp_users wp
				  JOIN
					covaa21_CLAS.classEnrollmentLists list
				  ON list.user_id = wp.ID
				  AND list.class_id IN ($classChosen)";
	}	
	
	$result = mysqli_query($wpdb, $query); 
	
    return $result;
}

function addGroup($chosenClass, $groupName)
{
	global $ovaldb;
	
	if(preg_match('/[^0-9A-Za-z\_\-]/', $groupName))
	{
		echo "<span>Invalid Group Name, please try again. Only Alphanumeric, '_' and '-' characters are allowed</span></br>";
		return false;
	}
	
	$query = "SELECT * FROM groups WHERE UPPER(name) = UPPER('$groupName')";
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_num_rows($result) > 0)
	{
		echo "<span>Group Name already exist, please use a different name.</span></br>";
		return false;
	}
	else
	{
		$query = "INSERT INTO groups (name, class_id) VALUES ('$groupName', $chosenClass)";
		
		$result = mysqli_query($ovaldb, $query);
		
		if($result)			
			return true;
		else
		{
			echo "<span>Error inserting into DB.</span></br>";
			return false;
		}
	}
}

function removeGroup($chosenClass, $chosenGroup)
{
	global $ovaldb;
	
	//Delete all Associated Group Members, Group Owners from the other 2 tables.
	$query = "DELETE FROM groupMembers
				WHERE group_id = $chosenGroup;
			  DELETE FROM groupOwners
				WHERE group_id = $chosenGroup;";
	
	/* $query = "DELETE gm, go, g 
				FROM groups g
				INNER JOIN groupOwners go
				ON go.group_id = g.id
				INNER JOIN groupMembers gm
				ON gm.group_id = g.id
				WHERE g.class_id = $chosenClass
				AND g.id = $chosenGroup"; */
	
	//echo $query;
	
	$result = mysqli_multi_query($ovaldb, $query);
	
	$query = "DELETE FROM groups
				WHERE id = $chosenGroup
				AND class_id = $chosenClass;";
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_affected_rows($ovaldb) > 0)
		return true;
	else
		return false;
}

function addOwnersToGroup($users, $chosenClass, $chosenGroup)
{
	global $ovaldb;
	
	$query = "INSERT INTO groupOwners ( group_id, user_id ) VALUES ";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= "($chosenGroup," . $users[$i] . ")";
		
		if($i+1<sizeof($users))
			$query .= ",";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	return $result;
}

function removeOwnersFromGroup($users, $chosenClass, $chosenGroup)
{
	global $ovaldb;
	
	$query = "DELETE FROM groupOwners
				WHERE
					group_id = $chosenGroup
				AND
					user_id IN (";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= $users[$i];
		
		if($i+1<sizeof($users))
			$query .= ",";
		else
			$query .= ")";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_affected_rows($ovaldb) > 0)
		return true;
	else
		return false;
}

function addUsersToGroup($users, $chosenClass, $chosenGroup)
{
	global $ovaldb;
	
	$query = "INSERT INTO groupMembers ( group_id, user_id ) VALUES ";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= "($chosenGroup," . $users[$i] . ")";
		
		if($i+1<sizeof($users))
			$query .= ",";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	return $result;
}

function removeUsersFromGroup($users, $chosenClass, $chosenGroup)
{
	global $ovaldb;
	
	$query = "DELETE FROM groupMembers
				WHERE
					group_id = $chosenGroup
				AND
					user_id IN (";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= $users[$i];
		
		if($i+1<sizeof($users))
			$query .= ",";
		else
			$query .= ")";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_affected_rows($ovaldb) > 0)
		return true;
	else
		return false;
}

function getUsers($classChosen, $checkUserType)
{
	global $wpdb;
				
	$query = "SELECT 
				* 
			  FROM 
				covaa21_covaawp.wp_users wp
			  JOIN
				covaa21_CLAS.classEnrollmentLists list
			  ON list.user_id = wp.ID
			  and list.class_id IN ($classChosen)
			  ORDER BY wp.ID";
			  
	$result = mysqli_query($wpdb, $query);
	
	if($checkUserType == "removeUsersFromClass")
	{
		return $result;
	}
	
	if(mysqli_num_rows($result) > 0)
	{
		$users = array();
		
		 while ($row = $result->fetch_assoc()) {
			array_push($users, $row["ID"]);
		}
		
		//print_r($users);
		
		$users = implode(",", $users);
		
		//print_r($users);
	
		$query = "SELECT
					* FROM covaa21_covaawp.wp_users wp
				  WHERE wp.ID NOT IN ($users)";
	}
	else
	{
		$query = "SELECT
					* FROM covaa21_covaawp.wp_users wp";
	}	
	
	$result = mysqli_query($wpdb, $query); 
	
    return $result;
}

function addUsersToClass($users, $chosenClass)
{
	global $ovaldb;
	
	$query = "INSERT INTO classEnrollmentLists ( class_id, user_id ) VALUES ";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= "($chosenClass," . $users[$i] . ")";
		
		if($i+1<sizeof($users))
			$query .= ",";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	return $result;
}

function removeUsersFromClass($users, $chosenClass)
{
	global $ovaldb;
	
	$query = "DELETE FROM classEnrollmentLists
				WHERE
					class_id = $chosenClass
				AND
					user_id IN (";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= $users[$i];
		
		if($i+1<sizeof($users))
			$query .= ",";
		else
			$query .= ")";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_affected_rows($ovaldb) > 0)
		return true;
	else
		return false;
}

function getInstructors($classChosen, $checkInstructorType)
{
	global $wpdb;
				
	$query = "SELECT 
				* 
			  FROM 
				covaa21_covaawp.wp_users wp
			  JOIN
				covaa21_CLAS.classInstructorsAndTAs list
			  ON list.user_id = wp.ID
			  and list.class_id IN ($classChosen)
			  ORDER BY wp.ID";
	
	//echo $query;
	
	$result = mysqli_query($wpdb, $query);
	
	if($checkInstructorType == "removeInstructorsFromClass")
	{
		return $result;
	}
	
	if(mysqli_num_rows($result) > 0)
	{
		$users = array();
		
		 while ($row = $result->fetch_assoc()) {
			array_push($users, $row["ID"]);
		}
		
		//print_r($users);
		
		$users = implode(",", $users);
		
		//print_r($users);
	
		$query = "SELECT 
				* 
			  FROM 
				covaa21_covaawp.wp_users wp
			  JOIN
				covaa21_CLAS.classEnrollmentLists list
			  ON list.user_id = wp.ID
			  AND list.user_id NOT IN ($users)
			  AND list.class_id IN ($classChosen)
			  ORDER BY wp.ID";
		  //echo $query;
		  
		  $result = mysqli_query($wpdb, $query);
	}
	else
	{
		$query = "SELECT 
				* 
			  FROM 
				covaa21_covaawp.wp_users wp
			  JOIN
				covaa21_CLAS.classEnrollmentLists list
			  ON list.user_id = wp.ID
			  AND list.class_id IN ($classChosen)
			  ORDER BY wp.ID";
		  //echo $query;
		  
		  $result = mysqli_query($wpdb, $query);
	}
	
    return $result;
}

function addInstructorsToClass($users, $chosenClass)
{
	global $ovaldb;
	
	$query = "INSERT INTO classInstructorsAndTAs ( class_id, user_id, is_instructor ) VALUES ";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= "($chosenClass," . $users[$i] . ", 1)";
		
		if($i+1<sizeof($users))
			$query .= ",";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	return $result;
}

function removeInstructorsFromClass($users, $chosenClass)
{
	global $ovaldb;
	
	$query = "DELETE FROM classInstructorsAndTAs
				WHERE
					class_id = $chosenClass
				AND
					user_id IN (";
	
	for($i=0; $i<sizeof($users); $i++)
	{
		$query .= $users[$i];
		
		if($i+1<sizeof($users))
			$query .= ",";
		else
			$query .= ")";
	}
	
	//echo $query;
	
	$result = mysqli_query($ovaldb, $query);
	
	if(mysqli_affected_rows($ovaldb) > 0)
		return true;
	else
		return false;
}
?>