<?php
/** Template Name: Survey Page **/


require_once(dirname(__FILE__) . "/includes/global_deploy_config.php");

	require_once(dirname(__FILE__) . '/includes/kaltura/kaltura_functions.php');

	require_once(dirname(__FILE__) . "/includes/common.inc.php");

	require_once(dirname(__FILE__) . "/includes/auth.inc.php");

	require_once(dirname(__FILE__) . '/database/users.php');

	require_once(dirname(__FILE__) . '/database/media.php');
	
	//require_once(dirname(__FILE__) . '/ga.php');

	error_reporting(1); //Simon added
	
	startSession();
	
	//Check if user is teacher, check subjects taught and classes associated
	$isAdmin  = isAdmin($_SESSION['role']);
	$userName = $_SESSION['name'];
	$userID   = $_SESSION['user_id'];

?>

<?php
global $mysqlUser, $mysqlPassword, $database;
$conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);
	

if (!$conn) {
die('Could not connect: ' . mysql_error());
}
else{
//echo 'Connected successfully';
}

$db_selected = mysql_select_db($database, $conn);
mysql_set_charset("utf8",$conn);

$query = "SELECT * FROM surveyData WHERE user_login = '$userID' AND isCompleted=0 AND SurveyType='Post'";
$result = mysql_query($query, $conn);

if(mysql_num_rows($result) > 0)
{
    while ($row = mysql_fetch_assoc($result)) {
			//var_dump($row);
			$values = $row["result"];
		}	
}else{
	$values = null;
}
	
$query1 = "SELECT * FROM surveyData WHERE user_login = '$userID' AND isCompleted=1 AND SurveyType='Post'";
$result1 = mysql_query($query1, $conn);
if(mysql_num_rows($result1)>0){
	$complete =1;
}else{
	$complete =0;
}
?>

<head>
<title>CoVAA Student Questionnaire</title>
	<!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<![endif]-->
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!--Load the AJAX API-->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="survey-jquery/survey.jquery.min.js"></script>
	<script>
	var values = <?php echo json_encode($values); ?>;
	var complete = <?php echo $complete; ?>;
	values = JSON.parse(values);

	</script>

	<style>
		/*Top menu bar css*/
			.bg-light {
				background-color: #26418F !important; /*#2962ff #3f51b5*/
			    color: white;
			    border-radius: 0px;
			}

			#nav-container {
				padding-right: 0px;
			}

			#page-title {
			    color: #83C8F6;
			    font-size: 16px !important;
			    line-height: 1.5;
			    padding-top: 5px;
			}

			.navbar-nav a {
				color: white;
			}

			.navbar-light .navbar-nav .active>.nav-link,
			.navbar-light .navbar-nav .nav-link {
			    color: white;
			}

			.nav>li>a {
				padding: 7px 7px;
			}

			.nav>li>a:hover {
			    text-decoration: underline;
			    color: white;
			    background-color: transparent;
			}

			#bs-example-navbar-collapse-1 > ul > li > a {
				font-size: 14px;
			   /* font-weight: 400;
			    line-height: 1.5;*/
			}

			.navbar-nav > .active {
			    background-color: grey;
			}

			.navbar, .well {
				/*margin-top: -4px;
				margin-bottom: 0px;*/
				/*padding: .5rem 1rem;*/
				height: 35px;
				font-size: 14px;
				min-height: 20px;
			    padding: 0px 10px;
			    border: 0px;
			}

			.navbar-brand img {
				margin: -12px 0 0 -14px;
				width: 80px;
			}

			.navbar-right {
			    /*margin-top: 4px;*/
			}
			/*End of Top menu bar css*/

		body {

			font-family: 'Roboto', sans-serif;
			height: 100%;
			background-color: #F4F5F9;
			position: relative;
		}
		#loadingAjax{
			margin: 0 auto;
			/*padding: 10px 5px 10px 5px;*/
			background-color: #F4F5F9;
			border-radius: 4px;
			min-width: 800px;
		}
		#surveyResult {
			margin: 0px auto;
			padding: 10px 5px 10px 5px;
			background-color: #FFFFFF;
			width: 800px;
			min-height: 100%;
			overflow: hidden;
			display: block;
			position: relative;
			padding-bottom: 100px;
		}
		#surveyContainer {
			margin: 0px auto;
			padding: 10px 5px 10px 5px;
			background-color: #F4F5F9;
			border-radius: 4px;
			width: 1300px;
			min-height: 100%;
			overflow: hidden;
			display: block;
			position: relative;
			padding-bottom: 100px;
		}
		/* .progress {
			width: 90% !important;
		}
		.progress-bar {
			width: 10% !important;
		} */
		h4, h2, h5{
			margin: 0;
		}
		h5{
			font-weight: bold;
			font-size: 16px;
			margin-top: 5px;
			margin-bottom: 5px;
		}
		h2{
			font-size:24px;
		}
		h3{
			font-weight: normal;
		}
		.panel-footer {
			text-align: center;
		}
		input[type="radio"] {
			transform: scale(2);
			margin: 4px 0 0 -4px;
		}
		input[type="button"],input[type=submit] {
			    font-size: 12px;
				font-weight: bold;
				line-height: 2em;
				border: none;
				min-width: 100px;
				cursor: pointer;
				padding: 5px;
				border-radius: 3px;
				color: white;
				background-color: #26418F;
		}
		.btn{
			font-size: 24px;
			background-color: #26418F;
			color: white;
		}
		.btn:hover{
			color: #FFF;
			text-decoration: underline;
		}
		thead {
			color: #000000;
			font-size: 11px;
		}
		tbody td:first-child {
			text-align: left;
		}
		tbody td, thead th {
			text-align: center;
		}
		tbody td:nth-child(2), thead th:nth-child(2) {
			width: 5%;
			background-color: #c523ff;
		}
		tbody td:nth-child(3), thead th:nth-child(3) {
			width: 5%;
			background-color: #da73ff;
		}
		tbody td:nth-child(4), thead th:nth-child(4) {
			width: 5%;
			background-color: #efc3ff;
		}
		tbody td:nth-child(5), thead th:nth-child(5) {
			width: 5%;
		}
		tbody td:nth-child(6), thead th:nth-child(6) {
			width: 5%;
			background-color: #d2ffb5;
		}
		tbody td:nth-child(7), thead th:nth-child(7) {
			width: 6%;
			background-color: #beff95;
		}
		tbody td:nth-child(8), thead th:nth-child(8) {
			width: 6%;
			background-color: #a1ff66;
		}
		
		/*For the 5 options page*/
		#sq_128 tbody td:nth-child(2), #sq_128 thead th:nth-child(2) {
			width: 5%;
			background-color: #c523ff;
		}
		#sq_128 tbody td:nth-child(3), #sq_128 thead th:nth-child(3) {
			width: 5%;
			background-color: #da73ff;
		}
		#sq_128 tbody td:nth-child(4), #sq_128 thead th:nth-child(4) {
			width: 5%;
			background-color: transparent;
		}
		#sq_128 tbody td:nth-child(5), #sq_128 thead th:nth-child(5) {
			width: 5%;
			background-color: #beff95;
		}
		#sq_128 tbody td:nth-child(6), #sq_128 thead th:nth-child(6) {
			width: 5%;
			background-color: #a1ff66;
		}
		#sq_128 tbody td:nth-child(7), #sq_128 thead th:nth-child(7) {
			width: 5%;
			background-color: transparent;
		}
		#footer{
			font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
			text-align: center;
			font-size: 12px;
			position: relative;
			left: 0;
			bottom: 0;
			height: 100px;
			margin-top: -100px;
			width: 100%;
			
			}
			#footer > label{
				font-weight:normal;
			}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm navbar-light bg-light">
		  <div id="nav-container" class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header" style="width:390px;">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../oval-master/index.php"><img src="./icons/covva_small.png" alt="CoVAA"></a>
            	<div id="page-title">Collaborative Video Annotation & Analytics</div>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right"> 
					<li><a href="../oval-master/index.php">Watch Videos</a></li>
					<li class="active"><a class="nav-link" href="#" >Survey<span class="sr-only">(current)</span></a></li>
					<?php if ($isAdmin == true): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="video_management.php?cid=<?php echo "$CID"?>&gid=<?php echo "$GID"?>">Upload</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../oval-master/teacher_dashboard.php">Teacher Dashboard</a>
                        </li>
                        <!--  <a href="video_management.php" >admin</a> -->
                    <?php endif; ?>
					<li><a href="../oval-master/student_dashboard_test.php">Learning Dashboard</a></li> 
					<li><a href="../oval-master/logout">Logout</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	<div id="loadingAjax"></div>
	<div id="surveyResult" class=""></div>
	<div id="surveyContainer" class="container-fluid"></div>
	<div id="footer">
		<label>
         CoVAA beta version. &copy; 2017 Dr. Jennifer Pei-Ling Tan. National Institute of Education. NRF EduLab Grant NRF2015-EDU001-IHL09.<br> 
		<span style="font-size: 11;">Powered by the OVAL/CLAS research initiatives at the Centre for Change and Complexity in Learning (C3L), University of South Australia.</span>
       </label>
		</div>
</body>

<script>

	var dashboard_type = 2;
	var user_login = <?php echo json_encode($user_login); ?>;
	var admin = <?php echo json_encode($isAdmin); ?>;
	
	if (admin==true)
	{
		dashboard_type = 2;
	}
	$(document).ready(function(){
		if (complete==0|| (admin==true)){
			//Hide submitting process initially
			$( "#surveyResult" ).hide();
			$("#loadingAjax").hide();
			// Enable navigation prompt
		window.onbeforeunload = function() {
			return true;
		};
		}else{
			$( "#surveyContainer" ).hide();
			$( "#surveyResult" ).html( "<center><h3>Survey Completed! Let's <a href='https://covaa21.com/oval-master/index.php' class='btn' role='button'>Watch Videos</a> Now!</h3><h4 style='font-weight: normal;'>You can view your <a href='https://covaa21.com/oval-master/student_dashboard_test.php' class='btn' role='button' style='font-size: 18px;font-weight: normal;'>Learning Dashboard</a> anytime</h4></center><br>" );
				$( "#surveyResult" ).show();
			$("#loadingAjax").hide();
			// Remove navigation prompt
			window.onbeforeunload = null;
		}
		//Bind ajax start and stop functions
		$(document).ajaxStart(function() {
		  $("#loadingAjax").show();
		});

		$(document).ajaxStop(function() {
		  $("#loadingAjax").hide();
		});
		
		//testDataToServer();
		
		
		
	});

	function testDataToServer() {
		
		var resultAsArray = {"studentName":"Simon Yang","studentClass":"nas-3i1","registerNo":17,
		"surveyQuestions":{"PG1":"1","PG2":"2","PG3":"3","PG6":"4","PG7":"5","PG8":"6",
		"LG1":"1","LG2":"2","LG3":"3","LG4":"4","LG5":"5","LG6":"6","LG7":"7","LG8":"6",
		"LA1":"1","LA2":"2","LA3":"3","PA1":"4","PA2":"5","PA3":"6", 
		"SE1":"1","SE2":"2","SE3":"3","SE4":"4","SE5":"5",
		"TV1":"1","TV2":"2","TV3":"3","TV4":"4","TV5":"5",
		"Comm1":"1","Comm2":"2","Alien1":"3","Comm3":"4","Trust1":"5","Comm5":"6","Alien3":"1","Trust2":"2","Trust3":"3","Alien4":"4","Trust4":"5","Trust5":"6",
		"TAS1":"1","TAS2":"2","TAS3":"3","TAS4":"4","TCS1":"5","TCS2":"6","TCS3":"7","TCS4":"6",
		"BENG1":"1","BENG2":"2","CENG9":"3","BENG4":"4","BENG3":"5","EENG5":"6","CENG10":"1","EENG6":"2","CENG11":"3","EENG7":"4","CENG12":"5","ENGE8":"6",
		"GENG2":"2","GENG3":"3","GENG4":"4",
		"CPcr1":"1","CPcr2":"2","CPcr3":"3","CPcr4":"4","CPcr5":"5","CPcr6":"6",
		"CPcu2":"1","CPcu3":"2","CPcu4":"3","CPcu5":"4","CPcu6":"5","CPcu7":"6","CPcu8":"7",
		"CPct1":"1","CPct2":"2","CPct5":"3",
		"ASC1":"1","ASC2":"2","ASC3":"3","ASC4":"4","ASC5":"5","ASC6":"1","ASC7":"2","ASC8":"3","ASC9":"4","ASC10":"5",
		"CRA1":"1","CRA2":"2","CRA3":"3","CRA4":"4","CRA5":"5","CRA6":"6","CRA7":"7","CRA8":"1","CRA9":"2","CRA10":"3","CRA11":"4","CRA12":"5","CRA13":"6",
		"DM1":"1","DS1":"2","SM1":"3","SS1":"4","DM2":"5","DS2":"6","SM2":"7","SS2":"1","DM3":"2","DS3":"3","SM3":"4","SS3":"5","DM4":"6","DS4":"7","SM4":"1","SS4":"2","DM5":"3","DS5":"4","SM5":"5","SS5":"6",
		"SDLr1":"1","SDLr2":"2","SDLr3":"3","SDLr4":"4","SDLr5":"5",
		"SDLg6":"1","SDLg7":"2","SDLg8":"3","SDLg9":"4",
		"SGP1":"2","SGP2":"1","SGP3":"5","SGP4":"2","SGP5":"6",
		"SGR1":"6","SGR2":"2","SGR3":"1","SGR4":"5"
		}};
		
		// Send the option selected using post
		$.ajax({
		  url: "survey_ajax_handler.php",
		  data: {
					request: "insertSurveyEntry",
					entryArray: resultAsArray,
					trialNo: 3,
					surveyType: "Pre"
				},
		  type: "POST",
		  dataType : "text",
		  beforeSend: function() {
				$( "#surveyResult" ).hide();
				$( "#surveyContainer" ).hide();
		  },
		  success: function( data ) {
			console.log("Response: " + data);
			if(data == 1)
			{
				$( "#surveyResult" ).html( "<h3>Survey submitted successfully! Thank you for your time.<br>"+"Now <a href='https://covaa21.com/oval-master/index.php'>let's watch videos!</a></h3>" );
				$( "#surveyResult" ).show();
				// Remove navigation prompt
				window.onbeforeunload = null;
				
				//$(location).attr('href', 'https://covaa21.com/oval-master/index.php')
				//window.location.replace("https://covaa21.com/oval-master/index.php");
			}
			else
			{
				$( "#surveyResult" ).html( "<h3>Sorry, there was a problem. Please try submitting again or notify your teacher. (Insertion Error)</h3><br><button onclick=sendDataToServer(survey);>Retry</button>" );
				$( "#surveyResult" ).show();
				//$( "#surveyContainer" ).show();
			}
		  },
		  error: function( xhr, status, errorThrown ) {
				//$( "#surveyResult" ).html( errorThrown );
				$( "#surveyResult" ).html( "<h3>Sorry, there was a problem. Please try submitting again or notify your teacher. (Network Error)</h3><br><button onclick=sendDataToServer(survey);>Retry</button>" );
				$( "#surveyResult" ).show();
				//alert( "Sorry, there was a problem. Please try submitting again or notify your teacher." );
				//$( "#surveyContainer" ).show();
				console.log( "Error: " + errorThrown );
				console.log( "Status: " + status );
				console.dir( xhr );
			}
		});
	}

	function sendDataToServer(survey) {
	  /* var resultAsString = JSON.stringify(survey.data);
	  alert(resultAsString); //send Ajax request to your web server. */
	  
	  var resultAsArray = survey.data;
	  
	  var rawData = JSON.stringify(resultAsArray, null, 4);
	  
	  var type = "Post";
	  var complete = 1;
	  //var type = "Post";
	  
		// Send the option selected using post
		$.ajax({
		  url: "survey_ajax_handler.php",
		  data: {
					request: "insertSurveyEntry",
					entryArray: resultAsArray,
					trialNo: 3,
					surveyType: type,
					dashboard: dashboard_type,
					username: user_login,
					complete: 1
				},
		  type: "POST",
		  dataType : "text",
		  beforeSend: function() {
				$( "#surveyResult" ).hide();
				$( "#surveyContainer" ).hide();
		  },
		  success: function( data ) {
			console.log("Response: " + data);
			if(data == 1)
			{
				$( "#surveyResult" ).html( "<center><h3>Survey Completed! Let's <a href='https://covaa21.com/oval-master/index.php' class='btn' role='button'>Watch Videos</a> Now!</h3><h4 style='font-weight: normal;'>You can view your <a href='https://covaa21.com/oval-master/student_dashboard_test.php' class='btn' role='button' style='font-size: 18px;font-weight: normal;'>Learning Dashboard</a> anytime</h4></center><br>" );
				$( "#surveyResult" ).show();
				// Remove navigation prompt
				window.onbeforeunload = null;
				
				//window.location.replace("https://covaa21.com/oval-master/index.php");
			}
			else
			{
				$( "#surveyResult" ).html( "<h3>Sorry, there was a problem. Please try submitting again or notify your teacher. (Insertion Error)</h3><button onclick=sendDataToServer(survey);>Retry</button><br><br><h5>If the issue persists, take a screenshot of the following data for your teacher.</h5><br><br>"+rawData );
				$( "#surveyResult" ).show();
				//$( "#surveyContainer" ).show();
			}
		  },
		  error: function( xhr, status, errorThrown ) {
				//$( "#surveyResult" ).html( errorThrown );
				$( "#surveyResult" ).html( "<h3>Sorry, there was a problem. Please try submitting again or notify your teacher. (Network Error)</h3><button onclick=sendDataToServer(survey);>Retry</button><br><br><h5>If the issue persists, take a screenshot of the following data for your teacher.</h5><br><br>"+rawData );
				$( "#surveyResult" ).show();
				//alert( "Sorry, there was a problem. Please try submitting again or notify your teacher." );
				//$( "#surveyContainer" ).show();
				console.log( "Error: " + errorThrown );
				console.log( "Status: " + status );
				console.dir( xhr );
			}
		});
	}
function sendDataToServer1(survey) {
	  
	  
	  var resultAsArray = survey.data;
	  
	  
	  var type = "Post";
	  
	  // Send the option selected using post
		$.ajax({
		  url: "survey_ajax_handler.php",
		  data: {
					request: "insertSurveyEntry",
					entryArray: resultAsArray,
					trialNo: 3,
					surveyType: type,
					dashboard: dashboard_type,
					username: user_login,
					complete: 0
				},
		  type: "POST",
		  dataType : "text",
		  success: function( data ) {
			console.log("Response: " + data);
			if(data == 1)
			{
				console.log(resultAsArray);
				//$( "#surveyResult" ).html( "<h3>Survey submitted successfully. Thank you for your time.</h3>" );
				//$( "#surveyResult" ).show();
				// Remove navigation prompt
				//window.onbeforeunload = null;
				
				//window.location.replace("https://covaa21.com/oval-master/index.php");
			}
			else
			{
				//$( "#surveyResult" ).html( "<h3>Sorry, there was a problem. Please try submitting again or notify your teacher. (Insertion Error)</h3><button onclick=sendDataToServer(survey);>Retry</button><br><br><h5>If the issue persists, take a screenshot of the following data for your teacher.</h5><br><br>"+rawData );
				//$( "#surveyResult" ).show();
				//$( "#surveyContainer" ).show();
			}
		  },
		  error: function( xhr, status, errorThrown ) {
				//$( "#surveyResult" ).html( errorThrown );
				//$( "#surveyResult" ).html( "<h3>Sorry, there was a problem. Please try submitting again or notify your teacher. (Network Error)</h3><button onclick=sendDataToServer(survey);>Retry</button><br><br><h5>If the issue persists, take a screenshot of the following data for your teacher.</h5><br><br>"+rawData );
				//$( "#surveyResult" ).show();
				//alert( "Sorry, there was a problem. Please try submitting again or notify your teacher." );
				//$( "#surveyContainer" ).show();
				console.log( "Error: " + errorThrown );
				console.log( "Status: " + status );
				console.dir( xhr );
			}
		});
		
	}
	Survey.Survey.cssType = "bootstrap";
	
	//console.log("dashboard_type: "+dashboard_type);
	
	var surveyJSON = {
 "pages": [
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<div>\n<p><strong>THANK YOU</strong> for agreeing to fill in this questionnaire</p>\n<ul>\n<li>The survey is about <strong>your feelings and attitudes</strong> towards <strong>learning and English Language.</strong></li>\n<li>You should be able to complete this in <strong><u>20 MINUTES.</u></strong></li>\n<li><strong>Please answer <u>ALL</u> the questions.</strong> There is <strong><u>NO RIGHT OR WRONG</u></strong> answer. </li>\n\n<li>Choose the answer that is most <strong><u>HONEST</u></strong> and <strong><u>ACCURATE</u></strong> about yourself. Do not worry about projecting a good image.</li>\n\n<li><strong>Your answers are <u>CONFIDENTIAL</u>. Your teacher and friends will <u>NOT KNOW</u> your responses.</strong></li>\n</ul>\n</div>",
       "name": "instructions"
      },
      {
       "type": "text",
       "name": "studentName",
       "title": "Full Name",
       "isRequired": true,
       "size": "50"
      },
      {
       "type": "dropdown",
       "name": "studentClass",
       "startWithNewLine": false,
       "title": "Class",
       "isRequired": true,
       "choices": [
        {
         "value": "nas-3i1",
         "text": "3I1"
        },
        {
         "value": "nas-3r1",
         "text": "3R1"
        },
        {
         "value": "nas-3r2",
         "text": "3R2"
        },
        {
         "value": "nas-3r3",
         "text": "3R3"
        },
        {
         "value": "nas-3r4",
         "text": "3R4"
        },
        {
         "value": "nas-3r5",
         "text": "3R5"
        },
        {
         "value": "nas-3r6",
         "text": "3R6"
        },
        {
         "value": "nas-3c1",
         "text": "3C1"
        },
        {
         "value": "nas-3c2",
         "text": "3C2"
        }
       ]
      },
      {
       "type": "text",
       "name": "registerNo",
       "startWithNewLine": false,
       "title": "Register No. :",
       "isRequired": true,
       "inputType": "number",
       "placeHolder": "Eg. 17"
      }
     ],
     "name": "panel1",
     "title": "WiREAD STUDENT QUESTIONNAIRE"
    }
   ],
   "name": "userParticulars"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "page1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "PG1",
         "text": "1. I prefer to do EL tasks that I can do well rather than things that I do poorly."
        },
        {
         "value": "PG2",
         "text": "2. I am happiest when I perform EL tasks that I know I won’t make any errors on."
        },
        {
         "value": "PG3",
         "text": "3. The EL tasks I enjoy the most are the things that I do the best."
        },
        {
         "value": "PG6",
         "text": "4. I like to be fairly confident that I can successfully perform an EL task before I try it."
        },
        {
         "value": "PG7",
         "text": "5. I like to work on EL tasks that I have done well on in the past."
        },
        {
         "value": "PG8",
         "text": "6. I feel smart when I can do an EL task better than most other people."
        }
       ],
       "title": "In my ENGLISH LANGUAGE (EL) class…"
      }
     ],
     "name": "QuestionPanel",
     "title": " "
    }
   ],
   "name": "page1"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "page2"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "LG1",
         "text": "7. The opportunity to do challenging work in EL class is important to me."
        },
        {
         "value": "LG2",
         "text": "8. When I fail to complete a difficult EL task, I try harder the next time I work on it."
        },
        {
         "value": "LG3",
         "text": "9. I prefer to work on EL tasks that force me to learn new things."
        },
        {
         "value": "LG4",
         "text": "10. The opportunity to learn new things in EL class is important to me."
        },
        {
         "value": "LG5",
         "text": "11. I do my best when I’m working on a fairly difficult EL task."
        },
        {
         "value": "LG6",
         "text": "12. I try hard to improve on my past performance in EL tasks."
        },
        {
         "value": "LG7",
         "text": "13. The opportunity to extend the range of my abilities in EL is important to me."
        },
        {
         "value": "LG8",
         "text": "14. When I have difficulty solving an EL task, I enjoy trying to approach it from different perspectives."
        }
       ],
       "title": "In my ENGLISH LANGUAGE (EL) class…"
      }
     ],
     "name": "QuestionPanel",
     "title": " "
    }
   ],
   "name": "page2"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "page4"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "SE1",
         "text": "15. I am sure I can learn the skills taught in EL subject well."
        },
        {
         "value": "SE2",
         "text": "16. I can do almost all the work/tasks in EL subject if I do not give up."
        },
        {
         "value": "SE3",
         "text": "17. If I have enough time, I can do a good job in all my EL tasks."
        },
        {
         "value": "SE4",
         "text": "18. Even if the EL topic/work is hard, I can learn it."
        },
        {
         "value": "SE5",
         "text": "19. I am sure I can do difficult tasks in my EL subject."
        },
        {
         "value": "TV1",
         "text": "20. I think learning EL subject is important."
        },
        {
         "value": "TV2",
         "text": "21. I find EL lessons interesting."
        },
        {
         "value": "TV3",
         "text": "22. What I learn in EL is useful."
        },
        {
         "value": "TV4",
         "text": "23. Compared to other subjects, EL is useful."
        },
        {
         "value": "TV5",
         "text": "24. Compared to other subjects, EL is important."
        }
       ],
       "title": " "
      }
     ],
     "name": "QuestionPanel",
     "title": " "
    }
   ],
   "name": "page3"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "page6"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "Comm1",
         "text": "25. I tell my EL teacher about my problems and troubles."
        },
        {
         "value": "Comm2",
         "text": "26. I show my EL teacher my feelings and moods."
        },
        {
         "value": "Alien1",
         "text": "27. I feel angry with my EL teacher."
        },
        {
         "value": "Alien4",
         "text": "28. I don’t believe what my EL teacher says."
        },
        {
         "value": "Comm3",
         "text": "29. I share my skills and talents with my EL teacher."
        },
        {
         "value": "Comm5",
         "text": "30. I tell my EL teacher about my achievements and failures."
        }
       ],
       "title": " "
      }
     ],
     "name": "QuestionPanel",
     "title": " "
    }
   ],
   "name": "page4"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "page7"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "Trust1",
         "text": "31. accepts me as I am."
        },
        {
         "value": "Alien3",
         "text": "32. sees me as a bad student."
        },
        {
         "value": "Trust2",
         "text": "33. believes in my capabilities."
        },
        {
         "value": "Trust3",
         "text": "34. cares about how I feel."
        },
        {
         "value": "Trust4",
         "text": "35. believes that I will pass my subjects."
        },
        {
         "value": "Trust5",
         "text": "36. trusts that I am good even if I don’t realize."
        }
       ],
       "title": " My EL teacher…"
      }
     ],
     "name": "QuestionPanel",
     "title": " "
    }
   ],
   "name": "page5"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "page8"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "TAS1",
         "text": "37. provides me choices and options."
        },
        {
         "value": "TAS2",
         "text": "38. encourages me to ask questions."
        },
        {
         "value": "TAS3",
         "text": "39. listens to how I would like to do things."
        },
        {
         "value": "TAS4",
         "text": "40. asks for my ideas before suggesting a new way to do things."
        },
        {
         "value": "TCS1",
         "text": "41. provides clear instructions that I understand."
        },
        {
         "value": "TCS2",
         "text": "42. provides clear learning goals during our lessons."
        },
        {
         "value": "TCS3",
         "text": "43. gives me tasks that match my abilities."
        },
        {
         "value": "TCS4",
         "text": "44. gives me feedback that helps me improve my work."
        }
       ],
       "title": " My EL teacher…"
      }
     ],
     "name": "QuestionPanel",
     "title": " "
    }
   ],
   "name": "page6"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "BENG1",
         "text": "45. I try hard to do well."
        },
        {
         "value": "BENG2",
         "text": "46. I participate in class discussions."
        },
        {
         "value": "CENG9",
         "text": "47. I try to connect what I am learning with my own experiences."
        },
        {
         "value": "BENG4",
         "text": "48. I listen very carefully."
        },
        {
         "value": "BENG3",
         "text": "49. I work as hard as I can."
        },
        {
         "value": "EENG5",
         "text": "50. I feel interested when we work on tasks."
        },
        {
         "value": "CENG10",
         "text": "51. I try to fit all the different ideas together and make sense of them."
        },
        {
         "value": "EENG6",
         "text": "52. I feel good."
        }
       ],
       "title": " In my EL class / lessons…"
      }
     ],
     "name": "panel7",
     "title": " "
    }
   ],
   "name": "page7"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "CENG11",
         "text": "53. I try to relate what I’m learning to what I already know."
        },
        {
         "value": "EENG7",
         "text": "54. I enjoy learning new things."
        },
        {
         "value": "CENG12",
         "text": "55. I make up my own examples to help me understand important concepts."
        },
        {
         "value": "EENG8",
         "text": "56. I get involved when we work on activities."
        },
        {
         "value": "GENG2",
         "text": "57. I share my ideas during group work."
        },
        {
         "value": "GENG3",
         "text": "58. I try my best to contribute to class/group discussions."
        },
        {
         "value": "GENG4",
         "text": "59. I try my best to contribute to group work."
        }
       ],
       "title": " In my EL class / lessons…"
      }
     ],
     "name": "panel8",
     "title": " "
    }
   ],
   "name": "page8"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "CPcr1",
         "text": "60. I contribute my ideas spontaneously."
        },
        {
         "value": "CPcr2",
         "text": "61. I give imaginative answers."
        },
        {
         "value": "CPcr3",
         "text": "62. I am creative with ideas."
        },
        {
         "value": "CPcr4",
         "text": "63. I am inventive."
        },
        {
         "value": "CPcr5",
         "text": "64. I contribute original (or novel) perspectives."
        },
        {
         "value": "CPcr6",
         "text": "65. I like to experiment with ideas."
        }
       ],
       "title": " In EL class discussions / tasks..."
      }
     ],
     "name": "panel10",
     "title": " "
    }
   ],
   "name": "page9"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "CPcu2",
         "text": "66. I am curious to learn more."
        },
        {
         "value": "CPcu3",
         "text": "67. I am intellectually active."
        },
        {
         "value": "CPcu4",
         "text": "68. I like to inquire deeply when learning."
        },
        {
         "value": "CPcu5",
         "text": "69. I take an investigative approach when studying."
        },
        {
         "value": "CPcu6",
         "text": "70. I enjoy analytical work (e.g. analysing EL texts)."
        },
        {
         "value": "CPcu7",
         "text": "71. I am inquisitive."
        },
        {
         "value": "CPcu8",
         "text": "72. I like to ask questions."
        },
        {
         "value": "CPct1",
         "text": "73. When an idea/opinion is presented, I evaluate whether it is convincing."
        },
        {
         "value": "CPct2",
         "text": "74. When a viewpoint is presented, I analyse the supporting evidence to see if it is credible."
        },
        {
         "value": "CPct5",
         "text": "75. When an assertion/conclusion is presented, I try to think of other possible alternatives."
        }
       ],
       "title": " In my EL class / lessons…"
      }
     ],
     "name": "panel11",
     "title": " "
    }
   ],
   "name": "page10"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Strongly DISAGREE"
        },
        {
         "value": "2",
         "text": "DISAGREE"
        },
        {
         "value": "3",
         "text": "Somewhat DISAGREE"
        },
        {
         "value": "4",
         "text": "Neutral"
        },
        {
         "value": "5",
         "text": "Somewhat AGREE"
        },
        {
         "value": "6",
         "text": "AGREE"
        },
        {
         "value": "7",
         "text": "Strongly AGREE"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "ASC1",
         "text": "76. I feel that I have a number of good qualities."
        },
        {
         "value": "ASC2",
         "text": "77. I feel that I am a student of worth at least equal to my classmates."
        },
        {
         "value": "ASC3",
         "text": "78. I tend to feel that I’m a failure."
        },
        {
         "value": "ASC4",
         "text": "79. I feel that I am able to do tasks as well as most other classmates."
        },
        {
         "value": "ASC5",
         "text": "80. I certainly feel useless at times."
        },
        {
         "value": "ASC6",
         "text": "81. I take a positive attitude toward myself."
        },
        {
         "value": "ASC7",
         "text": "82. I am generally satisfied with myself."
        },
        {
         "value": "ASC8",
         "text": "83. At times, I think I am no good at all."
        },
        {
         "value": "ASC9",
         "text": "84. I feel I do NOT have much to be proud of."
        },
        {
         "value": "ASC10",
         "text": "85. I wish I could have more respect for myself."
        }
       ],
       "title": " In my EL class…"
      }
     ],
     "name": "panel13",
     "title": " "
    }
   ],
   "name": "page11"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How OFTEN are the following statements TRUE of you?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Never"
        },
        {
         "value": "2",
         "text": "Rarely"
        },
        {
         "value": "3",
         "text": "Sometimes"
        },
        {
         "value": "4",
         "text": "Half the time"
        },
        {
         "value": "5",
         "text": "Often"
        },
        {
         "value": "6",
         "text": "Almost Always"
        },
        {
         "value": "7",
         "text": "Always"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "CRA1",
         "text": "86. I know how to explain my personal responses."
        },
        {
         "value": "CRA2",
         "text": "87. I can evaluate the validity and credibility of information presented."
        },
        {
         "value": "CRA3",
         "text": "88. I am able to analyse their organisational structure/patterns."
        },
        {
         "value": "CRA4",
         "text": "89. I am capable of evaluating the writer’s use of language/visuals."
        },
        {
         "value": "CRA5",
         "text": "90. I know how to analyse the effect of typographical features."
        },
        {
         "value": "CRA6",
         "text": "91. I am competent in evaluating the impact of text features used."
        },
        {
         "value": "CRA7",
         "text": "92. I know how to provide evidence to support my opinions."
        }
       ],
       "title": " When I read EL texts..."
      }
     ],
     "name": "panel15",
     "title": " "
    }
   ],
   "name": "page12"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How OFTEN are the following statements TRUE of you?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Never"
        },
        {
         "value": "2",
         "text": "Rarely"
        },
        {
         "value": "3",
         "text": "Sometimes"
        },
        {
         "value": "4",
         "text": "Half the time"
        },
        {
         "value": "5",
         "text": "Often"
        },
        {
         "value": "6",
         "text": "Almost Always"
        },
        {
         "value": "7",
         "text": "Always"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "CRA8",
         "text": "93. I am able to interpret or examine different points of view."
        },
        {
         "value": "CRA9",
         "text": "94. I can make connections between what I already know/feel with what I read."
        },
        {
         "value": "CRA10",
         "text": "95. I have the ability to pose critical questions."
        },
        {
         "value": "CRA11",
         "text": "96. I am able to identify the target audience."
        },
        {
         "value": "CRA12",
         "text": "97. I am capable of examining the underlying assumptions."
        },
        {
         "value": "CRA13",
         "text": "98. I have the ability to identify the writer’s intent."
        }
       ],
       "title": " When I read EL texts..."
      }
     ],
     "name": "panel16",
     "title": " "
    }
   ],
   "name": "page13"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How OFTEN are the following statements TRUE of you?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Never"
        },
        {
         "value": "2",
         "text": "Rarely"
        },
        {
         "value": "3",
         "text": "Sometimes"
        },
        {
         "value": "4",
         "text": "Half the time"
        },
        {
         "value": "5",
         "text": "Often"
        },
        {
         "value": "6",
         "text": "Almost Always"
        },
        {
         "value": "7",
         "text": "Always"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "DM1",
         "text": "99. Studying gives me a feeling of deep personal satisfaction."
        },
        {
         "value": "DS1",
         "text": "100. I feel satisfied when I have done enough work to form my own conclusions."
        },
        {
         "value": "SM1",
         "text": "101. My aim is to pass the exams while doing as little work as possible."
        },
        {
         "value": "SS1",
         "text": "102. I only study seriously what is given out in class or the exam syllabus."
        },
        {
         "value": "DM2",
         "text": "103. I feel that any text can be highly interesting once I get into it."
        },
        {
         "value": "DS2",
         "text": "104. I find new texts interesting and spend extra time trying to learn more about them."
        },
        {
         "value": "SM2",
         "text": "105. I do not find the subject very interesting so I keep my work to the minimum."
        },
        {
         "value": "SS2",
         "text": "106. I learn by memorising answers by heart even if I do not understand them."
        },
        {
         "value": "DM3",
         "text": "107. I find that studying can be as exciting as a good movie."
        },
        {
         "value": "DS3",
         "text": "108. I test myself on important topics until I understand them completely."
        }
       ],
       "title": " In my EL subject / class..."
      }
     ],
     "name": "panel17",
     "title": " "
    }
   ],
   "name": "page14"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How OFTEN are the following statements TRUE of you?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Never"
        },
        {
         "value": "2",
         "text": "Rarely"
        },
        {
         "value": "3",
         "text": "Sometimes"
        },
        {
         "value": "4",
         "text": "Half the time"
        },
        {
         "value": "5",
         "text": "Often"
        },
        {
         "value": "6",
         "text": "Almost Always"
        },
        {
         "value": "7",
         "text": "Always"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "SM3",
         "text": "109. I can pass most tests by memorising right answers instead of understanding them."
        },
        {
         "value": "SS3",
         "text": "110. I limit my study to what’s needed for exams as there’s no need to do extra."
        },
        {
         "value": "DM4",
         "text": "111. I work hard because I find the subject interesting."
        },
        {
         "value": "DS4",
         "text": "112. I spend my free time learning more about interesting topics discussed in class."
        },
        {
         "value": "SM4",
         "text": "113. Studying topics in-depth is not helpful. You only need a surface understanding."
        },
        {
         "value": "SS4",
         "text": "114. We shouldn’t be expected to spend a lot of time studying what won’t be tested in exams."
        },
        {
         "value": "DM5",
         "text": "115. I come to classes with questions in mind that I want to learn more about."
        },
        {
         "value": "DS5",
         "text": "116. I make a point of studying additional materials beyond what is provided in class."
        },
        {
         "value": "SM5",
         "text": "117. I see no point in working on tasks that are not likely to be in the exams."
        },
        {
         "value": "SS5",
         "text": "118. I find the best way to pass exams is to try to remember answers to likely questions."
        }
       ],
       "title": " In my EL subject / class..."
      }
     ],
     "name": "panel18",
     "title": " "
    }
   ],
   "name": "page15"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How OFTEN are the following statements TRUE of you?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Never"
        },
        {
         "value": "2",
         "text": "Rarely"
        },
        {
         "value": "3",
         "text": "Sometimes"
        },
        {
         "value": "4",
         "text": "Half the time"
        },
        {
         "value": "5",
         "text": "Often"
        },
        {
         "value": "6",
         "text": "Almost Always"
        },
        {
         "value": "7",
         "text": "Always"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "SDLr1",
         "text": "119. I reappraise my experiences so I can learn from them."
        },
        {
         "value": "SDLr2",
         "text": "120. I try to think about my strengths and weaknesses."
        },
        {
         "value": "SDLr3",
         "text": "121. I think about my actions to see whether I can improve them."
        },
        {
         "value": "SDLr4",
         "text": "122. I think about my past experiences to understand new ideas."
        },
        {
         "value": "SDLr5",
         "text": "123. I try to think about how I can do things better next time."
        },
        {
         "value": "SDLg6",
         "text": "124. I think about the different strategies/lenses I could use."
        },
        {
         "value": "SDLg7",
         "text": "125. I try to check my progress."
        },
        {
         "value": "SDLg8",
         "text": "126. I make plans for how I can improve."
        },
        {
         "value": "SDLg9",
         "text": "127. I try to determine the best way to go about it."
        }
       ],
       "title": " When I work on EL tasks..."
      }
     ],
     "name": "panel20",
     "title": " "
    }
   ],
   "name": "page16"
  },
  {
   "elements": [
    {
     "type": "panel",
     "elements": [
      {
       "type": "html",
       "html": "<h2>How OFTEN are the following statements TRUE of you?</h2>\n",
       "name": "question1"
      },
      {
       "type": "matrix",
       "columns": [
        {
         "value": "1",
         "text": "Never"
        },
        {
         "value": "2",
         "text": "Seldom"
        },
        {
         "value": "3",
         "text": "Sometimes"
        },
        {
         "value": "4",
         "text": "Often"
        },
        {
         "value": "5",
         "text": "Always"
        }
       ],
       "isAllRowRequired": true,
       "name": "surveyQuestions",
       "rows": [
        {
         "value": "SGP1",
         "text": "128. share what you've learned with your classmates?"
        },
        {
         "value": "SGP2",
         "text": "129. help your classmates solve a problem once you've figured it out?"
        },
        {
         "value": "SGP3",
         "text": "130. help other classmates when they have a problem?"
        },
        {
         "value": "SGP4",
         "text": "131. think about how your behaviour will affect other classmates?"
        },
        {
         "value": "SGP5",
         "text": "132. help your classmates learn new things?"
        },
        {
         "value": "SGR1",
         "text": "133. do what your teacher asks you?"
        },
        {
         "value": "SGR2",
         "text": "134. be considerate when others are trying to study/learn?"
        },
        {
         "value": "SGR3",
         "text": "135. keep working even when other classmates are slacking off?"
        },
        {
         "value": "SGR4",
         "text": "136. be interested when others are trying to share their views?"
        }
       ],
       "title": "How often do you try to..."
      }
     ],
     "name": "lastPanel",
     "title": " "
    }
   ],
   "name": "page17"
  }
 ],
 "requiredText": "",
 "showProgressBar": "bottom",
 "showQuestionNumbers": "off",
 "showTitle": false,
 "title": "wiread_survey"
}
	
	if(dashboard_type == 2)		//No dashboard
	{
		var surveyJSON = {
 title: "covaa_survey",
 pages: [
  {
   name: "userParticulars",
   elements: [
    {
     type: "panel",
     name: "panel1",
     elements: [
      {
       type: "html",
       name: "instructions",
       html: "<div>\n<p><strong>THANK YOU</strong> for agreeing to fill in this questionnaire</p>\n<ul>\n<li>The survey is about <strong>your feelings and attitudes</strong> towards <strong>learning Social Studies.</strong></li>\n<li>You should be able to complete this in <strong><u>20 MINUTES.</u></strong></li>\n<li><strong>Please answer <u>ALL</u> the questions.</strong> There is <strong><u>NO RIGHT OR WRONG</u></strong> answer. </li>\n\n<li>Choose the answer that is most <strong><u>HONEST</u></strong> and <strong><u>ACCURATE</u></strong> about yourself. Do not worry about projecting a good image.</li>\n\n<li><strong>Your answers are <u>CONFIDENTIAL</u>. Your teacher and friends will <u>NOT KNOW</u> your responses.</strong></li>\n</ul>\n</div>"
      }
     ],
     title: "CoVAA STUDENT QUESTIONNAIRE"
    }
   ]
  },
  {
   name: "page1",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SOCIAL STUDIES (SS) class...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "PG1",
         text: "1. I prefer to do SS tasks that I can do well rather than things that I do poorly."
        },
        {
         value: "PG2",
         text: "2. I am happiest when I perform SS tasks that I know I won't make any errors."
        },
        {
         value: "PG3",
         text: "3. The SS tasks I enjoy the most are the things that I do the best."
        },
        {
         value: "PG4",
         text: "4. The opinions others have about how well I can do SS tasks are important to me."
        },
        {
         value: "PG5",
         text: "5. I feel smart when I do SS tasks without making any mistakes."
        },
        {
         value: "PG6",
         text: "6. I like to be fairly confident that I can successfully perform an SS task before I try it."
        },
        {
         value: "PG7",
         text: "7. I like to work on SS tasks that I have done well on in the past."
        },
        {
         value: "PG8",
         text: "8. I feel smart when I can do an SS task better than most other people."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page2",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page2",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "LG1",
         text: "9. The opportunity to do challenging work in SS class is important to me."
        },
        {
         value: "LG2",
         text: "10. When I fail to complete a difficult SS task, I try harder the next time I work on it."
        },
        {
         value: "LG3",
         text: "11. I prefer to work on SS tasks that force me to learn new things."
        },
        {
         value: "LG4",
         text: "12. The opportunity to learn new things in SS class is important to me."
        },
        {
         value: "LG5",
         text: "13. I do my best when I'm working on a fairly difficult SS task."
        },
        {
         value: "LG6",
         text: "14. I try hard to improve on my past performance in SS tasks."
        },
        {
         value: "LG7",
         text: "15. The opportunity to extend the range of my abilities in SS is important to me."
        },
        {
         value: "LG8",
         text: "16. When I have difficulty solving an SS task, I enjoy trying to approach it from different perspectives."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page3",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page4",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "SE1",
         text: "17. I am sure I can learn the skills taught in SS subject well."
        },
        {
         value: "SE2",
         text: "18. I can do almost all the work/tasks in SS subject if I do not give up."
        },
        {
         value: "SE3",
         text: "19. If I have enough time, I can do a good job in all my SS tasks."
        },
        {
         value: "SE4",
         text: "20. Even if the SS topic/work is hard, I can learn it."
        },
        {
         value: "SE5",
         text: "21. I am sure I can do difficult tasks in my SS subject."
        },
        {
         value: "TV1",
         text: "22. I think learning SS subject is important."
        },
        {
         value: "TV2",
         text: "23. I find SS lessons interesting."
        },
        {
         value: "TV3",
         text: "24. What I learn in SS is useful."
        },
        {
         value: "TV4",
         text: "25. Compared to other subjects, SS is useful."
        },
        {
         value: "TV5",
         text: "26. Compared to other subjects, SS is important."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page4",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page6",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "Alien1",
         text: "27. I feel angry with my SS teacher."
        },
        {
         value: "Alien4",
         text: "28. I don't believe what my SS teacher says."
        },
        {
         value: "Trust1",
         text: "29. My SS teacher accepts me as I am."
        },
        {
         value: "Alien3",
         text: "30. My SS teacher sees me as a bad student."
        },
        {
         value: "Trust2",
         text: "31. My SS teacher believes in my capabilities."
        },
        {
         value: "Trust3",
         text: "32. My SS teacher cares about how I feel."
        },
        {
         value: "Trust4",
         text: "33. My SS teacher believes that I will pass my subjects."
        },
        {
         value: "Trust5",
         text: "34. My SS teacher trusts that I am good even if I don't realize."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page5",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page7",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: " My SS teacher...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "TAS1",
         text: "35. provides me choices and options."
        },
        {
         value: "TAS2",
         text: "36. encourages me to ask questions."
        },
        {
         value: "TAS3",
         text: "37. listens to how I would like to do things."
        },
        {
         value: "TAS4",
         text: "38. asks for my ideas before suggesting a new way to do things."
        },
        {
         value: "TCS1",
         text: "39. provides clear instructions that I understand."
        },
        {
         value: "TCS2",
         text: "40. provides clear learning goals during our lessons."
        },
        {
         value: "TCS3",
         text: "41. gives me tasks that match my abilities."
        },
        {
         value: "TCS4",
         text: "42. gives me feedback that helps me improve my work."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page6",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page8",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class / lessons...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "BENG1",
         text: "43. I try hard to do well."
        },
        {
         value: "BENG2",
         text: "44. I participate in class discussions."
        },
        {
         value: "CENG9",
         text: "45. I try to connect what I am learning with my own experiences."
        },
        {
         value: "BENG4",
         text: "46. I listen very carefully."
        },
        {
         value: "BENG3",
         text: "47. I work as hard as I can."
        },
        {
         value: "EENG5",
         text: "48. I feel interested when we work on tasks."
        },
        {
         value: "CENG10",
         text: "49. I try to fit all the different ideas together and make sense of them."
        },
        {
         value: "EENG6",
         text: "50. I feel good."
        },
        {
         value: "EENG13",
         text: "51. I find Social Studies lessons fun."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page7",
   elements: [
    {
     type: "panel",
     name: "panel7",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class / lessons...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "CENG11",
         text: "52. I try to relate what I'm learning to what I already know."
        },
        {
         value: "EENG7",
         text: "53. I enjoy learning new things."
        },
        {
         value: "CENG12",
         text: "54. I make up my own examples to help me understand important concepts."
        },
        {
         value: "EENG8",
         text: "55. I get involved when we work on activities."
        },
        {
         value: "GENG1",
         text: "56. I try my best to contribute to group discussions in SS."
        },
        {
         value: "GENG2",
         text: "57. I share my ideas during group work."
        },
        {
         value: "GENG3",
         text: "58. I try my best to contribute to class/group discussions."
        },
        {
         value: "GENG4",
         text: "59. I try my best to contribute to group work."
        },
        {
         value: "GENG5",
         text: "60. I enjoy discussions with my classmates in SS."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page8",
   elements: [
    {
     type: "panel",
     name: "panel8",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In SS class discussions / tasks...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "CPcr1",
         text: "61. I contribute my ideas spontaneously."
        },
        {
         value: "CPcr2",
         text: "62. I give imaginative answers."
        },
        {
         value: "CPcr3",
         text: "63. I am creative with ideas."
        },
        {
         value: "CPcr4",
         text: "64. I am inventive."
        },
        {
         value: "CPcr5",
         text: "65. I contribute original (or novel) perspectives."
        },
        {
         value: "CPcr6",
         text: "66. I like to experiment with ideas."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page9",
   elements: [
    {
     type: "panel",
     name: "panel10",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class / lessons...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "CPcu2",
         text: "67. I am curious to learn more."
        },
        {
         value: "CPcu3",
         text: "68. I am intellectually active."
        },
        {
         value: "CPcu4",
         text: "69. I like to inquire deeply when learning."
        },
        {
         value: "CPcu5",
         text: "70. I take an investigative approach when studying."
        },
        {
         value: "CPcu6",
         text: "71. I enjoy analytical work (e.g. analysing SS texts/videos)."
        },
        {
         value: "CPcu7",
         text: "72. I am inquisitive."
        },
        {
         value: "CPcu8",
         text: "73. I like to ask questions."
        },
        {
         value: "CPct1",
         text: "74. When an idea/opinion is presented, I evaluate whether it is convincing."
        },
        {
         value: "CPct2",
         text: "75. When a viewpoint is presented, I analyse the supporting evidence to see if it is credible."
        },
        {
         value: "CPct5",
         text: "76. When an assertion/conclusion is presented, I try to think of other possible alternatives."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page10",
   elements: [
    {
     type: "panel",
     name: "panel11",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "ASC1",
         text: "77. I feel that I have a number of good qualities."
        },
        {
         value: "ASC2",
         text: "78. I feel that I am a student of worth at least equal to my classmates."
        },
        {
         value: "ASC3",
         text: "79. I tend to feel that I'm a failure."
        },
        {
         value: "ASC4",
         text: "80. I feel that I am able to do tasks as well as most other classmates."
        },
        {
         value: "ASC5",
         text: "81. I certainly feel useless at times."
        },
        {
         value: "ASC6",
         text: "82. I take a positive attitude toward myself."
        },
        {
         value: "ASC7",
         text: "83. I am generally satisfied with myself."
        },
        {
         value: "ASC8",
         text: "84. At times, I think I am no good at all."
        },
        {
         value: "ASC9",
         text: "85. I feel I do NOT have much to be proud of."
        },
        {
         value: "ASC10",
         text: "86. I wish I could have more respect for myself."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page11",
   elements: [
    {
     type: "panel",
     name: "panel2",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How OFTEN are the following statements TRUE of you?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS subject / class...",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Rarely"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Half the time"
        },
        {
         value: "5",
         text: "Often"
        },
        {
         value: "6",
         text: "Almost Always"
        },
        {
         value: "7",
         text: "Always"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "DM1",
         text: "87. Studying gives me a feeling of deep personal satisfaction."
        },
        {
         value: "DS1",
         text: "88. I feel satisfied when I have done enough work to form my own conclusions."
        },
        {
         value: "SM1",
         text: "89. My aim is to pass exams while doing as little work as possible."
        },
        {
         value: "SS1",
         text: "90. I only study seriously what is given out in class or the exam syllabus."
        },
        {
         value: "DM2",
         text: "91. I feel that any text can be highly interesting once I get into it."
        },
        {
         value: "DS2",
         text: "92. I find new texts interesting and spend extra time trying to learn more about them."
        },
        {
         value: "SM2",
         text: "93. I do not find the subject very interesting so I keep my work to the minimum."
        },
        {
         value: "SS2",
         text: "94. I learn by memorising answers by heart even if I do not understand them."
        },
        {
         value: "DM3",
         text: "95. I find that studying can be as exciting as a good movie."
        },
        {
         value: "DS3",
         text: "96. I test myself on important topics until I understand them completely."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page12",
   elements: [
    {
     type: "panel",
     name: "panel3",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How OFTEN are the following statements TRUE of you?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS subject / class...",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Rarely"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Half the time"
        },
        {
         value: "5",
         text: "Often"
        },
        {
         value: "6",
         text: "Almost Always"
        },
        {
         value: "7",
         text: "Always"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "SM3",
         text: "97. I can pass most tests by memorising right answers instead of understanding them."
        },
        {
         value: "SS3",
         text: "98. I limit my study to what's needed for exams as there's no need to do extra."
        },
        {
         value: "DM4",
         text: "99. I work hard because I find the subject interesting."
        },
        {
         value: "DS4",
         text: "100. I spend my free time learning more about interesting topics discussed in class."
        },
        {
         value: "SM4",
         text: "101. Studying topics in-depth is not helpful. You only need a surface understanding."
        },
        {
         value: "SS4",
         text: "102. We shouldn't be expected to spend a lot of time studying what won't be tested in exams."
        },
        {
         value: "DM5",
         text: "103. I come to classes with questions in mind that I want to learn more about."
        },
        {
         value: "DS5",
         text: "104. I make a point of studying additional materials beyond what is provided in class."
        },
        {
         value: "SM5",
         text: "105. I see no point in working on tasks that are not likely to be in the exams."
        },
        {
         value: "SS5",
         text: "106. I find the best way to pass exams is to try to remember answers to likely questions."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page13",
   elements: [
    {
     type: "panel",
     name: "panel4",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How OFTEN are the following statements TRUE of you?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "When I work on SS tasks...",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Rarely"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Half the time"
        },
        {
         value: "5",
         text: "Often"
        },
        {
         value: "6",
         text: "Almost Always"
        },
        {
         value: "7",
         text: "Always"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "SDLr1",
         text: "107. I reappraise my experiences so I can learn from them."
        },
        {
         value: "SDLr2",
         text: "108. I try to think about my strengths and weaknesses."
        },
        {
         value: "SDLr3",
         text: "109. I think about my actions to see whether I can improve them."
        },
        {
         value: "SDLr4",
         text: "110. I think about my past experiences to understand new ideas."
        },
        {
         value: "SDLr5",
         text: "111. I try to think about how I can do things better next time."
        },
        {
         value: "SDLg6",
         text: "112. I think about the different strategies/skills I could use."
        },
        {
         value: "SDLg7",
         text: "113. I try to check my progress."
        },
        {
         value: "SDLg8",
         text: "114. I make plans for how I can improve."
        },
        {
         value: "SDLg9",
         text: "115. I try to determine the best way to go about it."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page14",
   elements: [
    {
     type: "panel",
     name: "panel5",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How often do you try to...</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Seldom"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Often"
        },
        {
         value: "5",
         text: "Always"
        },
        {
         value: "0",
         text: "NULL"
        }
       ],
       rows: [
        {
         value: "SGP1",
         text: "116. share what you've learned with your classmates?"
        },
        {
         value: "SGP2",
         text: "117. help your classmates solve a problem once you've figured it out?"
        },
        {
         value: "SGP3",
         text: "118. help other classmates when they have a problem?"
        },
        {
         value: "SGP4",
         text: "119. think about how your behaviour will affect other classmates?"
        },
        {
         value: "SGP5",
         text: "120. help your classmates learn new things?"
        },
        {
         value: "SGR1",
         text: "121. do what your teacher asks you?"
        },
        {
         value: "SGR2",
         text: "122. be considerate when others are trying to study/learn?"
        },
        {
         value: "SGR3",
         text: "123. keep working even when other classmates are slacking off?"
        },
        {
         value: "SGR4",
         text: "124. be interested when others are trying to share their views?"
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  }
 ],
 sendResultOnPageNext: true,
 showTitle: false,
 showQuestionNumbers: "off",
 showProgressBar: "bottom",
 requiredText: ""
}
	}
	else					 	//Self or Peer Dashboard
	{
		var surveyJSON = {
 title: "covaa_survey",
 pages: [
  {
   name: "userParticulars",
   elements: [
    {
     type: "panel",
     name: "panel1",
     elements: [
      {
       type: "html",
       name: "instructions",
       html: "<div>\n<p><strong>THANK YOU</strong> for agreeing to fill in this questionnaire</p>\n<ul>\n<li>The survey is about <strong>your feelings and attitudes</strong> towards <strong>learning Social Studies.</strong></li>\n<li>You should be able to complete this in <strong><u>20 MINUTES.</u></strong></li>\n<li><strong>Please answer <u>ALL</u> the questions.</strong> There is <strong><u>NO RIGHT OR WRONG</u></strong> answer. </li>\n\n<li>Choose the answer that is most <strong><u>HONEST</u></strong> and <strong><u>ACCURATE</u></strong> about yourself. Do not worry about projecting a good image.</li>\n\n<li><strong>Your answers are <u>CONFIDENTIAL</u>. Your teacher and friends will <u>NOT KNOW</u> your responses.</strong></li>\n</ul>\n</div>"
      }
     ],
     title: "CoVAA STUDENT QUESTIONNAIRE"
    }
   ]
  },
  {
   name: "page1",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SOCIAL STUDIES (SS) class...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "PG1",
         text: "1. I prefer to do SS tasks that I can do well rather than things that I do poorly."
        },
        {
         value: "PG2",
         text: "2. I am happiest when I perform SS tasks that I know I won't make any errors."
        },
        {
         value: "PG3",
         text: "3. The SS tasks I enjoy the most are the things that I do the best."
        },
        {
         value: "PG4",
         text: "4. The opinions others have about how well I can do SS tasks are important to me."
        },
        {
         value: "PG5",
         text: "5. I feel smart when I do SS tasks without making any mistakes."
        },
        {
         value: "PG6",
         text: "6. I like to be fairly confident that I can successfully perform an SS task before I try it."
        },
        {
         value: "PG7",
         text: "7. I like to work on SS tasks that I have done well on in the past."
        },
        {
         value: "PG8",
         text: "8. I feel smart when I can do an SS task better than most other people."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page2",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page2",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "LG1",
         text: "9. The opportunity to do challenging work in SS class is important to me."
        },
        {
         value: "LG2",
         text: "10. When I fail to complete a difficult SS task, I try harder the next time I work on it."
        },
        {
         value: "LG3",
         text: "11. I prefer to work on SS tasks that force me to learn new things."
        },
        {
         value: "LG4",
         text: "12. The opportunity to learn new things in SS class is important to me."
        },
        {
         value: "LG5",
         text: "13. I do my best when I'm working on a fairly difficult SS task."
        },
        {
         value: "LG6",
         text: "14. I try hard to improve on my past performance in SS tasks."
        },
        {
         value: "LG7",
         text: "15. The opportunity to extend the range of my abilities in SS is important to me."
        },
        {
         value: "LG8",
         text: "16. When I have difficulty solving an SS task, I enjoy trying to approach it from different perspectives."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page3",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page4",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
        title: " ",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "SE1",
         text: "17. I am sure I can learn the skills taught in SS subject well."
        },
        {
         value: "SE2",
         text: "18. I can do almost all the work/tasks in SS subject if I do not give up."
        },
        {
         value: "SE3",
         text: "19. If I have enough time, I can do a good job in all my SS tasks."
        },
        {
         value: "SE4",
         text: "20. Even if the SS topic/work is hard, I can learn it."
        },
        {
         value: "SE5",
         text: "21. I am sure I can do difficult tasks in my SS subject."
        },
        {
         value: "TV1",
         text: "22. I think learning SS subject is important."
        },
        {
         value: "TV2",
         text: "23. I find SS lessons interesting."
        },
        {
         value: "TV3",
         text: "24. What I learn in SS is useful."
        },
        {
         value: "TV4",
         text: "25. Compared to other subjects, SS is useful."
        },
        {
         value: "TV5",
         text: "26. Compared to other subjects, SS is important."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page4",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page6",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: " ",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "Alien1",
         text: "27. I feel angry with my SS teacher."
        },
        {
         value: "Alien4",
         text: "28. I don't believe what my SS teacher says."
        },
        {
         value: "Trust1",
         text: "29. My SS teacher accepts me as I am."
        },
        {
         value: "Alien3",
         text: "30. My SS teacher sees me as a bad student."
        },
        {
         value: "Trust2",
         text: "31. My SS teacher believes in my capabilities."
        },
        {
         value: "Trust3",
         text: "32. My SS teacher cares about how I feel."
        },
        {
         value: "Trust4",
         text: "33. My SS teacher believes that I will pass my subjects."
        },
        {
         value: "Trust5",
         text: "34. My SS teacher trusts that I am good even if I don't realize."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page5",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page7",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: " My SS teacher...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "TAS1",
         text: "35. provides me choices and options."
        },
        {
         value: "TAS2",
         text: "36. encourages me to ask questions."
        },
        {
         value: "TAS3",
         text: "37. listens to how I would like to do things."
        },
        {
         value: "TAS4",
         text: "38. asks for my ideas before suggesting a new way to do things."
        },
        {
         value: "TCS1",
         text: "39. provides clear instructions that I understand."
        },
        {
         value: "TCS2",
         text: "40. provides clear learning goals during our lessons."
        },
        {
         value: "TCS3",
         text: "41. gives me tasks that match my abilities."
        },
        {
         value: "TCS4",
         text: "42. gives me feedback that helps me improve my work."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page6",
   elements: [
    {
     type: "panel",
     name: "QuestionPanel",
     elements: [
      {
       type: "html",
       name: "page8",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class / lessons...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "BENG1",
         text: "43. I try hard to do well."
        },
        {
         value: "BENG2",
         text: "44. I participate in class discussions."
        },
        {
         value: "CENG9",
         text: "45. I try to connect what I am learning with my own experiences."
        },
        {
         value: "BENG4",
         text: "46. I listen very carefully."
        },
        {
         value: "BENG3",
         text: "47. I work as hard as I can."
        },
        {
         value: "EENG5",
         text: "48. I feel interested when we work on tasks."
        },
        {
         value: "CENG10",
         text: "49. I try to fit all the different ideas together and make sense of them."
        },
        {
         value: "EENG6",
         text: "50. I feel good."
        },
        {
         value: "EENG13",
         text: "51. I find Social Studies lessons fun."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page7",
   elements: [
    {
     type: "panel",
     name: "panel7",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class / lessons...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "CENG11",
         text: "52. I try to relate what I'm learning to what I already know."
        },
        {
         value: "EENG7",
         text: "53. I enjoy learning new things."
        },
        {
         value: "CENG12",
         text: "54. I make up my own examples to help me understand important concepts."
        },
        {
         value: "EENG8",
         text: "55. I get involved when we work on activities."
        },
        {
         value: "GENG1",
         text: "56. I try my best to contribute to group discussions in SS."
        },
        {
         value: "GENG2",
         text: "57. I share my ideas during group work."
        },
        {
         value: "GENG3",
         text: "58. I try my best to contribute to class/group discussions."
        },
        {
         value: "GENG4",
         text: "59. I try my best to contribute to group work."
        },
        {
         value: "GENG5",
         text: "60. I enjoy discussions with my classmates in SS."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page8",
   elements: [
    {
     type: "panel",
     name: "panel8",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In SS class discussions / tasks...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "CPcr1",
         text: "61. I contribute my ideas spontaneously."
        },
        {
         value: "CPcr2",
         text: "62. I give imaginative answers."
        },
        {
         value: "CPcr3",
         text: "63. I am creative with ideas."
        },
        {
         value: "CPcr4",
         text: "64. I am inventive."
        },
        {
         value: "CPcr5",
         text: "65. I contribute original (or novel) perspectives."
        },
        {
         value: "CPcr6",
         text: "66. I like to experiment with ideas."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page9",
   elements: [
    {
     type: "panel",
     name: "panel10",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class / lessons...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "CPcu2",
         text: "67. I am curious to learn more."
        },
        {
         value: "CPcu3",
         text: "68. I am intellectually active."
        },
        {
         value: "CPcu4",
         text: "69. I like to inquire deeply when learning."
        },
        {
         value: "CPcu5",
         text: "70. I take an investigative approach when studying."
        },
        {
         value: "CPcu6",
         text: "71. I enjoy analytical work (e.g. analysing SS texts/videos)."
        },
        {
         value: "CPcu7",
         text: "72. I am inquisitive."
        },
        {
         value: "CPcu8",
         text: "73. I like to ask questions."
        },
        {
         value: "CPct1",
         text: "74. When an idea/opinion is presented, I evaluate whether it is convincing."
        },
        {
         value: "CPct2",
         text: "75. When a viewpoint is presented, I analyse the supporting evidence to see if it is credible."
        },
        {
         value: "CPct5",
         text: "76. When an assertion/conclusion is presented, I try to think of other possible alternatives."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page10",
   elements: [
    {
     type: "panel",
     name: "panel11",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS class...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "ASC1",
         text: "77. I feel that I have a number of good qualities."
        },
        {
         value: "ASC2",
         text: "78. I feel that I am a student of worth at least equal to my classmates."
        },
        {
         value: "ASC3",
         text: "79. I tend to feel that I'm a failure."
        },
        {
         value: "ASC4",
         text: "80. I feel that I am able to do tasks as well as most other classmates."
        },
        {
         value: "ASC5",
         text: "81. I certainly feel useless at times."
        },
        {
         value: "ASC6",
         text: "82. I take a positive attitude toward myself."
        },
        {
         value: "ASC7",
         text: "83. I am generally satisfied with myself."
        },
        {
         value: "ASC8",
         text: "84. At times, I think I am no good at all."
        },
        {
         value: "ASC9",
         text: "85. I feel I do NOT have much to be proud of."
        },
        {
         value: "ASC10",
         text: "86. I wish I could have more respect for myself."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page11",
   elements: [
    {
     type: "panel",
     name: "panel2",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How OFTEN are the following statements TRUE of you?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS subject / class...",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Rarely"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Half the time"
        },
        {
         value: "5",
         text: "Often"
        },
        {
         value: "6",
         text: "Almost Always"
        },
        {
         value: "7",
         text: "Always"
        }
       ],
       rows: [
        {
         value: "DM1",
         text: "87. Studying gives me a feeling of deep personal satisfaction."
        },
        {
         value: "DS1",
         text: "88. I feel satisfied when I have done enough work to form my own conclusions."
        },
        {
         value: "SM1",
         text: "89. My aim is to pass exams while doing as little work as possible."
        },
        {
         value: "SS1",
         text: "90. I only study seriously what is given out in class or the exam syllabus."
        },
        {
         value: "DM2",
         text: "91. I feel that any text can be highly interesting once I get into it."
        },
        {
         value: "DS2",
         text: "92. I find new texts interesting and spend extra time trying to learn more about them."
        },
        {
         value: "SM2",
         text: "93. I do not find the subject very interesting so I keep my work to the minimum."
        },
        {
         value: "SS2",
         text: "94. I learn by memorising answers by heart even if I do not understand them."
        },
        {
         value: "DM3",
         text: "95. I find that studying can be as exciting as a good movie."
        },
        {
         value: "DS3",
         text: "96. I test myself on important topics until I understand them completely."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page12",
   elements: [
    {
     type: "panel",
     name: "panel3",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How OFTEN are the following statements TRUE of you?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "In my SS subject / class...",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Rarely"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Half the time"
        },
        {
         value: "5",
         text: "Often"
        },
        {
         value: "6",
         text: "Almost Always"
        },
        {
         value: "7",
         text: "Always"
        }
       ],
       rows: [
        {
         value: "SM3",
         text: "97. I can pass most tests by memorising right answers instead of understanding them."
        },
        {
         value: "SS3",
         text: "98. I limit my study to what's needed for exams as there's no need to do extra."
        },
        {
         value: "DM4",
         text: "99. I work hard because I find the subject interesting."
        },
        {
         value: "DS4",
         text: "100. I spend my free time learning more about interesting topics discussed in class."
        },
        {
         value: "SM4",
         text: "101. Studying topics in-depth is not helpful. You only need a surface understanding."
        },
        {
         value: "SS4",
         text: "102. We shouldn't be expected to spend a lot of time studying what won't be tested in exams."
        },
        {
         value: "DM5",
         text: "103. I come to classes with questions in mind that I want to learn more about."
        },
        {
         value: "DS5",
         text: "104. I make a point of studying additional materials beyond what is provided in class."
        },
        {
         value: "SM5",
         text: "105. I see no point in working on tasks that are not likely to be in the exams."
        },
        {
         value: "SS5",
         text: "106. I find the best way to pass exams is to try to remember answers to likely questions."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page13",
   elements: [
    {
     type: "panel",
     name: "panel4",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How OFTEN are the following statements TRUE of you?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "When I work on SS tasks...",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Rarely"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Half the time"
        },
        {
         value: "5",
         text: "Often"
        },
        {
         value: "6",
         text: "Almost Always"
        },
        {
         value: "7",
         text: "Always"
        }
       ],
       rows: [
        {
         value: "SDLr1",
         text: "107. I reappraise my experiences so I can learn from them."
        },
        {
         value: "SDLr2",
         text: "108. I try to think about my strengths and weaknesses."
        },
        {
         value: "SDLr3",
         text: "109. I think about my actions to see whether I can improve them."
        },
        {
         value: "SDLr4",
         text: "110. I think about my past experiences to understand new ideas."
        },
        {
         value: "SDLr5",
         text: "111. I try to think about how I can do things better next time."
        },
        {
         value: "SDLg6",
         text: "112. I think about the different strategies/skills I could use."
        },
        {
         value: "SDLg7",
         text: "113. I try to check my progress."
        },
        {
         value: "SDLg8",
         text: "114. I make plans for how I can improve."
        },
        {
         value: "SDLg9",
         text: "115. I try to determine the best way to go about it."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page14",
   elements: [
    {
     type: "panel",
     name: "panel5",
     elements: [
      {
       type: "html",
       name: "question1",
       html: "<h2>How often do you try to...</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
        title: " ",
       columns: [
        {
         value: "1",
         text: "Never"
        },
        {
         value: "2",
         text: "Seldom"
        },
        {
         value: "3",
         text: "Sometimes"
        },
        {
         value: "4",
         text: "Often"
        },
        {
         value: "5",
         text: "Always"
        }
       ],
       rows: [
        {
         value: "SGP1",
         text: "116. share what you've learned with your classmates?"
        },
        {
         value: "SGP2",
         text: "117. help your classmates solve a problem once you've figured it out?"
        },
        {
         value: "SGP3",
         text: "118. help other classmates when they have a problem?"
        },
        {
         value: "SGP4",
         text: "119. think about how your behaviour will affect other classmates?"
        },
        {
         value: "SGP5",
         text: "120. help your classmates learn new things?"
        },
        {
         value: "SGR1",
         text: "121. do what your teacher asks you?"
        },
        {
         value: "SGR2",
         text: "122. be considerate when others are trying to study/learn?"
        },
        {
         value: "SGR3",
         text: "123. keep working even when other classmates are slacking off?"
        },
        {
         value: "SGR4",
         text: "124. be interested when others are trying to share their views?"
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page15",
   elements: [
    {
     type: "panel",
     name: "panel14",
     elements: [
      {
       type: "html",
       name: "question4",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
        title: " ",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "SUPPTEACH5",
         text: "125. My teacher encourages me to use CoVAA."
        },
        {
         value: "SUPPTEACH6",
         text: "126. I am motivated by my teacher to use CoVAA."
        },
        {
         value: "SUPPTEACH7",
         text: "127. There is positive support from my teacher to use CoVAA."
        },
        {
         value: "SUPPPEER2",
         text: "128. Classmates that I want to learn from are also using CoVAA."
        },
        {
         value: "SUPPPEER3",
         text: "129. My good friends in class use CoVAA."
        },
        {
         value: "SUPPPEER4",
         text: "130. There is positive support from my classmates to use CoVAA."
        },
        {
         value: "SUPPPEER1",
         text: "131. I am encouraged by my friends in class to use CoVAA."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page16",
   elements: [
    {
     type: "panel",
     name: "panel15",
     elements: [
      {
       type: "html",
       name: "question6",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
        title: " ",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "SUPP8",
         text: "132. It's a good idea to use CoVAA to critically discuss SS topics beyond class time."
        },
        {
         value: "SUPPPEER9",
         text: "133. Using CoVAA is cool."
        },
        {
         value: "EOU1",
         text: "134. I had no problems using CoVAA."
        },
        {
         value: "EOU2",
         text: "135. CoVAA is easy to use."
        },
        {
         value: "EOU3",
         text: "136. It is easy to view and make annotations on videos using CoVAA."
        },
        {
         value: "EOU4",
         text: "137. It is easy to read and make comments on CoVAA discussion/chat board."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page17",
   elements: [
    {
     type: "panel",
     name: "panel16",
     elements: [
      {
       type: "html",
       name: "question8",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
        title: " ",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "PU19",
         text: "138. CoVAA is useful for learning at my own pace."
        },
        {
         value: "PU4Express",
         text: "139. CoVAA provokes new ideas and discussions during lessons."
        },
        {
         value: "PU5Social",
         text: "140. CoVAA helps me feel more connected to my classmates."
        },
        {
         value: "PU20",
         text: "141. CoVAA helps me get to know my classmates' views/experiences better."
        },
        {
         value: "PU7Social",
         text: "142. CoVAA exposes me to exemplary answers from peers."
        },
        {
         value: "PU8Express",
         text: "143. CoVAA helps me get inspiration for new ideas/opinions on the topic."
        },
        {
         value: "PU10Express",
         text: "144. CoVAA helps me learn to express and justify my ideas/opinions more."
        },
        {
         value: "PU13C21",
         text: "145. CoVAA helps me develop my critical thinking skills."
        },
        {
         value: "PU14C21",
         text: "146. CoVAA helps enhance my interest in SS topics."
        },
        {
         value: "PU15C21",
         text: "147. CoVAA helps me understand topics from different perspectives."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page18",
   elements: [
    {
     type: "panel",
     name: "panel17",
     elements: [
      {
       type: "html",
       name: "question9",
       html: "<h2>How much do you AGREE or DISAGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
        title: " ",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "PU22",
         text: "148. CoVAA enhances my opportunities to learn from my classmates."
        },
        {
         value: "PU17Acad",
         text: "149. CoVAA helps me improve my performance in tests/exams."
        },
        {
         value: "PU18Acad",
         text: "150. CoVAA helps me learn new knowledge beyond the textbook materials."
        },
        {
         value: "PU21",
         text: "151. CoVAA helps to deepen my understanding of concepts/topics taught."
        },
        {
         value: "PUSocial6",
         text: "152. CoVAA helps me expand my learning network in SS class."
        },
        {
         value: "PUExpress9",
         text: "153. CoVAA increases my opportunities for critical discussion of SS topics."
        },
        {
         value: "PU21C11",
         text: "154. CoVAA helps me develop creative thinking skills (e.g. new ideas/ways to think about topics)."
        },
        {
         value: "PU21C12",
         text: "155. CoVAA helps me develop my digital/technology skills."
        },
        {
         value: "PUAcad16",
         text: "156. CoVAA exposes me to more tips/ideas from others on how to do well in SS exams."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page19",
   elements: [
    {
     type: "panel",
     name: "panel18",
     elements: [
      {
       type: "html",
       name: "question10",
       html: "<h2>How much do you AGREE with the following statements?</h2>\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "The following VIDEO ANNOTATION & DISCUSSION BOARD features are USEFUL for my learning and growth:",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "PUV1",
         text: "157. Opportunity to collaboratively view and discuss new SS videos/topics"
        },
        {
         value: "PUV2",
         text: "158. Exposure to a variety of interesting and provocative issues"
        },
        {
         value: "PUV3",
         text: "159. Using DISCUSSION FRAMES to guide me in posting annotations/comments/replies"
        },
        {
         value: "PUV4",
         text: "160. POPOVERS (having hints and prompt questions to guide me in posting annotations/comments/replies)"
        },
        {
         value: "PUV5",
         text: "161. MAKE ANNOTATIONS on videos using CoVAA"
        },
        {
         value: "PUV6",
         text: "162. POST COMMENTS on CoVAA discussion board"
        },
        {
         value: "PUV7",
         text: "163. REPLY to friends' comments on CoVAA discussion board"
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page20",
   elements: [
    {
     type: "panel",
     name: "panel19",
     elements: [
      {
       type: "html",
       name: "question11",
       html: "<h2>How much do you AGREE with the following statements?</h2>\n\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "The following LEARNING DASHBOARD features are USEFUL for my learning and growth:",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "PULD1",
         text: "164. Learning data on MY DISCUSSION FRAMES USAGE"
        },
        {
         value: "PULD2",
         text: "165. Learning data on MY CoVAA DISCUSSION NETWORK"
        },
        {
         value: "PULD3",
         text: "166. Learning data on MY 21CC PROFILE"
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page21",
   elements: [
    {
     type: "panel",
     name: "panel20",
     elements: [
      {
       type: "html",
       name: "question12",
       html: "<h2>How much do you AGREE with the following statements?</h2>\n\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "My Learning Dashboard...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "EOU5LD",
         text: "167. is clear and understandable."
        },
        {
         value: "PULD4",
         text: "168. helps me understand my SS learning attitudes and behaviours better."
        },
        {
         value: "PULD5",
         text: "169. can help me to improve my SS learning attitudes and behaviours."
        },
        {
         value: "PULD6",
         text: "170. helps me to evaluate and expand my use of DISCUSSION FRAMES."
        },
        {
         value: "PULD7",
         text: "171. helps me to track my participation in discussions of SS topics on CoVAA."
        },
        {
         value: "PULD8",
         text: "172. motivates me to increase my participation in discussions of SS topics on CoVAA."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page22",
   elements: [
    {
     type: "panel",
     name: "panel21",
     elements: [
      {
       type: "html",
       name: "question13",
       html: "<h2>How much do you AGREE with the following statements?</h2>\n\n"
      },
      {
       type: "matrix",
       name: "surveyQuestions",
       title: "My Learning Dashboard...",
       columns: [
        {
         value: "1",
         text: "Strongly DISAGREE"
        },
        {
         value: "2",
         text: "DISAGREE"
        },
        {
         value: "3",
         text: "Somewhat DISAGREE"
        },
        {
         value: "4",
         text: "Neutral"
        },
        {
         value: "5",
         text: "Somewhat AGREE"
        },
        {
         value: "6",
         text: "AGREE"
        },
        {
         value: "7",
         text: "Strongly AGREE"
        }
       ],
       rows: [
        {
         value: "PULD9",
         text: "173. shows me who I can interact more with in SS discussions on CoVAA."
        },
        {
         value: "PULD10",
         text: "174. helps me understand my strengths and weaknesses in SS better."
        },
        {
         value: "PULD11",
         text: "175. shows me where I can improve or what I need more practice on."
        },
        {
         value: "PULD12",
         text: "176. shows me who I can reach out to for help in SS."
        },
        {
         value: "PULD13",
         text: "177. shows me whom I can help in SS."
        }
       ],
       isAllRowRequired: true
      }
     ]
    }
   ]
  },
  {
   name: "page23",
   elements: [
    {
     type: "panel",
     name: "panel6",
     elements: [
      {
       type: "html",
       name: "question2",
       html: "<h2>CoVAA allows you to view teacher-selected videos and respond to your teachers' questions via ADD ANNOTATION and ADD COMMENTS (discussion board). It also enables you to read, download and respond to your classmates' annotations and comments. </h2>\n"
      }
     ]
    },
    {
     type: "comment",
     name: "openQuestion1",
     title: "178. What aspects of CoVAA did you find to be useful for your learning, and why? (Please list as many as possible)",
     isRequired: true
    }
   ]
  },
  {
   name: "page24",
   elements: [
    {
     type: "panel",
     name: "panel9",
     elements: [
      {
       type: "html",
       name: "question3",
       html: "<h2>CoVAA allows you to view teacher-selected videos and respond to your teachers' questions via ADD ANNOTATION and ADD COMMENTS (discussion board). It also enables you to read, download and respond to your classmates' annotations and comments. </h2>\n"
      }
     ]
    },
    {
     type: "comment",
     name: "openQuestion2",
     title: "179. What suggestions do you have on how CoVAA can be IMPROVED?",
     isRequired: true
    }
   ]
  },
  {
   name: "page25",
   elements: [
    {
     type: "panel",
     name: "panel12",
     elements: [
      {
       type: "html",
       name: "question5",
       html: "<h2>CoVAA allows you to view teacher-selected videos and respond to your teachers' questions via ADD ANNOTATION and ADD COMMENTS (discussion board). It also enables you to read, download and respond to your classmates' annotations and comments. </h2>\n"
      }
     ]
    },
    {
     type: "comment",
     name: "openQuestion3",
     title: "180. What were the top 3 reasons that motivated you to use CoVAA at home/outside lesson time?",
     isRequired: true
    }
   ]
  },
  {
   name: "page26",
   elements: [
    {
     type: "panel",
     name: "panel13",
     elements: [
      {
       type: "html",
       name: "question7",
       html: "<h2>CoVAA allows you to view teacher-selected videos and respond to your teachers' questions via ADD ANNOTATION and ADD COMMENTS (discussion board). It also enables you to read, download and respond to your classmates' annotations and comments. </h2>\n"
      }
     ]
    },
    {
     type: "comment",
     name: "openQuestion4",
     title: "181. What were the top 3 reasons you did not use CoVAA at home/outside lesson time?",
     isRequired: true
    }
   ]
  }
 ],
 sendResultOnPageNext: true,
 showTitle: false,
 showQuestionNumbers: "off",
 showProgressBar: "bottom",
 requiredText: ""
}
	}
	
	var survey = new Survey.Model(surveyJSON);
	survey.data=values;
	$("#surveyContainer").Survey({
		model:survey,
		onComplete:sendDataToServer,
		onPartialSend: sendDataToServer1
	});
</script>

<?php //get_template_part('page-parts/general-after-wrap'); ?>

<?php //get_footer(); 

/* Omit closing PHP tag to avoid "Headers already sent" issues. */