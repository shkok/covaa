function round(value, decimals) {
	return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}
var selectedSubject = "";
var selectedClass = "";
var selectedClassName = "";
//var userID = "<?php echo $userID;?>";
var selectedStudents;
var selectedVideo;
var sna_links;
var sna_weights;
var sna_strengths;
var total_students_count = 0;
var logged_in = 0;
var never_logged_in = 0;
	
var watched_video = 0;
var never_watched_video = 0;
var comments = 0;
var annotations = 0;
var replies = 0;

//thinking skills
var ideate =0; 
var justify =0; 
var agree = 0; 
var disagree = 0
var inquire = 0

//critical lens
var message =0; 
var purpose =0; 
var audience =0;
var viewpoint = 0; 
var assumption = 0
var inference = 0
var impact = 0
var evidence = 0
var totalqnsNo=0;
var completedQnsTracker = [];

/* 21CC Profile variables*/
var SelfEfficacy = 0;
var TaskValues = 0;
var Behave_Cog_EmoEngagement = 0;
var AcadSelfConcept = 0;
var TopSelfEfficacy = 0;
var TopTaskValues = 0;
var TopBehave_Cog_EmoEngagement = 0;
var TopAcadSelfConcept = 0;
var AvgSelfEfficacy = 0;
var AvgTaskValues = 0;
var AvgBehave_Cog_EmoEngagement = 0;
var AvgAcadSelfConcept = 0;
var Creativity = 0;
var Curiosity = 0;
var CriticalThinking = 0;
var Collaboration = 0;
var SelfDirectedLearning = 0;
var TopCreativity = 0;
var TopCuriosity = 0;
var TopCriticalThinking = 0;
var TopCollaboration = 0;
var TopSelfDirectedLearning = 0;
var AvgCreativity = 0;
var AvgCuriosity = 0;
var AvgCriticalThinking = 0;
var AvgCollaboration = 0;
var AvgSelfDirectedLearning = 0;
var PerformanceGoals = 0;
var LearningGoals = 0;
var SocialGoal = 0;
var SurfaceLearning =0;
var DeepLearning =0;

var PostSelfEfficacy = 0;
var PostTaskValues = 0;
var PostBehave_Cog_EmoEngagement = 0;
var PostAcadSelfConcept = 0;
var PostTopSelfEfficacy = 0;
var PostTopTaskValues = 0;
var PostTopBehave_Cog_EmoEngagement = 0;
var PostTopAcadSelfConcept = 0;
var PostAvgSelfEfficacy = 0;
var PostAvgTaskValues = 0;
var PostAvgBehave_Cog_EmoEngagement = 0;
var PostAvgAcadSelfConcept = 0;
var PostCreativity = 0;
var PostCuriosity = 0;
var PostCriticalThinking = 0;
var PostCollaboration = 0;
var PostSelfDirectedLearning = 0;
var PostTopCreativity = 0;
var PostTopCuriosity = 0;
var PostTopCriticalThinking = 0;
var PostTopCollaboration = 0;
var PostTopSelfDirectedLearning = 0;
var PostAvgCreativity = 0;
var PostAvgCuriosity = 0;
var PostAvgCriticalThinking = 0;
var PostAvgCollaboration = 0;
var PostAvgSelfDirectedLearning = 0;
var PostTopPerformanceGoals = 0;
var PostAvgPerformanceGoals = 0;
var PostPerformanceGoals = 0;
var PostTopLearningGoals = 0;
var PostAvgLearningGoals = 0;
var PostLearningGoals = 0;
var PostAvgSocialGoal = 0;
var PostTopSocialGoal = 0;
var PostSocialGoal = 0;
var PostTopSurfaceLearning =0;
var PostAvgSurfaceLearning =0;
var PostSurfaceLearning =0;
var PostDeepLearning =0;
var PostTopDeepLearning =0;
var PostAvgDeepLearning =0;
/* For 21cc slider chart look and feel */
var twoColComp = {
  init: function (){
    var tables = document.getElementsByClassName('slider-chart');
    // for each table
    for(var i = 0; i < tables.length; i++) {
      // don't process one that's already been done (has class two-column-comp)
      if (new RegExp('(^| )two-column-comp( |$)', 'gi').test(tables[i].className)){
         return;
      }
      //TODO: need to verify cross-browser support of these vars
      var h = tables[i].clientHeight, 
          t = tables[i].getBoundingClientRect().top,
          wT = window.pageYOffset || document.documentElement.scrollTop,
          wH = window.innerHeight;
      if(wT + wH > t + h/2){
         this.make(tables[i]);
       }
    }
  },
  
  make : function(el){
    var rows = el.getElementsByTagName('tr'),
        vals = [],
        max,
        percent;
    // for each row in the table, get vals
    for(var x = 0; x < rows.length; x++) {
      var cells = rows[x].getElementsByTagName('td');
      for(var y = 1; y < cells.length; y++){
        vals.push(parseFloat(cells[y].innerHTML, 10));
      }
    }
    //max = Math.max.apply( Math, vals );
    max = 7;
    percent = 100/max;
    //for each row in the table, apply vals
    for(x = 0; x < rows.length; x++) {
      var cells = rows[x].getElementsByTagName('td');
      for(var y = 1; y < cells.length; y++){
        var currNum = parseFloat(cells[y].innerHTML, 10);
        cells[y].style.backgroundSize = percent * currNum + "% 100%";
        cells[y].style.transitionDelay = x/20 + "s";
      } 
    }
    //add a class so you dont process it a bunch of times
    el.className = "two-column-comp";
  } // end make
}
/* End For 21cc slider chart look and feel */

$(document).ready(function(){

	/*Reflections module code start*/
	//HL
	$(function() {
	    $("#lightbox-popup").draggable();
	});

	// 1. when id=reflection is clicked
	$(".reflection").click(function() {

		var chart_id = $(this).data('seq');
		$("#chart_id").val(chart_id);

		var videoID = "survey";
		var chart_selection = "";
		var showAllTextAreas = true;

		if(chart_id == "learningSS")
			chart_selection = "REFLECT & GOAL SET : MY LEARNING ATTITUDES";
		else if(chart_id == "motivation")
			chart_selection = "REFLECT & GOAL SET : MY LEARNING MOTIVATIONS";
		else if(chart_id == "21CC")
			chart_selection = "REFLECT & GOAL SET : MY 21ST CENTURY SKILLS PROFILE";
		else if(chart_id == "strategies")
			chart_selection = "REFLECT & GOAL SET : MY LEARNING STRATEGIES";
		else if(chart_id == "discussion"){
			chart_selection = "REFLECT & GOAL SET : MY DISCUSSION FRAMES USAGE";
			videoID = $('#videoSelect').val();
		}
		else if(chart_id == "network")
		{
			chart_selection = "REFLECT & GOAL SET: MY DISCUSSION NETWORK";
			showAllTextAreas = false;
			$("#good").val("NA");
			$("#helpMe").val("NA");
			videoID = $('#videoSelect').val();
		}

		$("#video_id").val(videoID);

		//var chart_selection = $(this).text();
		//var chart_selection = $(this).data('title');

		$.ajax({
			type: "POST",
			url: 'reflection_handler.php',
			data: {
				request: "fetchReflection",
				chart_id: chart_id, 
				user_id: userID,
				video_id: videoID
			},
			success: function (response) {
				var data_array = $.parseJSON(response);
				console.log(data_array);
				$("#lightbox-bg").show();
				$("#lightbox-popup").show();
				$("#reflect_title").html(chart_selection);

				if(!showAllTextAreas) 
				{
					$('.nonSNA').hide();
					$('#label_improve').html("How can I improve my discussion connections with others in class?");
					$('#label_helpOthers').html("How can I help others be more involved in the discussion?");

				}
				else 
				{
					$('.nonSNA').show();
					$('#label_improve').html("How can I improve?");
					$('#label_helpOthers').html("How can I help others?");
					$("#good").val(data_array[0]);
					$("#helpMe").val(data_array[3]);
				}
				
				$("#helpOthers").val(data_array[1]);
				$("#improve").val(data_array[2]);
				
			}
		});
	});
 
    // 2. when form is submit 
    $('form').submit(function() {
 
		var good = $("#good");
		var helpOthers = $("#helpOthers");
		var improve = $("#improve");
		var helpMe = $("#helpMe");

		if (good.val().length > 0 && helpOthers.val().length > 0 && improve.val().length > 0 && helpMe.val().length > 0) {
			$.post(
				$(this).attr('action'), 
				$(this).serialize(), 
				function(response){
					console.log(response);
				},'json');

			$("#lightbox-bg").hide();
			$("#lightbox-popup").hide();
		}
		else {
			alert("Please complete the form.");
		}

		return false;
    });
 
    // 3. when id=lightbox-close is clicked
    $("#lightbox-close").click(function() {
		$("#lightbox-bg").hide();
		$("#lightbox-popup").hide();
    });
    /*End of Reflections module code*/

	 $(window).scroll(function () {
		if ($(this).scrollTop() > 10) {
			$('#back-to-top').fadeIn();
		} else {
			$('#back-to-top').fadeOut();
		}
	});
	// scroll body to 0px on click
	$('#back-to-top').click(function () {
		$('#back-to-top').tooltip('hide');
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	
	$('#back-to-top').tooltip('show');
	
	//Event listener to identify clicking of the SNA tab.
	$("#sidebar").on('shown.bs.tab', function (e) { 
		console.log("target: " + e.target.id); 
		//if SNA tab is clicked, only then #sna_container "exists", then sna can be drawn.
		if(e.target.id == "sna_tab")
			drawSNAChart();
	});
	
	$("#btn_sna_generate").click(function(){
		$('#sna_container').html("<h3 style='text-align:center;margin:50% 0'>Redrawing...</h3>");
		drawSNAChart();
	});
	//Populating Subject/Class/Video Dropdown on default
	var subjectOptions = '';
	
	$.each( overall_data, function( i, l ){
		subjectOptions += '<option value="'+ overall_data[i]["id"] + '">' + overall_data[i]["name"] + '</option>';
	});
	$('#subjectSelect').append(subjectOptions);
	$("#subjectSelect option:last").prop("selected", "selected");
	
	selectedSubject = $("#subjectSelect").val();
	var classOptions = '<option value="">Choose a Class</option>';
			
	$.each( overall_data[selectedSubject]["classes"], function( i, l ){
		classOptions += '<option value="'+ overall_data[selectedSubject]["classes"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][i]["name"] + '</option>';
	});
	$('#classSelect').empty().append(classOptions);
	$("#classSelect option:last").prop("selected", "selected");
	
	selectedClass = $('#classSelect').val();
	var videoOptions = '<option value="">Choose a Video</option><option value="overall">Overall</option>';
			
	$.each( overall_data[selectedSubject]["classes"][selectedClass]["videos"], function( i, l ){
		videoOptions += '<option value="'+ overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["name"] + '</option>';
	});
	
	$('#videoSelect').empty().append(videoOptions);
	$("#videoSelect option:eq(1)").prop("selected", "selected");
	drawAllCharts();
	//End of populated subject/class/video dropdowns on default
	
	
	//Populate Class Dropdown when Subject chosen
	$('#subjectSelect').change(function(){
		selectedSubject = $("#subjectSelect").val();
		
		$('#videoLabel').fadeOut();
			$('#videoSelect').fadeOut();
		
		if(selectedSubject === "")
		{
			$('#classLabel').fadeOut();
			$('#classSelect').fadeOut();
		}
		else
		{
			var classOptions = '<option value="">Choose a Class</option>';
			
			$.each( overall_data[selectedSubject]["classes"], function( i, l ){
				classOptions += '<option value="'+ overall_data[selectedSubject]["classes"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][i]["name"] + '</option>';
			});
			$('#classSelect').empty().append(classOptions);
			
			$('#classLabel').fadeIn();
			$('#classSelect').fadeIn();
		}
	});
	
	//Populate Video Dropdown when class chosen
	$('#classSelect').change(function(){
		selectedClass = $('#classSelect').val();
		
		if(selectedClass === "")
		{
			$('#videoLabel').fadeOut();
			$('#videoSelect').fadeOut();
		}
		else
		{
			var videoOptions = '<option value="">Choose a Video</option>';
			
			$.each( overall_data[selectedSubject]["classes"][selectedClass]["videos"], function( i, l ){
				videoOptions += '<option value="'+ overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["name"] + '</option>';
			});
			
			$('#videoSelect').empty().append(videoOptions);
			
			$('#videoLabel').fadeIn();
			$('#videoSelect').fadeIn();
		}
	});				
	
	//Key function that tabulates the visualizations values
	$('#videoSelect').change(function(){drawAllCharts()});
/*	window.onload = function(){
	  twoColComp.init();
	}
	window.onscroll = function(){
	  twoColComp.init();
	}*/
	function drawAllCharts()
	{
		//Get selected video's ID. Can be "Overall" as well.
		selectedVideoID = $('#videoSelect').val();
		totalqnsNo=0;
		selectedStudents = overall_data[selectedSubject]["classes"][selectedClass]["users"];
		allVideos = overall_data[selectedSubject]["classes"][selectedClass]["videos"];
		selectedClassName = overall_data[selectedSubject]["classes"][selectedClass]["name"];
		var qnsNo;
		
		
		$.each(allVideos, function(i, l){
				 totalqnsNo += parseInt(allVideos[i]["qnsNo"]);
			});
		
		//<button id='btn_sna_generate'>Reset View / Generate</button>
		/*$( "#vidann" ).empty();
			
			$( "#vidann" ).append( "<div class=\"grid_title\">My Videos & Annotations</div><hr>"+
													"<div class=\"grid_chart_o\">"+
													"<div id=\"video_chart1\"></div>"+
													"<div id=\"eachVideo_chart1\"></div>"+
													"</div>"+
													"<div class=\"grid_chart_o\" style=\"float: right;\">"+
														"<div id=\"annotations_chart1\"></div>"+
														"<div id=\"annotationQns_chart1\"></div>"+
													"</div>");*/
		if(selectedVideoID != "overall")
		{
			//Set the selectVideo array based on selected video id. Contains selected video's weights, strength and social links
			selectedVideo = overall_data[selectedSubject]["classes"][selectedClass]["videos"][selectedVideoID];
			qnsNo = parseInt(overall_data[selectedSubject]["classes"][selectedClass]["videos"][selectedVideoID]["qnsNo"]);
			
			/*$( "#vidann" ).empty();
			
			$( "#vidann" ).append( "<div class=\"grid_title\">My Videos & Annotations</div><hr>"+
									"<div class=\"grid_chart\">"+
									"<div id=\"annotations_chart\"></div>"+
														"</div>");*/
		}
		else
		{
			
			selectedVideo = overall_data[selectedSubject]["classes"][selectedClass];//Class overall weights, strengths, social links are located here
			
			
		}
		//Reset Global Variables
		annotations = 0;
		comments = 0;
		replies = 0;
		//thinking skills
		ideate =0; 
		justify =0; 
		agree = 0; 
		disagree = 0
		inquire = 0
		
		//critical lens
		message =0; 
		purpose =0; 
		audience =0;
		viewpoint = 0; 
		assumption = 0
		inference = 0
		impact = 0
		evidence = 0

		//21CC Profile Variables
		SelfEfficacy =0;
		TaskValues = 0;
		Behave_Cog_EmoEngagement = 0;
		AcadSelfConcept = 0;
		TopSelfEfficacy = 0;
		TopTaskValues = 0;
		TopBehave_Cog_EmoEngagement = 0;
		TopAcadSelfConcept = 0;
		AvgSelfEfficacy = 0;
		AvgTaskValues = 0;
		AvgBehave_Cog_EmoEngagement = 0;
		AvgAcadSelfConcept = 0;
		Creativity = 0;
		Curiosity = 0;
		CriticalThinking =0;
		Collaboration = 0;
		SelfDirectedLearning = 0;
		TopCreativity = 0;
		TopCuriosity = 0;
		TopCriticalThinking=0;
		TopCollaboration = 0;
		TopSelfDirectedLearning = 0;
		AvgCreativity = 0;
		AvgCuriosity = 0;
		AvgCriticalThinking=0;
		AvgCollaboration = 0;
		AvgSelfDirectedLearning = 0;
		PerformanceGoals = 0;
		LearningGoals = 0;
		SocialGoal = 0;
		SurfaceLearning =0;
		DeepLearning =0;
		TopPerformanceGoals = 0;
		TopLearningGoals = 0;
		TopSocialGoal = 0;
		TopSurfaceLearning =0;
		TopDeepLearning =0;
		AvgPerformanceGoals = 0;
		AvgLearningGoals = 0;
		AvgSocialGoal = 0;
		AvgSurfaceLearning =0;
		AvgDeepLearning =0;
		
		PostSelfEfficacy = 0;
		PostTaskValues = 0;
		PostBehave_Cog_EmoEngagement = 0;
		PostAcadSelfConcept = 0;
		PostTopSelfEfficacy = 0;
		PostTopTaskValues = 0;
		PostTopBehave_Cog_EmoEngagement = 0;
		PostTopAcadSelfConcept = 0;
		PostAvgSelfEfficacy = 0;
		PostAvgTaskValues = 0;
		PostAvgBehave_Cog_EmoEngagement = 0;
		PostAvgAcadSelfConcept = 0;
		PostCreativity = 0;
		PostCuriosity = 0;
		PostCriticalThinking = 0;
		PostCollaboration = 0;
		PostSelfDirectedLearning = 0;
		PostTopCreativity = 0;
		PostTopCuriosity = 0;
		PostTopCriticalThinking = 0;
		PostTopCollaboration = 0;
		PostTopSelfDirectedLearning = 0;
		PostAvgCreativity = 0;
		PostAvgCuriosity = 0;
		PostAvgCriticalThinking = 0;
		PostAvgCollaboration = 0;
		PostAvgSelfDirectedLearning = 0;
		PostPerformanceGoals = 0;
		PostLearningGoals = 0;
		PostSocialGoal = 0;
		PostSurfaceLearning =0;
		PostDeepLearning =0;
		
		var length= 0;
		var length1= 0;

	$.each(allsurvey, function( i, l ){
		if(selectedClassName == allsurvey[i]["class"]){
			length++;
			if (parseFloat(allsurvey[i]["cal"]["SelfEfficacy"]) > TopSelfEfficacy){
				TopSelfEfficacy = parseFloat(allsurvey[i]["cal"]["SelfEfficacy"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["TaskValues"]) > TopTaskValues){
				TopTaskValues = parseFloat(allsurvey[i]["cal"]["TaskValues"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["Behave_Cog_EmoEngagement"])> TopBehave_Cog_EmoEngagement){
				TopBehave_Cog_EmoEngagement = parseFloat(allsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["AcadSelfConcept"])> TopAcadSelfConcept){
				TopAcadSelfConcept = parseFloat(allsurvey[i]["cal"]["AcadSelfConcept"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["Creativity"]) > TopCreativity){
				TopCreativity = parseFloat(allsurvey[i]["cal"]["Creativity"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["Curiosity"]) > TopCuriosity){
				TopCuriosity = parseFloat(allsurvey[i]["cal"]["Curiosity"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["CriticalThinking"])> TopCriticalThinking){
				TopCriticalThinking = parseFloat(allsurvey[i]["cal"]["CriticalThinking"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["Collaboration"])> TopCollaboration){
				TopCollaboration = parseFloat(allsurvey[i]["cal"]["Collaboration"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["SelfDirectedLearning"])> TopSelfDirectedLearning){
				TopSelfDirectedLearning = parseFloat(allsurvey[i]["cal"]["SelfDirectedLearning"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["PerformanceGoals"]) > TopPerformanceGoals){
				TopPerformanceGoals = parseFloat(allsurvey[i]["cal"]["PerformanceGoals"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["LearningGoals"]) > TopLearningGoals){
				TopLearningGoals = parseFloat(allsurvey[i]["cal"]["LearningGoals"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["SocialGoal"])> TopSocialGoal){
				TopSocialGoal = parseFloat(allsurvey[i]["cal"]["SocialGoal"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["SurfaceLearning"])> TopSurfaceLearning){
				TopSurfaceLearning = parseFloat(allsurvey[i]["cal"]["SurfaceLearning"]); 
			}
			if (parseFloat(allsurvey[i]["cal"]["DeepLearning"])> TopDeepLearning){
				TopDeepLearning = parseFloat(allsurvey[i]["cal"]["DeepLearning"]); 
			}
			
			AvgSelfEfficacy += parseFloat(allsurvey[i]["cal"]["SelfEfficacy"]);
			AvgTaskValues += parseFloat(allsurvey[i]["cal"]["TaskValues"]); 
			AvgBehave_Cog_EmoEngagement += parseFloat(allsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]); 
			AvgAcadSelfConcept += parseFloat(allsurvey[i]["cal"]["AcadSelfConcept"]); 
			
			AvgCreativity += parseFloat(allsurvey[i]["cal"]["Creativity"]);
			AvgCuriosity += parseFloat(allsurvey[i]["cal"]["Curiosity"]); 
			AvgCriticalThinking += parseFloat(allsurvey[i]["cal"]["CriticalThinking"]); 
			AvgCollaboration += parseFloat(allsurvey[i]["cal"]["Collaboration"]); 
			AvgSelfDirectedLearning += parseFloat(allsurvey[i]["cal"]["SelfDirectedLearning"]); 
			
			AvgPerformanceGoals += parseFloat(allsurvey[i]["cal"]["PerformanceGoals"]);
			AvgLearningGoals += parseFloat(allsurvey[i]["cal"]["LearningGoals"]); 
			AvgSocialGoal+= parseFloat(allsurvey[i]["cal"]["SocialGoal"]); 
			AvgSurfaceLearning += parseFloat(allsurvey[i]["cal"]["SurfaceLearning"]); 
			AvgDeepLearning += parseFloat(allsurvey[i]["cal"]["DeepLearning"]); 
		
		}
		
	});
		
	AvgSelfEfficacy = AvgSelfEfficacy/length;
	AvgTaskValues = AvgTaskValues/length;
	AvgBehave_Cog_EmoEngagement = AvgBehave_Cog_EmoEngagement/length;
	AvgAcadSelfConcept = AvgAcadSelfConcept/length;
	AvgCreativity = AvgCreativity/length;
	AvgCuriosity = AvgCuriosity/length;
	AvgCriticalThinking = AvgCriticalThinking/length;
	AvgCollaboration = AvgCollaboration/length;
	AvgSelfDirectedLearning = AvgSelfDirectedLearning/length;
	
	AvgPerformanceGoals = AvgPerformanceGoals/length;
	AvgLearningGoals = AvgLearningGoals/length;
	AvgSocialGoal= AvgSocialGoal/length;
	AvgSurfaceLearning = AvgSurfaceLearning/length;
	AvgDeepLearning = AvgDeepLearning/length;
	
	//post result
	$.each(allpostsurvey, function( i, l ){
		if(selectedClassName == allpostsurvey[i]["class"]){
			length1++;
			if (parseFloat(allpostsurvey[i]["cal"]["SelfEfficacy"]) > PostTopSelfEfficacy){
				PostTopSelfEfficacy = parseFloat(allpostsurvey[i]["cal"]["SelfEfficacy"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["TaskValues"]) > PostTopTaskValues){
				PostTopTaskValues = parseFloat(allpostsurvey[i]["cal"]["TaskValues"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Behave_Cog_EmoEngagement"])> PostTopBehave_Cog_EmoEngagement){
				PostTopBehave_Cog_EmoEngagement = parseFloat(allpostsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["AcadSelfConcept"])> PostTopAcadSelfConcept){
				PostTopAcadSelfConcept = parseFloat(allpostsurvey[i]["cal"]["AcadSelfConcept"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Creativity"]) > PostTopCreativity){
				PostTopCreativity = parseFloat(allpostsurvey[i]["cal"]["Creativity"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Curiosity"]) > PostTopCuriosity){
				PostTopCuriosity = parseFloat(allpostsurvey[i]["cal"]["Curiosity"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["CriticalThinking"])> PostTopCriticalThinking){
				PostTopCriticalThinking = parseFloat(allpostsurvey[i]["cal"]["CriticalThinking"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["Collaboration"])> PostTopCollaboration){
				PostTopCollaboration = parseFloat(allpostsurvey[i]["cal"]["Collaboration"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["SelfDirectedLearning"])> PostTopSelfDirectedLearning){
				PostTopSelfDirectedLearning = parseFloat(allpostsurvey[i]["cal"]["SelfDirectedLearning"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["PerformanceGoals"]) > PostTopPerformanceGoals){
				PostTopPerformanceGoals = parseFloat(allpostsurvey[i]["cal"]["PerformanceGoals"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["LearningGoals"]) > PostTopLearningGoals){
				PostTopLearningGoals = parseFloat(allpostsurvey[i]["cal"]["LearningGoals"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["SocialGoal"])> PostTopSocialGoal){
				PostTopSocialGoal = parseFloat(allpostsurvey[i]["cal"]["SocialGoal"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["SurfaceLearning"])> PostTopSurfaceLearning){
				PostTopSurfaceLearning = parseFloat(allpostsurvey[i]["cal"]["SurfaceLearning"]); 
			}
			if (parseFloat(allpostsurvey[i]["cal"]["DeepLearning"])> PostTopDeepLearning){
				PostTopDeepLearning = parseFloat(allpostsurvey[i]["cal"]["DeepLearning"]); 
			}
			
			PostAvgSelfEfficacy += parseFloat(allpostsurvey[i]["cal"]["SelfEfficacy"]);
			PostAvgTaskValues += parseFloat(allpostsurvey[i]["cal"]["TaskValues"]); 
			PostAvgBehave_Cog_EmoEngagement += parseFloat(allpostsurvey[i]["cal"]["Behave_Cog_EmoEngagement"]); 
			PostAvgAcadSelfConcept += parseFloat(allpostsurvey[i]["cal"]["AcadSelfConcept"]); 
			
			PostAvgCreativity += parseFloat(allpostsurvey[i]["cal"]["Creativity"]);
			PostAvgCuriosity += parseFloat(allpostsurvey[i]["cal"]["Curiosity"]); 
			PostAvgCriticalThinking += parseFloat(allpostsurvey[i]["cal"]["CriticalThinking"]); 
			PostAvgCollaboration += parseFloat(allpostsurvey[i]["cal"]["Collaboration"]); 
			PostAvgSelfDirectedLearning += parseFloat(allpostsurvey[i]["cal"]["SelfDirectedLearning"]); 
			
			PostAvgPerformanceGoals += parseFloat(allpostsurvey[i]["cal"]["PerformanceGoals"]);
			PostAvgLearningGoals += parseFloat(allpostsurvey[i]["cal"]["LearningGoals"]); 
			PostAvgSocialGoal+= parseFloat(allpostsurvey[i]["cal"]["SocialGoal"]); 
			PostAvgSurfaceLearning += parseFloat(allpostsurvey[i]["cal"]["SurfaceLearning"]); 
			PostAvgDeepLearning += parseFloat(allpostsurvey[i]["cal"]["DeepLearning"]); 
			
			
		
		}
		
	});
	PostAvgSelfEfficacy = PostAvgSelfEfficacy/length1;
	PostAvgTaskValues = PostAvgTaskValues/length1;
	PostAvgBehave_Cog_EmoEngagement = PostAvgBehave_Cog_EmoEngagement/length1;
	PostAvgAcadSelfConcept = PostAvgAcadSelfConcept/length1;
	PostAvgCreativity = PostAvgCreativity/length1;
	PostAvgCuriosity = PostAvgCuriosity/length1;
	PostAvgCriticalThinking = PostAvgCriticalThinking/length1;
	PostAvgCollaboration = PostAvgCollaboration/length1;
	PostAvgSelfDirectedLearning = PostAvgSelfDirectedLearning/length1;
	
	PostAvgPerformanceGoals = PostAvgPerformanceGoals/length1;
	PostAvgLearningGoals = PostAvgLearningGoals/length1;
	PostAvgSocialGoal= PostAvgSocialGoal/length1;
	PostAvgSurfaceLearning = PostAvgSurfaceLearning/length1;
	PostAvgDeepLearning = PostAvgDeepLearning/length1;
	
		//completedQnsTracker = [];
			
		//console.log("qnsNo: "+ qnsNo);
		
		/*for(var i=0; i<qnsNo; i++)//Question number starts from 1, thus the +1
		{
			completedQnsTracker[i] = 0;//Setup each question as 0 student answered
		}*/
		
		//console.log("completedQnsTracker: "+completedQnsTracker);
		
		/*$.each(selectedStudents, function(i, l)
		{
			//var completedAnnotations = [];
			//console.log(selectedStudents[i]["id"]);
			//console.log(selectedStudents[i]);
			$.each(selectedStudents[i], function(x, y){
			if(selectedStudents[i][x]["id"] == <?php echo $userID;?>)//If its the current logged in user
			{
				$.each(selectedStudents[i][x]["submissions"], function(j, k)//Loop through the current user's submissions
				{
					//**********************************************************
					//TODO: Check if overall. If overall, Video ID don't matter.
					//**********************************************************
					var video_id = selectedStudents[i][x]["submissions"][j]["video_id"];
					
					if(video_id === selectedVideo["id"])//If found a submission belonging to the current use for selected video
					{
						
						var questionNum = parseInt(selectedStudents[i][x]["submissions"][j]["qnsNo"]);
						var parent_id = selectedStudents[i][x]["submissions"][j]["parent_id"];
						var child_id = selectedStudents[i][x]["submissions"][j]["child_id"];
						
						var lens= selectedStudents[i][x]["submissions"][j]["tags"];
						var skills= selectedStudents[i][x]["submissions"][j]["commentType"];
						if(questionNum > 0 && questionNum <= qnsNo)//If its an annotation
						{
							selectedStudents[i][x]["annotations"] += 1;
							annotations++;
						}
						else if(parent_id !== null)//else if it is a general reply
						{selectedStudents[i][x]["replies"] += 1;	//To mark that this student has at least 1 reply
							replies++;
						}
						else if(questionNum === 0){//else if it is a general comment
							selectedStudents[i][x]["comments"] += 1;//To mark that this student has at least 1 comment	 
							comments++;
						}
						
						//check critical lens used
						if(lens == "Message/Issue"){
							selectedStudents[i][x]["message"]++;
							message++; 
						}
						else if(lens == "Purpose"){
							selectedStudents[i][x]["purpose"]++;
							purpose++; 
						}
						else if(lens == "Audience"){
							selectedStudents[i][x]["audience"]++;
							audience++;
						}
						else if(lens == "Viewpoint"){
							selectedStudents[i][x]["viewpoint"]++;
							viewpoint++;
						}
						else if(lens == "Assumption"){
							selectedStudents[i][x]["assumption"]++;
							assumption++;
						}
						else if(lens == "Inference/Interpretation"){
							selectedStudents[i][x]["inference"]++;
							inference++;
						}
						else if(lens == "Consequences/Impact"){
							selectedStudents[i][x]["impact"]++;
							impact++;
						}
						else if(lens == "Evidence"){
							selectedStudents[i][x]["evidence"]++;
							evidence++;
						}
						
						//check thinking skills used
						if(skills == "Ideate/Reflect"){
							selectedStudents[i][x]["ideate"]++;
							ideate++; 
						}
						else if(skills == "Justify/Explain"){
							selectedStudents[i][x]["justify"]++;
							justify++; 
						}
						else if(skills == "Agree/Validate"){
							selectedStudents[i][x]["agree"]++;
							agree++;
						}
						else if(skills == "Disagree/Challenge"){
							selectedStudents[i][x]["disagree"]++;
							disagree++;
						}
						else if(skills == "Inquire/Clarify"){
							selectedStudents[i][x]["inquire"]++;
							inquire++;
						}
						
					}
				});//End of loop for each submission of current student
			
				if(completedAnnotations.length === qnsNo && qnsNo > 0)//If number of unique question annotation is the same as total number of questions asked
					selectedStudents[i]["completeAnnotations"] = 1;//To mark that this student has completed all annotation questions
				
				selectedStudents[i]["completedAnnotations"] = completedAnnotations;//To attach array to the student object
				
				
				if(selectedStudents[i]["login"] === 1)
					logged_in++;
				else
					never_logged_in++;
				
				if(selectedStudents[i]["viewedVideos"][selectedVideo["id"]])
					watched_video++;
				else
					never_watched_video++;
				
				if(selectedStudents[i]["annotations"] === 1)
					annotations++;
				if(selectedStudents[i]["comments"] === 1)
					comments++;
				if(selectedStudents[i]["replies"] === 1)
					replies++;
			}
			});
		});//End of loop for each student
		*/
		total_students_count = Object.keys(selectedStudents).length;
		
		/*console.log("annotations: "+annotations);
		console.log("comments: "+comments);
		console.log("replies: "+replies);*/
		
		/*console.log(selectedStudents);
		console.log("total_students_count: "+total_students_count);
		console.log("logged_in: "+logged_in);
		console.log("watched_video: "+watched_video);
		console.log("annotations: "+annotations);
		console.log("comments: "+comments);
		console.log("replies: "+replies);
		console.log("qnsNo: "+qnsNo);
		console.log("completedQnsTracker: "+completedQnsTracker);*/
		
		//google.charts.setOnLoadCallback(drawParticipationChart);
		//google.charts.setOnLoadCallback(drawViewVideosChart);
		//google.charts.setOnLoadCallback(drawCLChart);
		google.charts.setOnLoadCallback(drawTSChart);
		//google.charts.setOnLoadCallback(drawCLChart1);
		//google.charts.setOnLoadCallback(drawTSChart1);
		google.charts.setOnLoadCallback(drawAttitudeChart);
		//google.charts.setOnLoadCallback(drawFeelChart); //Not visualized in covaa trial 3
		google.charts.setOnLoadCallback(drawDesiresChart);		
		google.charts.setOnLoadCallback(draw21ccChart);
		google.charts.setOnLoadCallback(drawMindsetChart);
		/*google.charts.setOnLoadCallback(drawAnnotationChart);
		google.charts.setOnLoadCallback(drawAnnotationChart1);
		google.charts.setOnLoadCallback(drawAnnotationQnsChart1);
		
		google.charts.setOnLoadCallback(drawViewVideosChart1);
		google.charts.setOnLoadCallback(drawEachVideosChart1);*/
		drawSNAChart();
		
		if(qnsNo > 0)
			google.charts.setOnLoadCallback(drawAnnotationChart);	
		else
			$("#annotations_chart").html("There are no annotation questions.");
	}
});//End of document.ready

// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['corechart', 'bar']});
// Set a callback to run when the Google Visualization API is loaded.
 /*google.charts.setOnLoadCallback(drawParticipationChart);
google.charts.setOnLoadCallback(drawViewVideosChart);
google.charts.setOnLoadCallback(drawAnnotationChart);
google.charts.setOnLoadCallback(drawAnnotationQuestionsChart);
google.charts.setOnLoadCallback(drawCLChart);
google.charts.setOnLoadCallback(drawTSChart); */
// Set BAR chart options
var options = {
			   //colors:['#32d3e4','#e44332'],//colors:['#109618','#3366CC'],
			   colors:['#00ACEC','#FF1655'],
			   theme: 'material',
			   legend: {
					position: 'bottom', 
					cursor: 'pointer', 
					textStyle: {fontSize: 11}
				},
				animation:{
					startup: true,
					duration: 1000,
					easing: 'out',
				},
				//enableInteractivity: false,
				//isStacked: 'percent',
				//'chartArea': {'top': '-20','width': '100%', 'height': '60%'},
				hAxis: {
					gridlines: {
						count: 0,
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent'
				},
				vAxis: {
					gridlines: {
						color: 'transparent'
					},
					scaleType: 'linear',
					 format: '#,###',
					minValue: 0,
					baselineColor: 'transparent',
					
				},
				annotations: {
					alwaysOutside: false,
				  textStyle: {
					  //fontName: 'Times-Roman',
					  color: '#FFFFFF',
					  fontSize: 12,
					  bold: true,
					  auraColor: 'none',
					  offset: 100
					  /* ,
					  italic: true,
					  color: '#871b47',     // The color of the text.
					  auraColor: '#d799ae', // The color of the text outline.
					  opacity: 0.8          // The transparency of the text. */
					}
				}
			  };

function drawSNAChart()
{
	/*console.log("SNA Data");
	console.log(selectedVideoID);*/
	if(selectedVideoID == "overall")
	{
		//SNA drawing setup
		if(overall_data[selectedSubject]["classes"][selectedClass]["weights"]["top_weight"] == 0)//Check that the selected video has at least 1 comment. By checking the top weight is not zero. To prevent error when drawing sna
			$('#sna_container').html("<h3 style='text-align:center;margin:0 0;position: relative;top: 50%;transform: translateY(-50%); '>Start posting a comment to see yourself in the network.</h3>");
		else
		{
			$('#sna_container').empty();
			drawSNA(selectedVideo["weights"], 
					selectedVideo["strengths"], 
					overall_data[selectedSubject]["classes"][selectedClass]["users"][selectedVideoID], 
					selectedVideo["social_links"]);		
		}
	}
	else
	{
		if(overall_data[selectedSubject]["classes"][selectedClass]["videos"][selectedVideoID]["weights"]["top_weight"] == 0)//Check that the selected video has at least 1 comment. By checking the top weight is not zero. To prevent error when drawing sna
			$('#sna_container').html("<h3 style='text-align:center;margin:0 0;position: relative;top: 50%;transform: translateY(-50%); '>Start posting a comment to see yourself in the network.</h3>");
		else
		{
			$('#sna_container').empty();
			drawSNA(selectedVideo["weights"], 
					selectedVideo["strengths"], 
					overall_data[selectedSubject]["classes"][selectedClass]["users"][selectedVideoID], 
					selectedVideo["social_links"]);		
		}
	}
	//End of SNA drawing setup
}
function drawParticipationChart()
{		
	var data = google.visualization.arrayToDataTable([
			  ['', 'Annotation', 'Comment', 'Reply'],
			  ['Video', annotations, comments, replies],
			]);
	var chart = new google.visualization.ColumnChart(document.getElementById('participation_chart'));
	chart.draw(data, options);
	
	var qnsNo = parseInt(selectedVideo["qnsNo"]);
	var noComp = qnsNo - annotations;
	if (noComp <=0)
	{
		noComp =0;
	}
	var data1 = google.visualization.arrayToDataTable([
		['Question', 'No. of annotations responded', { role: 'annotation' }, 'No. of annotations NOT responded', { role: 'annotation' } ],
		['', annotations, annotations, noComp, noComp]
	  ]);
	var options1 = {
			   colors: ['#00ACEC','#FF1655'],//colors:['#32d3e4','#e44332'],//colors:['#109618','#3366CC'],
			   legend: {
					position: 'bottom', 
					cursor: 'pointer', 
					textStyle: {fontSize: 11}
				},
				animation:{
					startup: true,
					duration: 1000,
					easing: 'out',
				},
				isStacked: 'percent',
				'chartArea': {'width': '80%', 'height': '20%'},
				hAxis: {
					gridlines: {
						count: 0,
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent'
				},
				vAxis: {
					gridlines: {
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent' 
				},
				annotations: {
				  textStyle: {
					  //fontName: 'Times-Roman',
					  color: '#FFFFFF',
					  fontSize: 12,
					  bold: true,
					  auraColor: 'none' ,
					  /*italic: true,
					  color: '#871b47',     // The color of the text.
					  auraColor: '#d799ae', // The color of the text outline.
					  opacity: 0.8          // The transparency of the text. */
					}
				}
			  };
	var chart1 = new google.visualization.BarChart(document.getElementById('completion_chart'));
	chart1.draw(data1, options1);
	
}
function drawViewVideosChart() 
{				
}
function drawCLChart()
{
		message = selectedStudents[selectedVideoID][userID]["message"];
		purpose = selectedStudents[selectedVideoID][userID]["purpose"];
		audience = selectedStudents[selectedVideoID][userID]["audience"];
		viewpoint = selectedStudents[selectedVideoID][userID]["viewpoint"];
		assumption = selectedStudents[selectedVideoID][userID]["assumption"];
		inference = selectedStudents[selectedVideoID][userID]["inference"];
		impact =selectedStudents[selectedVideoID][userID]["impact"];
		evidence =selectedStudents[selectedVideoID][userID]["evidence"];
	Highcharts.chart('CL_chart', {
	    chart: {
	        polar: true,
	        type: 'line',
			width: 285
	    },
			exporting: {
					enabled: false
				},
				credits: {
					enabled: false
				},
	    title: {
					text: 'My <strong>CRITICAL LENS</strong> Usage Data'//,
					//style: { "font-weight": "bold" }
					//x: -80
				},
				subtitle: {
					text: 'How often did I apply the different critical lenses...?',
					style: {fontWeight: 'bold'}
				},
	    pane: {
	        size: '90%'
	    },
	    xAxis: {
	        categories: ['Message', 'Purpose', 'Audience', 'Point of View',
	                'Assumption', 'Inference', 'Impact', 'Evidence'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
					gridLineInterpolation: 'polygon',
					lineWidth: 0,
					allowDecimals: false,
					min: 0,
					tickInterval: 1
				},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
	    },
	    series: [{
	        name: 'Me',
	        data: [message, purpose, audience, viewpoint, assumption, inference, impact, evidence],
	        pointPlacement: 'on'
	    }]
	});
}
function drawTSChart()
{
	var selectedClassVideo = overall_data[selectedSubject]["classes"][selectedClass]["users"][selectedVideoID];
	var topIdeate = 0;
	var topJustify = 0;
	var topAgree = 0;
	var topDisagree = 0;
	var topInquire = 0;
	
	var avgIdeate = 0;
	var avgJustify = 0;
	var avgAgree = 0;
	var avgDisagree = 0;
	var avgInquire = 0;
	
	var length =0;
	//alert(length);
	$.each(selectedClassVideo, function( i, l ){
		if(selectedClassVideo[i]["role"] ==1){
			length++;
			if (selectedClassVideo[i]["ideate"] > topIdeate){
				topIdeate = selectedClassVideo[i]["ideate"];
			}
			if (selectedClassVideo[i]["justify"] > topJustify){
				topJustify = selectedClassVideo[i]["justify"];
			}
			if (selectedClassVideo[i]["agree"] > topAgree){
				topAgree = selectedClassVideo[i]["agree"];
			}
			if (selectedClassVideo[i]["disagree"] > topDisagree){
				topDisagree = selectedClassVideo[i]["disagree"];
			}
			if (selectedClassVideo[i]["inquire"] > topInquire){
				topInquire = selectedClassVideo[i]["inquire"];
			}
			
			avgIdeate += selectedClassVideo[i]["ideate"];
			avgJustify += selectedClassVideo[i]["justify"];
			avgAgree += selectedClassVideo[i]["agree"];
			avgDisagree += selectedClassVideo[i]["disagree"];
			avgInquire += selectedClassVideo[i]["inquire"];
		}
		
	});
		
		
		avgIdeate = avgIdeate/length;
		avgJustify = avgJustify/length;
		avgAgree = avgAgree/length;
		avgDisagree = avgDisagree/length;
		avgInquire = avgInquire/length;
		//alert(avgIdeate);
	
	ideate = selectedStudents[selectedVideoID][userID]["ideate"];
	justify = selectedStudents[selectedVideoID][userID]["justify"];
	agree = selectedStudents[selectedVideoID][userID]["agree"];
	disagree = selectedStudents[selectedVideoID][userID]["disagree"];
	inquire = selectedStudents[selectedVideoID][userID]["inquire"];

	Highcharts.chart('TS_chart', {
	    chart: {
	        polar: true,
	        type: 'line',
	    },
			exporting: {
					enabled: false
				},
				credits: {
					enabled: false
				},
	    title: {
					text: 'MY DISCUSSION FRAMES USAGE',
					style: {fontWeight: 'bold', "fontSize": "12px" }
					//x: -80
				},
				subtitle: {
					text: 'How often did I apply the different discussion frames...?',
					style: {fontWeight: 'bold', "fontSize": "10"}
				},
	    pane: {
			center: ['50%', '50%'],
	        size: 270
	    },
	    xAxis: {
	        categories: ['Ideate', 'Justify', 'Agree', 'Disagree',
	                'Inquire'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
	        margin: 1
	    },
	    series: [{
	        name: 'Me',
			color: '#7d3ae8',
	        data: [ideate, justify, agree, disagree, inquire],
	        pointPlacement: 'on',
			visible:  role==1 ? true : false
	    },{
	        name: 'Class',
			color: '#e8993a',
	        data: [avgIdeate, avgJustify, avgAgree, avgDisagree, avgInquire],
	        pointPlacement: 'on',
	        visible:  role==1 ? false : true
	    },{
	        name: 'Top Peer',
			color: '#3a89e8',
	        data: [topIdeate, topJustify, topAgree, topDisagree, topInquire],
	        pointPlacement: 'on',
	        visible:  role==1 ? false : true
	    }]
	});
}

function drawCLChart1()
{
		message = selectedStudents[selectedVideoID][userID]["message"];
		purpose = selectedStudents[selectedVideoID][userID]["purpose"];
		audience = selectedStudents[selectedVideoID][userID]["audience"];
		viewpoint = selectedStudents[selectedVideoID][userID]["viewpoint"];
		assumption = selectedStudents[selectedVideoID][userID]["assumption"];
		inference = selectedStudents[selectedVideoID][userID]["inference"];
		impact =selectedStudents[selectedVideoID][userID]["impact"];
		evidence =selectedStudents[selectedVideoID][userID]["evidence"];
	
	//console.log(selectedStudents[selectedVideoID][userID]["message"]);
	Highcharts.chart('CL_chart1', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
			exporting: {
					enabled: false
				},
				credits: {
					enabled: false
				},
	    title: {
					text: 'My <strong>CRITICAL LENS</strong> Usage Data'//,
					//style: { "font-weight": "bold" }
					//x: -80
				},
				subtitle: {
					text: 'How often did I apply the different critical lenses...?',
					style: {fontWeight: 'bold'}
				},
	    pane: {
	        size: '90%'
	    },
	    xAxis: {
	        categories: ['Message', 'Purpose', 'Audience', 'Point of View',
	                'Assumption', 'Inference', 'Impact', 'Evidence'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
					gridLineInterpolation: 'polygon',
					lineWidth: 0,
					allowDecimals: false,
					min: 0,
					tickInterval: 1
				},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
	    },
	    series: [{
	        name: 'Me',
	        data: [message, purpose, audience, viewpoint, assumption, inference, impact, evidence],
	        pointPlacement: 'on'
	    }]
	});
}

function drawTSChart1()
{
	ideate = selectedStudents[selectedVideoID][userID]["ideate"];
	justify = selectedStudents[selectedVideoID][userID]["justify"];
	agree = selectedStudents[selectedVideoID][userID]["agree"];
	disagree = selectedStudents[selectedVideoID][userID]["disagree"];
	inquire = selectedStudents[selectedVideoID][userID]["inquire"];
	Highcharts.chart('TS_chart1', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'My <strong>DISCUSSION FRAMES</strong> Usage'//,
			//style: { "font-weight": "bold" }
			//x: -80
		},
		subtitle: {
			text: 'How often did I apply the different discussion frames...?',
			style: {fontWeight: 'bold'}
		},
	    pane: {
	        size: '90%'
	    },
	    xAxis: {
	        categories: ['Ideate', 'Justify', 'Agree', 'Disagree',
	                'Inquire'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
	    },
	    series: [{
	        name: 'Me',
	        data: [ideate, justify, agree, disagree, inquire],
	        pointPlacement: 'on'
	    }]
	});
}
function drawAttitudeChart()
{

	
	if (!cal){
	SelfEfficacy = 0;
	TaskValues = 0;
	Behave_Cog_EmoEngagement = 0;
	AcadSelfConcept = 0;
		
	}else{
	SelfEfficacy = parseFloat(cal["SelfEfficacy"]);
	TaskValues = parseFloat(cal["TaskValues"]);
	Behave_Cog_EmoEngagement = parseFloat(cal["Behave_Cog_EmoEngagement"]);;
	AcadSelfConcept = parseFloat(cal["AcadSelfConcept"]);
	}
	
	if (!postcal){
	PostSelfEfficacy = 0;
	PostTaskValues = 0;
	PostBehave_Cog_EmoEngagement = 0;
	PostAcadSelfConcept = 0;
		
	}else{
	PostSelfEfficacy = parseFloat(postcal["SelfEfficacy"]);
	PostTaskValues = parseFloat(postcal["TaskValues"]);
	PostBehave_Cog_EmoEngagement = parseFloat(postcal["Behave_Cog_EmoEngagement"]);
	PostAcadSelfConcept = parseFloat(postcal["AcadSelfConcept"]);
	}
	

	
	Highcharts.chart('attitudes_chart', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'MY LEARNING ATTITUDES',
			style: {fontWeight: 'bold', "fontSize": "12px" }
			//x: -80
		},
		/*subtitle: {
			text: 'How much I appreciate SS\'s importance and usefulness...<br>How confident am I in doing well... <br>How attentive and interested am I...<br>How much of a critical reader am I...<br>How satisfied am I with myself in EL...',
			style: {fontWeight: 'bold', align: 'right'}
		},*/
	    pane: {
	        center: ['50%', '50%'],
	        size: 150
	    },
	    xAxis: {
	        categories: ['Confidence', 'Appreciation for Geography', 'Self-Belief', 'Attention & Interest'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			max: 7,
			startOnTick: true,
			endOnTick: true,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
			y: 23
	    },
	    series: [{
	        name: 'Me (Before)',
			color: '#b36aff',
	        data: [SelfEfficacy, TaskValues, AcadSelfConcept, Behave_Cog_EmoEngagement],
	        pointPlacement: 'on',
			visible:  role==1? true : false,
			showInLegend: role==1 ? true : false
	    },{
	        name: 'Me (Now)',
			color: '#7d3ae8',
	        data: [PostSelfEfficacy, PostTaskValues, PostAcadSelfConcept, PostBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
			visible:  role==1 ? true : false,
			showInLegend: role==1 ? true : false
	    },{
	        name: 'Class (Before)',
			color: '#ffca69',
	         data: [AvgSelfEfficacy, AvgTaskValues, AvgAcadSelfConcept, AvgBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible: role==1 ? false : true,
			showInLegend: role==1 ? false : true
	    },{
	        name: 'Class (Now)',
			color: '#e8993a',
	         data: [PostAvgSelfEfficacy, PostAvgTaskValues, PostAvgAcadSelfConcept, PostAvgBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible: role==1 ? false : true,
			showInLegend: role==1 ? false : true
	    },{
	        name: 'Top Peer (Before)',
			color: '#79b8ff',
	         data: [TopSelfEfficacy, TopTaskValues, TopAcadSelfConcept, TopBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible:role==1 ? false : false,
			showInLegend: true
	    },{
	        name: 'Top Peer (Now)',
			color: '#4188cc',
	         data: [PostTopSelfEfficacy, PostTopTaskValues, PostTopAcadSelfConcept, PostTopBehave_Cog_EmoEngagement],
	        pointPlacement: 'on',
	        visible: role==1 ? false : false,
			showInLegend: true
	    }]
	});
}

function drawFeelChart()
{
	var Closeness = parseInt(cal["Closeness"]);
	var Trust = parseInt(cal["Trust"]);
	var Autonomy_CompetenceSupport = parseInt(cal["Autonomy_CompetenceSupport"]);
	Highcharts.chart('feel_chart', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'HOW DO I FEEL ABOUT MY SS TEACHER?',
			style: {fontWeight: 'bold', "fontSize": "12px" }
			//x: -80
		},
		/*subtitle: {
			text: 'How often did I apply the different thinking skills...?',
			style: {fontWeight: 'bold'}
		},*/
	    pane: {
	        center: ['50%', '50%'],
	        size: 150
	    },
	    xAxis: {
	        categories: ['Closeness', 'Belief in me', 'Supportive'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
			y: 25
	    },
	    series: [{
	        name: 'Me',
	        data: [Closeness, Trust, Autonomy_CompetenceSupport],
	        pointPlacement: 'on'
	    }/*,{
	        name: 'Current',
	         data: [4.4, 5.2, 3.3],
	        pointPlacement: 'on'
	    }*/]
	});
}

function drawDesiresChart()
{
	
	if(!cal)
	{
		PerformanceGoals =0;
		LearningGoals = 0;
		SocialGoal = 0;
	}
	else{
	PerformanceGoals = parseFloat(cal["PerformanceGoals"]);
	LearningGoals = parseFloat(cal["LearningGoals"]);
	SocialGoal = parseFloat(cal["SocialGoal"]);
	}
	if(!postcal)
	{
		PostPerformanceGoals =0;
		PostLearningGoals = 0;
		PostSocialGoal = 0;
	}
	else{
	PostPerformanceGoals = parseFloat(postcal["PerformanceGoals"]);
	PostLearningGoals = parseFloat(postcal["LearningGoals"]);
	PostSocialGoal = parseFloat(postcal["SocialGoal"]);
	}
	Highcharts.chart('feel_chart', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'MY LEARNING MOTIVATIONS',
			style: {fontWeight: 'bold', "fontSize": "12px" }
			//x: -80
		},
		/*subtitle: {
			text: 'How often did I apply the different thinking skills...?',
			style: {fontWeight: 'bold'}
		},*/
	    pane: {
	        center: ['50%', '65%'],
	        size: 150
	    },
	    xAxis: {
	        categories: ['Prove Competence To Others<br>(Looking "Smart")', 'Master Skills & Knowledge<br>(Becoming "Expert")', 'Be Helpful & Responsible<br>(Being "Good")'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: true,
			min: 0,
			max: 7,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
			y: 5,
			margin: 5
	    },
	    series: [{
	        name: 'Me (Before)',
			color: '#b36aff',
	        data: [PerformanceGoals, LearningGoals, SocialGoal],
	        pointPlacement: 'on',
			visible:  role==1 ? true : false,
			showInLegend: role==1 ? true : false
	    },{
	        name: 'Me (Now)',
			color: '#7d3ae8',
	        data: [PostPerformanceGoals, PostLearningGoals, PostSocialGoal],
	        pointPlacement: 'on',
			visible:  role==1 ? true : false,
			showInLegend: role==1 ? true : false
	    },{
	        name: 'Class (Before)',
			color: '#ffca69',
	         data: [AvgPerformanceGoals, AvgLearningGoals, AvgSocialGoal],
	        pointPlacement: 'on',
	        visible: role==1 ? false : true,
			showInLegend: role==1 ? false : true
	    },{
	        name: 'Class (Now)',
			color: '#e8993a',
	         data: [PostAvgPerformanceGoals, PostAvgLearningGoals, PostAvgSocialGoal],
	        pointPlacement: 'on',
	        visible: role==1 ? false : true,
			showInLegend: role==1 ? false : true
	    },{
	        name: 'Top Peer (Before)',
			color: '#79b8ff',
	         data: [TopPerformanceGoals, TopLearningGoals, TopSocialGoal],
	        pointPlacement: 'on',
	        visible:role==1 ? false : false,
			showInLegend: true
	    },{
	        name: 'Top Peer (Now)',
			color: '#4188cc',
	         data: [PostTopPerformanceGoals, PostTopLearningGoals, PostTopSocialGoal],
	        pointPlacement: 'on',
	        visible: role==1 ? false : false,
			showInLegend: true
	    }]
	});
}

function draw21ccChart()
{
	if(!cal){
		Creativity = 0;
		Curiosity = 0;
		CriticalThinking = 0;
		Collaboration = 0;
		SelfDirectedLearning =0;
	}else{
	Creativity = parseFloat(cal["Creativity"]);
	Curiosity = parseFloat(cal["Curiosity"]);
	CriticalThinking = parseFloat(cal["CriticalThinking"]);
	Collaboration = parseFloat(cal["Collaboration"]);
	SelfDirectedLearning =parseFloat(cal["SelfDirectedLearning"]);
	}
	
	if(!postcal){
		PostCreativity = 0;
		PostCuriosity = 0;
		PostCriticalThinking = 0;
		PostCollaboration = 0;
		PostSelfDirectedLearning =0;
	}else{
	PostCreativity = parseFloat(postcal["Creativity"]);
	PostCuriosity = parseFloat(postcal["Curiosity"]);
	PostCriticalThinking = parseFloat(postcal["CriticalThinking"]);
	PostCollaboration = parseFloat(postcal["Collaboration"]);
	PostSelfDirectedLearning =parseFloat(postcal["SelfDirectedLearning"]);
	}

	
	Highcharts.chart('21cc_chart', {
	    chart: {
	        polar: true,
	        type: 'line'
	    },
		exporting: {
			enabled: false
		},
		credits: {
			enabled: false
		},
	    title: {
			text: 'MY 21ST CENTURY SKILLS PROFILE',
			style: {fontWeight: 'bold', "fontSize": "12px" }
			//x: -80
		},
		/*subtitle: {
			text: 'How often did I apply the different critical lenses...?',
			style: {fontWeight: 'bold'}
		},*/
	    pane: {
	        center: ['50%', '50%'],
	        size: 150
	    },
	    xAxis: {
	        categories: ['Creativity', 'Curiosity', 'Critical Thinking', 'Collaboration',
	                'Self Directed Learner'],
	        tickmarkPlacement: 'on',
	        lineWidth: 0
	    },
	    yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			allowDecimals: false,
			min: 0,
			tickInterval: 1
		},
	    tooltip: {
	        shared: true,
	        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
	    },
	    legend: {
	        align: 'center',
			y: 20
	    },
	   series: [{
	        name: 'Me (Before)',
			color: '#b36aff',
	        data: [Creativity, Curiosity, CriticalThinking, Collaboration, SelfDirectedLearning],
	        pointPlacement: 'on',
			visible:  role==1 ? true : false,
			showInLegend: role==1 ? true : false
	    },{
	        name: 'Me (Now)',
			color: '#7d3ae8',
	        data: [PostCreativity, PostCuriosity, PostCriticalThinking, PostCollaboration, PostSelfDirectedLearning],
	        pointPlacement: 'on',
			visible:  role==1 ? true : false,
			showInLegend: role==1 ? true : false
	    },{
	        name: 'Class (Before)',
			color: '#ffca69',
	         data: [AvgCreativity, AvgCuriosity, AvgCriticalThinking, AvgCollaboration, AvgSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible: role==1 ? false : true,
			showInLegend: role==1 ? false : true
	    },{
	        name: 'Class (Now)',
			color: '#e8993a',
	         data: [PostAvgCreativity, PostAvgCuriosity, PostAvgCriticalThinking, PostAvgCollaboration, PostAvgSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible: role==1 ? false : true,
			showInLegend: role==1 ? false : true
	    },{
	        name: 'Top Peer (Before)',
			color: '#79b8ff',
	         data: [TopCreativity, TopCuriosity, TopCriticalThinking, TopCollaboration, TopSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible:role==1 ? false : false,
			showInLegend: true
	    },{
	        name: 'Top Peer (Now)',
			color: '#4188cc',
	         data: [PostTopCreativity, PostTopCuriosity, PostTopCriticalThinking, PostTopCollaboration, PostTopSelfDirectedLearning],
	        pointPlacement: 'on',
	        visible: role==1 ? false : false,
			showInLegend: true
	    }]
	});
}

var hidden_me_before = false;
var hidden_class_before = true;
var hidden_topPeer_before = true;
var hidden_me_now = false;
var hidden_class_now = true;
var hidden_topPeer_now = true;

function drawMindsetChart()
{
	

	/*var learning_goals_val = parseFloat(cal["PerformanceGoals"]);
	var surface_goals_val = parseFloat(cal["LearningGoals"]);
	var social_goals_val = parseFloat(cal["DeepLearning"]);*/
	;
	if(!cal){
		DeepLearning = 0;
		SurfaceLearning = 0;

	}else{
	DeepLearning = parseFloat(cal["DeepLearning"]);
	SurfaceLearning = parseFloat(cal["SurfaceLearning"])
	}
	if(!postcal){
		PostDeepLearning = 0;
		PostSurfaceLearning = 0;

	}else{
	PostDeepLearning = parseFloat(postcal["DeepLearning"]);
	PostSurfaceLearning = parseFloat(postcal["SurfaceLearning"]);
	}
	/*var learning_goals_pct = (learning_goals_val/7 * 100).toFixed(0);
	var surface_goals_pct = (surface_goals_val/7 * 100).toFixed(0);
	var social_goals_pct = (social_goals_val/7 * 100).toFixed(0);
	var deep_learning_pct = (deep_learning_val/7 * 100).toFixed(0);
	var surface_learning_pct = (surface_learning_val/7 * 100).toFixed(0);*/
	$('#surfacedeeptable').remove();
	$('#learningStrategiesLegend').remove();
	
	

	$("<table id='surfacedeeptable' class='slider-chart'></table>").appendTo("#learning_mindset_container");
	$('#surfacedeeptable').append("<tr><th></th><th style='text-align:left;font-size:11px;padding-right:20px;line-height:14px;'>Focus on surface<br>understanding<br>and scoring in exams</th><th style='text-align:right;font-size:11px;line-height:14px;'>Focus on deep<br>understanding<br>and learning for life</th></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><th></th><th style='font-size:10px;text-align:left'>(Less Healthy)</th><th style='font-size:10px;text-align:right'>(More Healthy)</th></tr>");
	if (role ==1){
		$('#surfacedeeptable tr:last').after("<tr id='tr_me_before' class='two-column-comp-me-before'><td style='font-size:11px;font-weight:bold'>Me (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(SurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(DeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_me_now' class='two-column-comp-me-now'><td style='font-size:11px;font-weight:bold'>Me (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_before' class='two-column-comp-top-before'><td style='font-size:11px;font-weight:bold'>Top Peer (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(TopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(TopDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_now' class='two-column-comp-top-now'><td style='font-size:11px;font-weight:bold'>Top Peer (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopDeepLearning,2)+"</td></tr>");	
	}else{
	$('#surfacedeeptable tr:last').after("<tr id='tr_class_before' class='two-column-comp-class-before'><td style='font-size:11px;font-weight:bold'>Class (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(AvgSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(AvgDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_class_now' class='two-column-comp-class-now'><td style='font-size:11px;font-weight:bold'>Class (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostAvgSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostAvgDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_before' class='two-column-comp-top-before'><td style='font-size:11px;font-weight:bold'>Top Peer (Before)</td><td style='font-size:12px;font-weight:bold'>"+round(TopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(TopDeepLearning,2)+"</td></tr>");
	$('#surfacedeeptable tr:last').after("<tr><td></td></tr>");
	$('#surfacedeeptable tr:last').after("<tr id='tr_topPeer_now' class='two-column-comp-top-now'><td style='font-size:11px;font-weight:bold'>Top Peer (Now)</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopSurfaceLearning,2)+"</td><td style='font-size:12px;font-weight:bold'>"+round(PostTopDeepLearning,2)+"</td></tr>");	
	}
	
	twoColComp.init();

	if (role ==1){
		
		$('<div id="learningStrategiesLegend"><div id="square_me_before" class="square"></div><label id="label_me_before">Me (Before)</label><div id="square_me_now" class="square"></div><label id="label_me_now">Me (Now)</label><div id="square_topPeer_before" class="square"></div><label id="label_topPeer_before">Top Peer (Before)</label><div id="square_topPeer_now" class="square"></div><label id="label_topPeer_now">Top Peer (Now)</label></div>').appendTo("#learning_mindset_container");
	}else{
		$('<div id="learningStrategiesLegend"><div id="square_class_before" class="square"></div><label id="label_class_before">Class (Before)</label><div id="square_class_now" class="square"></div><label id="label_class_now">Class (Now)</label><div id="square_topPeer_before" class="square"></div><label id="label_topPeer_before">Top Peer (Before)</label><div id="square_topPeer_now" class="square"></div><label id="label_topPeer_now">Top Peer (Now)</label></div>').appendTo("#learning_mindset_container");
		
	}
	if (role==1)
	{
		
		/*$('#label_me').css("color", "lightgrey");
		$('#tr_me').css("opacity", 0);	
		$('#label_class').css("color", "black");
		$('#tr_class').css("opacity", 1);
		$('#label_topPeer').css("color", "black");
		$('#tr_topPeer').css("opacity", 1);
		hidden_me = true;
		hidden_class = false;
		hidden_topPeer = false;*/

		$('#label_me_before').css("color", "black");
		$('#tr_me_before').css("opacity", 1);
		$('#label_me_now').css("color", "black");
		$('#tr_me_now').css("opacity", 1);		
		$('#label_topPeer_before').css("color", "lightgrey");
		$('#tr_topPeer_before').css("opacity", 0);
		$('#label_topPeer_now').css("color", "lightgrey");
		$('#tr_topPeer_now').css("opacity", 0);	
		hidden_me_before = false;
		hidden_me_now = false;
		hidden_topPeer_before = true;
		hidden_topPeer_now = true;
	}else{
		$('#label_topPeer_before').css("color", "lightgrey");
		$('#tr_topPeer_before').css("opacity", 0);
		$('#label_topPeer_now').css("color", "lightgrey");
		$('#tr_topPeer_now').css("opacity", 0);	
		$('#label_class_before').css("color", "black");
		$('#tr_class_before').css("opacity", 1);
		$('#label_class_now').css("color", "black");
		$('#tr_class_now').css("opacity", 1);
		hidden_topPeer_before = true;
		hidden_topPeer_now = true;
		hidden_class_before = false;
		hidden_class_now = false;
		
	}

	$('#label_me_before').click(function(){
		if(hidden_me_now)
		{
			$('#label_me_before').css("color", "black");
			$('#tr_me_before').css("opacity", 1);
			hidden_me_before = false;
		}
		else
		{
			$('#label_me_before').css("color", "lightgrey");
			$('#tr_me_before').css("opacity", 0);	
			hidden_me_before = true;
		}
	});
	$('#label_me_now').click(function(){
		if(hidden_me_now)
		{
			$('#label_me_now').css("color", "black");
			$('#tr_me_now').css("opacity", 1);
			hidden_me_now = false;
		}
		else
		{
			$('#label_me_now').css("color", "lightgrey");
			$('#tr_me_now').css("opacity", 0);	
			hidden_me_now = true;
		}
	});
		

	$('#label_class_before').click(function(){
		if(hidden_class_before)
		{
			$('#label_class_before').css("color", "black");
			$('#tr_class_before').css("opacity", 1);
			hidden_class_before = false;
		}
		else
		{
			$('#label_class_before').css("color", "lightgrey");
			$('#tr_class_before').css("opacity", 0);	
			hidden_class_before = true;
		}
	});
	$('#label_class_now').click(function(){
		if(hidden_class_now)
		{
			$('#label_class_now').css("color", "black");
			$('#tr_class_now').css("opacity", 1);
			hidden_class_now = false;
		}
		else
		{
			$('#label_class_now').css("color", "lightgrey");
			$('#tr_class_now').css("opacity", 0);	
			hidden_class_now = true;
		}
	});

	$('#label_topPeer_before').click(function(){
		if(hidden_topPeer_before)
		{
			$('#label_topPeer_before').css("color", "black");
			$('#tr_topPeer_before').css("opacity", 1);
			hidden_topPeer_before = false;
		}
		else
		{
			$('#label_topPeer_before').css("color", "lightgrey");
			$('#tr_topPeer_before').css("opacity", 0);	
			hidden_topPeer_before = true;
		}
	});
	$('#label_topPeer_now').click(function(){
		if(hidden_topPeer_now)
		{
			$('#label_topPeer_now').css("color", "black");
			$('#tr_topPeer_now').css("opacity", 1);
			hidden_topPeer_now = false;
		}
		else
		{
			$('#label_topPeer_now').css("color", "lightgrey");
			$('#tr_topPeer_now').css("opacity", 0);	
			hidden_topPeer_now = true;
		}
	});

	/*var color_to_use = "red";
	//background-image: linear-gradient(to bottom,#337ab7 0,#286090 100%);
	$("#learning_goals_bar").css("width", learning_goals_pct+"%");
	if(learning_goals_val < 4)
		color_to_use = "red";
	else if(learning_goals_val < 6)
		color_to_use = "lightgreen";
	else
		color_to_use = "darkgreen";
	$("#learning_goals_bar").css("background-image", "linear-gradient(to bottom,"+color_to_use+" 0,"+color_to_use+" 100%)");
	$("#learning_goals_bar > .progress-value").html(learning_goals_pct+"%");
	$("#surface_goals_bar").css("width", surface_goals_pct+"%");
	if(surface_goals_val < 4)
		color_to_use = "darkgreen";
	else if(surface_goals_val < 6)
		color_to_use = "lightgreen";
	else
		color_to_use = "red";
	$("#surface_goals_bar").css("background-image", "linear-gradient(to bottom,"+color_to_use+" 0,"+color_to_use+" 100%)");
	$("#surface_goals_bar > .progress-value").html(surface_goals_pct+"%");
	
	$("#social_goals_bar").css("width", social_goals_pct+"%");
	if(social_goals_val < 4)
		color_to_use = "red";
	else if(social_goals_val < 6)
		color_to_use = "lightgreen";
	else
		color_to_use = "darkgreen";
	$("#social_goals_bar").css("background-image", "linear-gradient(to bottom,"+color_to_use+" 0,"+color_to_use+" 100%)");
	$("#social_goals_bar > .progress-value").html(social_goals_pct+"%");
	$("#deep_learning_bar").css("width", deep_learning_pct+"%");
	if(deep_learning_val < 4)
		color_to_use = "red";
	else if(deep_learning_val < 6)
		color_to_use = "lightgreen";
	else
		color_to_use = "darkgreen";
	$("#deep_learning_bar").css("background-image", "linear-gradient(to bottom,"+color_to_use+" 0,"+color_to_use+" 100%)");
	$("#deep_learning_bar > .progress-value").html(deep_learning_pct+"%");
	$("#surface_learning_bar").css("width", surface_learning_pct+"%");
	if(surface_learning_val < 4)
		color_to_use = "darkgreen";
	else if(surface_learning_val < 6)
		color_to_use = "lightgreen";
	else
		color_to_use = "red";
	$("#surface_learning_bar").css("background-image", "linear-gradient(to bottom,"+color_to_use+" 0,"+color_to_use+" 100%)");
	$("#surface_learning_bar > .progress-value").html(surface_learning_pct+"%");*/
}
function drawAnnotationChart()
{
	
	var submission = selectedStudents[selectedVideoID][userID]["submissions"];
	var replied = 0;
		$.each(submission, function(x, y) {
				 if (submission[x]["video_id"] == selectedVideo["id"]){
					 if (submission[x]["qnsNo"]>0 && submission[x]["qnsNo"]<=selectedVideo["qnsNo"]){
						 replied +=1;
					 }
				 }
			 
			});
	var notReplied = selectedVideo["qnsNo"] - replied;
	if (notReplied < 0){
		notReplied = 0;
	}
		
	var data = google.visualization.arrayToDataTable([
		['Qns Replied', 'No. of qns replied'],
          ['Annotated', replied],
          ['Not Annotated', notReplied]
        ]);
        var options = {
         width:'100%',
		height:'100%',
		pieHole: 0.4,
		pieSliceTextStyle: {
			color: '#FFFFFF',
			fontSize: 10,
			bold: true
		},
		 legend: {
			position: 'bottom', 
			cursor: 'pointer', 
			textStyle: {fontSize: 11}}, 
		animation: {
			startup: true,
			duration: 1000,
			easing: 'out',
		},
		colors: ['#00ACEC','#FF1655'], 
		'chartArea': {'left':'0', 'top': '5', 'width': '100%', 'height': '80%'},
		pieStartAngle: 180,
        };
        var chart = new google.visualization.PieChart(document.getElementById('annotations_chart'));
        chart.draw(data, options);
}
function drawAnnotationQnsChart()
{
	//console.log("completedQnsTracker length: " + completedQnsTracker.length);
	
	var dataArray = [];
	
	dataArray.push(['Question', 'annotations', { role: 'annotation' }, 'Never annotations', { role: 'annotation' } ]);
	
	$.each(completedQnsTracker, function(i, l){
		var qNum = i+1;
		var done_this_qn = completedQnsTracker[i];
		var done_this_qn_percentage = round(done_this_qn/total_students_count*100, 1);
		var num_not_done_this_qn = total_students_count - done_this_qn;
		var num_not_done_this_qn_percentage = round(num_not_done_this_qn/total_students_count*100, 1);
		
		var tempArray = ['Q'+qNum, done_this_qn, done_this_qn_percentage+'%', num_not_done_this_qn, num_not_done_this_qn_percentage+'%'];
		dataArray.push(tempArray);
	});
	
	/* var data = google.visualization.arrayToDataTable([
		['Question', 'annotations', { role: 'annotation' }, 'Never annotations', { role: 'annotation' } ],
		['Q1', 10, '', 24, '']
	  ]); */
	
	// Create the data table.			
	var data = google.visualization.arrayToDataTable(dataArray);
	var options = {
			   colors: ['#00ACEC','#FF1655'],//colors:['#32d3e4','#e44332'],//colors:['#109618','#3366CC'],
			   legend: {
					position: 'none', 
					cursor: 'pointer', 
					textStyle: {fontSize: 11}
				},
				animation:{
					startup: true,
					duration: 1000,
					easing: 'out',
				},
				isStacked: 'percent',
				'chartArea': {'width': '80%', 'height': '80%'},
				hAxis: {
					gridlines: {
						count: 0,
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent'
				},
				vAxis: {
					gridlines: {
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent' 
				},
				annotations: {
				  textStyle: {
					  //fontName: 'Times-Roman',
					  color: '#FFFFFF',
					  fontSize: 12,
					  bold: true,
					  auraColor: 'none' ,
					  /*italic: true,
					  color: '#871b47',     // The color of the text.
					  auraColor: '#d799ae', // The color of the text outline.
					  opacity: 0.8          // The transparency of the text. */
					}
				}
			  };
		
	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.BarChart(document.getElementById('annotationQns_chart'));
	chart.draw(data, options);
	
	google.visualization.events.addListener(chart, 'onmouseover', function(){
		$('#annotationQns_chart').css('cursor','pointer');
	});
	
	google.visualization.events.addListener(chart, 'onmouseout', function(){
		$('#annotationQns_chart').css('cursor','default');
	});
	
	function selectHandler() {
		var selectedItem = chart.getSelection()[0];
		if (selectedItem) {
			var question = data.getValue(selectedItem.row, 0);
			question = question.substring(1);
			question = parseInt(question);
			
			//var value = data.getValue(selectedItem.row, selectedItem.column);
			var studentListHTML = "<div class='container-fluid'><div class='row'>";
			if(selectedItem.column === 1 || selectedItem.column === 2)//Selected annotations value
			{
				//alert('selected annotations people for '+question);
				$("#modal-title").html("Students who answered Annotation Question "+question);
				$.each(selectedStudents, function(i, l){
													
					if(selectedStudents[i]["completedAnnotations"][question-1])
					{
						//studentListHTML += '<div class="col-sm-4 col-md-2 student-label positive">'+selectedStudents[i]["name"]+'</div>';
						studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
					}
				});
			}
		  
			else if(selectedItem.column === 3 || selectedItem.column === 4)//Selected never annotations value
			{
				//alert('selected never annotations people for '+question);
				$("#modal-title").html("Students who have not answered Annotation Question "+question);
				$.each(selectedStudents, function(i, l){
													
					if(!selectedStudents[i]["completedAnnotations"][question-1])
					{
						//studentListHTML += '<div class="col-sm-4 col-md-2 student-label negative">'+selectedStudents[i]["name"]+'</div>';
						studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
					}
				});
			}
			
			studentListHTML+= '</div></div>';
		
			$("#modal-body").html(studentListHTML);
				
			$('#myModal').modal('toggle');
		}
	  }
	
	google.visualization.events.addListener(chart, 'select', selectHandler);
}
function drawAnnotationChart1(){
	
	var submission = selectedStudents[selectedVideoID][userID]["submissions"];
	var replied = 0;
	$.each(allVideos, function(i, l){
		$.each(submission, function(x, y) {
				 if (submission[x]["video_id"] == allVideos[i]["id"]){
					 if (submission[x]["qnsNo"]>0 && submission[x]["qnsNo"]<=allVideos[i]["qnsNo"]){
						 replied +=1;
					 }
				 }
			 
			});
});
	
	var notReplied = totalqnsNo - replied;
	if (notReplied < 0){
		notReplied = 0;
	}
		
	var data = google.visualization.arrayToDataTable([
		['Qns Replied', 'No. of qns replied'],
          ['Annotated', replied],
          ['Not Annotated', notReplied]
        ]);
        var options = {
         width:210,
		height:190,
		pieHole: 0.4,
		pieSliceTextStyle: {
			color: '#FFFFFF',
			fontSize: 10,
			bold: true
		},
		 legend: {
			position: 'bottom', 
			cursor: 'pointer', 
			textStyle: {fontSize: 11}}, 
		animation: {
			startup: true,
			duration: 1000,
			easing: 'out',
		},
		colors: ['#00ACEC','#FF1655'], 
		'chartArea': {'left':'40', 'top': '5', 'width': '100%', 'height': '80%'},
		pieStartAngle: 180,
        };
        var chart = new google.visualization.PieChart(document.getElementById('annotations_chart1'));
        chart.draw(data, options);
	}

function drawAnnotationQnsChart1()
{
	var submission = selectedStudents[selectedVideoID][userID]["submissions"];
	var replied = 0;
	var dataArray = [];
	var vNo	=0;
	dataArray.push(['Video', 'Annotated', { role: 'annotated' }, 'Not Annotated', { role: 'not annotated' } ]);
	$.each(allVideos, function(i, l){
		var qns = allVideos[i]["qnsNo"];
	if(qns!=0){
		vNo++;
		replied = 0;
		$.each(submission, function(x, y) {
				 if (submission[x]["video_id"] == allVideos[i]["id"]){
					 if (submission[x]["qnsNo"]>0 && submission[x]["qnsNo"]<=allVideos[i]["qnsNo"]){
						 replied +=1;
					 }
				 }
			 
			});
		var replied_percentage = round(replied/qns*100, 1);
		var notReplied = qns - replied;
		if (notReplied < 0){
		notReplied = 0;
	}
		var num_not_replied_percentage = round(replied/qns*100, 1);
		var tempArray = ['Vid'+vNo, replied, replied_percentage+'%', notReplied, num_not_replied_percentage+'%'];
		dataArray.push(tempArray);
	}
});
	
						
				var data = google.visualization.arrayToDataTable(dataArray);
				var options = {
							height:100,
						   colors: ['#00ACEC','#FF1655'],
						   legend: {
								position: 'none', 
								cursor: 'pointer', 
								textStyle: {fontSize: 11}
							},
							animation:{
								startup: true,
								duration: 1000,
								easing: 'out',
							},
							isStacked: 'percent',
							'chartArea': {'width': '80%', 'height': '80%'},
							hAxis: {
								gridlines: {
									count: 0,
									color: 'transparent'
								},
								scaleType: 'linear',
								minValue: 0,
								baselineColor: 'transparent'
							},
							vAxis: {
								gridlines: {
									color: 'transparent'
								},
								scaleType: 'linear',
								minValue: 0,
								baselineColor: 'transparent' 
							},
							annotations: {
							  textStyle: {
								  color: '#FFFFFF',
								  fontSize: 11,
								  bold: true,
								  auraColor: 'none' ,
								}
							}
						  };
					
				var chart = new google.visualization.BarChart(document.getElementById('annotationQns_chart1'));
				chart.draw(data, options);
}

function drawViewVideosChart1() 
{				
	var videosStatus = selectedStudents[selectedVideoID][userID]["video_view_status"];
	var videos = 0;
	var videosWatched = 0;
	var videoCheck =0;
	var videosNotWatch = 0;

	$.each(allVideos, function(i, l){
		videos++;
		videoCheck=0;
		$.each(videosStatus, function(x, y){
			if (videoCheck !=1){
				if (videosStatus[x]["label"] == allVideos[i]["id"]){
					if (videosStatus[x]["action"] == "75%"){
						videosWatched++;
						videoCheck =1;
					}
				}
			}
		});
	});

	videosNotWatch = videos-videosWatched;
	if (videosNotWatch <0){
		videosNotWatch = 0;
	}
	
	var data = google.visualization.arrayToDataTable([
		['Video Viewed', 'No. of video viewed'],
          ['Viewed', videosWatched],
          ['Not Viewed', videosNotWatch]
        ]);
        var options = {
         width:210,
		height:190,
		pieHole: 0.4,
		pieSliceTextStyle: {
			color: '#FFFFFF',
			fontSize: 10,
			bold: true
		},
		legend: {
			position: 'bottom', 
			cursor: 'pointer', 
			textStyle: {fontSize: 11}
		}, 
		animation: {
			startup: true,
			duration: 1000,
			easing: 'out',
		},
		colors: ['#00ACEC','#FF1655'], 
		'chartArea': {'left':'40', 'top': '5', 'width': '100%', 'height': '80%'},
		pieStartAngle: 180,
        };

    var chart = new google.visualization.PieChart(document.getElementById('video_chart1'));
    chart.draw(data, options);
}

function drawEachVideosChart1() 
{				
	var videosStatus = selectedStudents[selectedVideoID][userID]["video_view_status"];
	var percentWatched;
	var videoID;
	var dataArray = [];
	var vNo	=0;
	dataArray.push(['Video', 'Videos Viewed', { role: 'video viewed' }, 'Videos Not Viewed', { role: 'video not viewed' } ]);
	$.each(allVideos, function(i, l){
		 vNo +=1;
		percentWatched=0;
		$.each(videosStatus, function(x, y) {
				 if (videosStatus[x]["label"] == allVideos[i]["id"]){
					 if (videosStatus[x]["action"]=="10%"){
						 percentWatched =10;
					 }else if (videosStatus[x]["action"]=="25%"){
						 percentWatched =25;
					 }else if (videosStatus[x]["action"]=="50%"){
						 percentWatched =50;
					 }else if (videosStatus[x]["action"]=="75%"){
						 percentWatched =75;
					 }else if (videosStatus[x]["action"]=="90%"){
						 percentWatched =90;
					 }else if (videosStatus[x]["action"]=="Watch to End"){
						 percentWatched =100;
					 }else {
						 percentWatched =0;
					 }
				 }
			 
			});
		var percentNotWatched = 100 - percentWatched;
		var tempArray = ['Vid'+vNo, percentWatched, percentWatched+'%', percentNotWatched, percentNotWatched+'%'];
		dataArray.push(tempArray);
	});
	
						
	var data = google.visualization.arrayToDataTable(dataArray);
	var options = {
				height:100,
			   colors: ['#00ACEC','#FF1655'],
			   legend: {
					position: 'none', 
					cursor: 'pointer', 
					textStyle: {fontSize: 11}
				},
				animation:{
					startup: true,
					duration: 1000,
					easing: 'out',
				},
				isStacked: 'percent',
				'chartArea': {'width': '80%', 'height': '80%'},
				hAxis: {
					gridlines: {
						count: 0,
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent'
				},
				vAxis: {
					gridlines: {
						color: 'transparent'
					},
					scaleType: 'linear',
					minValue: 0,
					baselineColor: 'transparent' 
				},
				annotations: {
				  textStyle: {
					  color: '#FFFFFF',
					  fontSize: 11,
					  bold: true,
					  auraColor: 'none' ,
					}
				}
			  };
		
	var chart = new google.visualization.BarChart(document.getElementById('eachVideo_chart1'));
	chart.draw(data, options);
}

function setupIndividualStudent(status, current_student, HTML_string)
{
	var studentListHTML = HTML_string;
	
	var has_login = '<span class="badge negative-bg">No</span>Logged In';
	var has_viewed = '<span class="badge negative-bg">No</span>Watched Full Video';
	var has_completed = '<span class="badge negative-bg">No</span>All Annotation Questions Answered';
	var has_annotations = '<span class="badge negative-bg">No</span>Made Annotation';
	var has_comments = '<span class="badge negative-bg">No</span>Posted Comment';
	var has_replies = '<span class="badge negative-bg">No</span>Posted Reply';
	
	if(current_student["login"] > 0)has_login = '<span class="badge positive-bg">Yes</span>Logged In';
	if(Object.keys(current_student["viewedVideos"]).length > 0)has_viewed = '<span class="badge positive-bg">Yes</span>Watched Full Video';
	if(current_student["completeAnnotations"] > 0)has_completed = '<span class="badge positive-bg">Yes</span>All Annotation Questions Answered';
	if(current_student["annotations"] > 0)has_annotations = '<span class="badge positive-bg">Yes</span>Made Annotation';
	if(current_student["comments"] > 0)has_comments = '<span class="badge positive-bg">Yes</span>Posted Comment';
	if(current_student["replies"] > 0)has_replies = '<span class="badge positive-bg">Yes</span>Posted Reply';
	
	studentListHTML += '<div class="col-sm-4 col-md-3 student-label '+status+' panel-group" id="accordion" role="tablist" aria-multiselectable="true">'+
											'<div class="panel-heading" role="tab" id="headingOne">'+
												'<h4 class="panel-title">'+
													'<a role="button" data-toggle="collapse" data-parent="#accordion" href=".collapseStudents" aria-expanded="false" aria-controls="collapseStudents">'+
														current_student["name"]+
													'</a>'+
												'</h4>'+
											'</div>'+
											'<div class="panel-collapse collapseStudents collapse" role="tabpanel" aria-labelledby="headingOne">'+
												'<ul class="list-group">'+
													'<li class="list-group-item">'+
														has_login+
													'</li>'+
													'<li class="list-group-item">'+
														has_viewed+
													'</li>'+
													'<li class="list-group-item">'+
														has_completed+
													'</li>'+
													'<li class="list-group-item">'+
														has_annotations+
													'</li>'+
													'<li class="list-group-item">'+
														has_comments+
													'</li>'+
													'<li class="list-group-item">'+
														has_replies+
													'</li>'+
												'</ul>'+
											'</div>'+
										'</div>';
	
	return studentListHTML;
}
function selectHandler(chart, data) {
	var selection = chart.getSelection();
	if (selection.length) {
		
		var selectedValue = data.getColumnLabel(selection[0].column);
		var studentListHTML = "<div class='container-fluid'><div class='row'>";
		
		if(selection[0].column === 2 || selection[0].column === 4)//In the case where the user selects the annotation labels (Percentage labels)
		{
			selectedValue = data.getColumnLabel(selection[0].column-1);
		}
		
		//console.log("selectedValue: "+ selectedValue);
		//console.log("selection[0].column: "+selection[0].column);
		
		$.each(selectedStudents, function(i, l){
				
			if(selectedValue == "Logged In")
			{
				$("#modal-title").html("Students who have logged in");
				
				if(selectedStudents[i]["login"] === 1)
				{
					studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
				}
			}
			else if(selectedValue == "Never Logged In")
			{
				$("#modal-title").html("Students who have never logged in");
				if(selectedStudents[i]["login"] === 0)
				{
					studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
				}
			}
			
			else if(selectedValue == "Watched Video")
			{
				$("#modal-title").html("Students who finished watching video");
			
				if(selectedStudents[i]["viewedVideos"][selectedVideo["id"]])
				{
					studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
				}
			}
			
			else if(selectedValue == "Never Watched Video")
			{
				$("#modal-title").html("Students who never watched video");
				if(!selectedStudents[i]["viewedVideos"][selectedVideo])
				{
					studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
				}
			}
			
			else if(selectedValue == "comments")
			{
				$("#modal-title").html("Students who comments");
				if(selectedStudents[i]["comments"] === 1)
				{
					studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
				}
			}
			
			else if(selectedValue == "Never comments")
			{
				$("#modal-title").html("Students who never comments");
			
				if(selectedStudents[i]["comments"] === 0)
				{
					studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
				}
			}
			
			else if(selectedValue == "replies")
			{
				$("#modal-title").html("Students who replies to comments");
				
				if(selectedStudents[i]["replies"] === 1)
				{
					studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
				}
			}
			
			else if(selectedValue == "Never replies")
			{
				$("#modal-title").html("Students who never replies to comments");
				
				if(selectedStudents[i]["replies"] === 0)
				{
					studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
				}
			}	
		});
		
		studentListHTML+= '</div></div>';
		
		$("#modal-body").html(studentListHTML);
			
		$('#myModal').modal('toggle');
	}
}