<?php require_once(dirname(__FILE__) . "/student_dashboard_handler.php");?>

<!doctype html>

<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>My CoVAA Learning Profile</title>
		<meta name="description" content="Detailed info for students participation">
		<meta name="author" content="Simon">

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!--Load the AJAX API
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>-->
		
		<!--Load JQueryUI for draggable function-->
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="jquery.ui.touch-punch.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<!--Load Highcharts-->
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/highcharts-more.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		
	  <!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	  <![endif]-->
	  
		<style>

			/*Top menu bar css*/
			.bg-light {
				background-color: #26418F !important; /*#2962ff #3f51b5*/
			    color: white;
			    border-radius: 0px;
			}

			#nav-container {
				padding-right: 0px;
			}

			#page-title {
			    color: #83C8F6;
			    font-size: 16px !important;
			    line-height: 1.5;
			    padding-top: 5px;
			}

			.navbar-nav a {
				color: white;
			}

			.navbar-light .navbar-nav .active>.nav-link,
			.navbar-light .navbar-nav .nav-link {
			    color: white;
			}

			.nav>li>a {
				padding: 7px 7px;
			}

			.nav>li>a:hover {
			    text-decoration: underline;
			    color: white;
			    background-color: transparent;
			}

			#bs-example-navbar-collapse-1 > ul > li > a {
				font-size: 14px;
			   /* font-weight: 400;
			    line-height: 1.5;*/
			}

			.navbar-nav > .active {
			    background-color: grey;
			}

			.navbar, .well {
				/*margin-top: -4px;
				margin-bottom: 0px;*/
				/*padding: .5rem 1rem;*/
				height: 35px;
				font-size: 14px;
				min-height: 20px;
			    padding: 0px 10px;
			    border: 0px;
			}

			.navbar-brand img {
				margin: -12px 0 0 -14px;
				width: 80px;
			}

			.navbar-right {
			    /*margin-top: 4px;*/
			}
			/*End of Top menu bar css*/

			.nav-pills>li>a {
   				border-radius: 0px;
   			}

			#sidebar>li {
				/*border-top: 1px solid grey;
			    border-left: 1px solid grey;
			    border-right: 1px solid grey;*/
			    background-color: lightgrey;
			}

			#sidebar>li>a:hover {
				text-decoration: underline;
			    color: black;
			    background-color: #337ab7;
			}

			#container {
				margin: 0px;
				padding: 10px;
				background-color: #F4F5F9;
			}
			body {

				font-family: 'Roboto', sans-serif;
				font-size: 12px;
				background-color: #F4F5F9;
			}
			#page_title {
				margin: 1px auto 7px auto;
			}
			hr {
				margin: 1px;
			}
			/* #summary_div, #selection_div, #filter_div {
				padding: 15px;
			} */
			#subjectLabel, #classLabel, #classSelect, #videoLabel, #videoSelect {
				/*display: none;*/
				margin-left: 5px;
			}
			.grid {
			}
			.grid_tile {
				height: 500px;
				/* width: 250px; */
				/*max-width: 450px;*/
				background-color: #FFFFFF;
				margin: 10px 0 15px 0;
				color: #2c292d;
				font-size: 2vw;
				/*text-align: center; Please leave this commented off. Affects the SNA plotting*/
				border-radius: 4px;
				border: solid 1px #676A7C;
				background-image: -moz-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
				background-image: -webkit-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
			}
			.grid_tile_bar {
				height: 320px;
			}
			.grid_tile_bar1 {
				/*height: 500px;*/
			}
			.grid_tile_annotations {
				height: 600px;
			}
			.grid_tile_sna {
				width: 470px;
				height: 500px;
			}
			#sna_container{
				width: 100%;
				height: 86%;
				/*border: 1px solid black;
				border-radius: 0.5px;*/
			}
			#btn_sna_generate, #sna_info {
				margin-left: 10px;
				margin-top: 10px;
				font-size: 12px;
				font-weight: normal;
				padding: 3px;
				border: 1px solid #666666;
				border-radius: 3px;
				background-color: #337ab7;
				color: white;
				cursor: pointer;
			}
			.reflection {
				margin-left: 10px;
				margin-top: 10px;
				font-size: 12px;
				font-weight: normal;
				padding: 3px;
				border: 1px solid #666666;
				border-radius: 3px;
				background-color: #e8993a;
				color: white;
				cursor: pointer;
			}
			#btn_sna_generate:hover, #sna_info:hover, .reflection:hover, #reflection-submit:hover {
				background-color: grey;
			}
			.grid_title {
				font-weight: bold;
				font-size: 12px;
				text-align: center;
				vertical_align: center;
				padding: 7px 5px 0px 5px;
			}
			.grid_number {
				font-size: 60px;
			}
			.grid_text {
				font-size: 12px;
				padding: 0 10px 0 10px;
			}
			.grid_chart {
				display: inline-block; /*To center align the chart*/
				height: 450px;
				width: 450px;
			}
			.grid_chart21cc {
				display: inline-block; /*To center align the chart*/
				height: 275px;
				width: 500px;
			}
			.grid_chart_o {
				display: inline-block; /*To center align the chart*/
				height: 260px;
				width: 50%;
			}
			#annotationQns_chart, annotations_chart {
				display: inline-block; /*To center align the chart*/
				height: 200px;
				width: 100%;
			}
			.grid_link {
				text-align: center; 
				padding: 0 10px 5px 10px;
			}
			/* .btn-red, .btn-red:hover, btn-red:visited, btn-red:active {
				background: #e44332 !important;
				border-color: inherit !important;
				color: #2c292d;
			}
			.btn-blue, .btn-blue:hover, btn-blue:visited, btn-blue:active {
				background: #32d3e4 !important;
				border-color: inherit !important;
				font-weight: bold;
				color: #2c292d;
			} */
			.dataContainer {
				display: flex;
				justify-content: center;
				font-family: "Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif;
				border-top: 1px solid grey;
			}
			.grid_tile_student {
				background-color: #FFFFFF;
				margin: 10px;
				padding: 10px;
				color: #676A7C;
				border-radius: 4px;
				border: solid 1px #676A7C;
				background-image: -moz-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
				background-image: -webkit-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
			}
			.grid_tile_student > button {
				float: right;
			}
			.back-to-top {
				cursor: pointer;
				position: fixed;
				bottom: 20px;
				right: 20px;
				display:none;
			}
			.modal-body {
				/*min-height: 300px;*/
			}
			.student-label {
				margin: 5px;
				padding: 5px;
				border: 3px solid;
				border-radius: 5px;
				text-align: center; 
			}
			.student-label a {
				font-weight: bold;
			}
			.positive {
				border-color: #00ACEC;
			}
			.positive-bg {
				background-color: #00ACEC;
			}
			.negative {
				border-color: #FF1655;
			}
			.negative-bg {
				background-color: #FF1655;
			}
			.list-group {
				font-weight: normal;
				text-align: left;
			}
			.col-md-12, .col-sm-12, .col-xs-12, .col-md-10, .col-sm-10, .col-xs-10{
				padding-left:0px;
			}
			#sidebar{
				padding-top:10px;
			}

			/*21CC Learning Mindset Chart styling*/
			#learning_mindset_container {
				padding: 8px 0px -1px 0px;
			}
			.progress-title{
			    font-size: 12px;
			    color: #666666;
			    margin: 0 0 10px 0;
			    text-align: left;
			}
			.progress{
			    height: 15px;
			    line-height: 8px;
			    border-radius: 20px;
			    background: #f0f0f0;
			    margin-bottom: 20px;
			    box-shadow: none;
			    overflow: visible;
			}
			.progress .progress-bar{
			    position: relative;
			    border-radius: 20px 0 0 20px;
			    animation: animate-positive 2s;
			}
			.progress .progress-value{
			    display: block;
			    font-size: 12px;
			    color: #fff;
			    position: absolute;
			    top: -1;
			    right: 8px;
			}
			@-webkit-keyframes animate-positive{
			    0% { width: 0%; }
			}
			@keyframes animate-positive{
			    0% { width: 0%; }
			}
			/*End of 21CC Learning Mindset Chart styling*/

			/*21cc Learning Strategies Styles*/
			/*.two-column-comp {
			  width: 90%;
			  background: #eee;
			  padding: 1em;
			  margin: 1em auto;
			  box-sizing: border-box;
			  border: 1px solid #ccc;
			}*/
			
			.two-column-comp {
				width: 97%;
				padding: 1em;
				box-sizing: border-box;
			}

			.two-column-comp th { 
				font-size: 12px; /*1.2em;*/
				text-align: center;
				font-weight: normal;
				/*padding-right:10px;*/
			}

			.two-column-comp td {
			  text-align:right;
			  font-weight: bold;
			  border-bottom: 1px solid #fbfbfb;
			  width: 20%;
			  padding: .35em .25em;
			  background-size: 0% 100%;
			  background-repeat: no-repeat;
			  -webkit-transition: all .75s ease-out;
			  -moz-transition: all .75s ease-out;
			  transition: all .75s ease-out;
			}

			.two-column-comp-me-before td:nth-child(2) {
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  text-align: right;
			  opacity: 0.7;
			  background-image: -webkit-linear-gradient(to left, #b36aff, #b36aff);
			  background-image: -moz-linear-gradient(to left, #b36aff, #b36aff);
			  background-image: -ms-linear-gradient(to left, #b36aff, #b36aff);
			  background-image: linear-gradient(to left, #b36aff, #b36aff);
			  background-position: right top;
			}

			.two-column-comp-me-before td:nth-child(3) {
			  text-align:left;
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  background-image: -webkit-linear-gradient(right, #b36aff, #b36aff);
			  background-image: -moz-linear-gradient(right, #b36aff, #b36aff);
			  background-image: -ms-linear-gradient(right, #b36aff, #b36aff);
			  background-image: linear-gradient(right, #b36aff, #b36aff);
			  background-position: left top;
			}
			.two-column-comp-me-now td:nth-child(2) {
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  text-align: right;
			  opacity: 0.7;
			  background-image: -webkit-linear-gradient(to left, #7d3ae8, #7d3ae8);
			  background-image: -moz-linear-gradient(to left, #7d3ae8, #7d3ae8);
			  background-image: -ms-linear-gradient(to left, #7d3ae8, #7d3ae8);
			  background-image: linear-gradient(to left, #7d3ae8, #7d3ae8);
			  background-position: right top;
			}

			.two-column-comp-me-now td:nth-child(3) {
			  text-align:left;
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  background-image: -webkit-linear-gradient(right, #7d3ae8, #7d3ae8);
			  background-image: -moz-linear-gradient(right, #7d3ae8, #7d3ae8);
			  background-image: -ms-linear-gradient(right, #7d3ae8, #7d3ae8);
			  background-image: linear-gradient(right, #7d3ae8, #7d3ae8);
			  background-position: left top;
			}

			.two-column-comp-class-before td:nth-child(2) {
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  text-align: right;
			  opacity: 0.7;
			  background-image: -webkit-linear-gradient(to left, #ffca69, #ffca69);
			  background-image: -moz-linear-gradient(to left, #ffca69, #ffca69);
			  background-image: -ms-linear-gradient(to left, #ffca69, #ffca69);
			  background-image: linear-gradient(to left, #ffca69, #ffca69);
			  background-position: right top;
			}

			.two-column-comp-class-before td:nth-child(3) {
			  text-align:left;
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  background-image: -webkit-linear-gradient(right, #ffca69, #ffca69);
			  background-image: -moz-linear-gradient(right, #ffca69, #ffca69);
			  background-image: -ms-linear-gradient(right, #ffca69, #ffca69);
			  background-image: linear-gradient(right, #ffca69, #ffca69);
			  background-position: left top;
			}
			
			.two-column-comp-class-now td:nth-child(2) {
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  text-align: right;
			  opacity: 0.7;
			  background-image: -webkit-linear-gradient(to left, #e8993a, #e8993a);
			  background-image: -moz-linear-gradient(to left, #e8993a, #e8993a);
			  background-image: -ms-linear-gradient(to left, #e8993a, #e8993a);
			  background-image: linear-gradient(to left, #e8993a, #e8993a);
			  background-position: right top;
			}

			.two-column-comp-class-now td:nth-child(3) {
			  text-align:left;
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  background-image: -webkit-linear-gradient(right, #e8993a, #e8993a);
			  background-image: -moz-linear-gradient(right, #e8993a, #e8993a);
			  background-image: -ms-linear-gradient(right, #e8993a, #e8993a);
			  background-image: linear-gradient(right, #e8993a, #e8993a);
			  background-position: left top;
			}
			
			.two-column-comp-top-before td:nth-child(2) {
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  text-align: right;
			  opacity: 0.7;
			  background-image: -webkit-linear-gradient(to left, #79b8ff, #79b8ff);
			  background-image: -moz-linear-gradient(to left, #79b8ff, #79b8ff);
			  background-image: -ms-linear-gradient(to left, #79b8ff, #79b8ff);
			  background-image: linear-gradient(to left, #79b8ff, #79b8ff);
			  background-position: right top;
			}

			.two-column-comp-top-before td:nth-child(3) {
			  text-align:left;
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  background-image: -webkit-linear-gradient(right, #79b8ff, #79b8ff);
			  background-image: -moz-linear-gradient(right, #79b8ff, #79b8ff);
			  background-image: -ms-linear-gradient(right, #79b8ff, #79b8ff);
			  background-image: linear-gradient(right, #79b8ff, #79b8ff);
			  background-position: left top;
			}
			
			.two-column-comp-top-now td:nth-child(2) {
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  text-align: right;
			  opacity: 0.7;
			  background-image: -webkit-linear-gradient(to left, #4188cc, #4188cc);
			  background-image: -moz-linear-gradient(to left, #4188cc, #4188cc);
			  background-image: -ms-linear-gradient(to left, #4188cc, #4188cc);
			  background-image: linear-gradient(to left, #4188cc, #4188cc);
			  background-position: right top;
			}

			.two-column-comp-top-now td:nth-child(3) {
			  text-align:left;
			  width: 40%;
			  color: white;
			  background-color: lightgrey;
			  /*text-shadow: 1px 2px #222;*/
			  background-image: -webkit-linear-gradient(right, #4188cc, #4188cc);
			  background-image: -moz-linear-gradient(right, #4188cc, #4188cc);
			  background-image: -ms-linear-gradient(right, #4188cc, #4188cc);
			  background-image: linear-gradient(right, #4188cc, #4188cc);
			  background-position: left top;
			}
			#learningStrategiesLegend {
				padding: 10px 0 0 0;
			}

			#label_me_now, #label_class_now, #label_topPeer_now,#label_me_before, #label_class_before, #label_topPeer_before {
				font-size: 12px;
				padding-left:7px;
				cursor: pointer;
			}

			#label_me_before:hover, #label_class_before:hover, #label_topPeer_before:hover, #label_me_now:hover, #label_class_now:hover, #label_topPeer_now:hover {
				color: black;
			}

			#label_class_before, #label_topPeer_before, #label_class_now, #label_topPeer_now {
				color: lightgrey;
			}
			#tr_class_before, #tr_topPeer_before, #tr_class_now, #tr_topPeer_now {
				opacity: 0;
			}

			.square {
				display: inline-block;
				padding:5px;
				width: 5px;
				height: 5px;
				margin-left: 10px;
			}

			#square_me_now { background-color: #7d3ae8; }
			#square_class_now { background-color: #e8993a; }
			#square_topPeer_now { background-color: #4188cc; }
			#square_me_before { background-color: #b36aff; }
			#square_class_before { background-color: #ffca69; }
			#square_topPeer_before { background-color: #79b8ff; }

			/*End of 21CC Learning Strategies Styles*/

			/*Reflections module styles*/
			#lightbox-content input[type=text], #lightbox-content select, #lightbox-content textarea {
		        width: 100%;
		        padding: 12px;
		        border: 1px solid #ccc;
		        border-radius: 4px;
		        box-sizing: border-box;
		        margin-top: 6px;
		        margin-bottom: 16px;
		        resize: none;
		    }
		 
		    #lightbox-content input[type=submit] {
		        /*margin-left: 10px;
				margin-top: 10px;*/
				font-size: 12px;
				font-weight: normal;
				padding: 3px;
				border: 1px solid #666666;
				border-radius: 3px;
				background-color: #337ab7;
				color: white;
				cursor: pointer;
		    }
		 
		    #lightbox-content input[type=submit]:hover {
		        background-color: #45a049;
		    }
		 
		    #lightbox-content {
		        
		    }
		 
		    #lightbox-bg {
		      cursor: not-allowed;
		      position: absolute;
		      top: 0%;
		      left: 0%;
		      width: 100%;
		      height: 822px;
		      min-height: 100%;
		      /*background-image:url('icons/lightbox.png');*/
		      background-repeat: repeat;
		      z-index:1;
		      display: none;
		    }
		 
		    #lightbox-popup {
		      cursor:move;
		      margin-left: calc((100% - 400px)/2);
		      margin-right: calc((100% - 400px)/2);
		      position: absolute;
		      top: 15%;
		      width: 400px;
		      height: auto;/*503px*/
		      padding: 16px 16px 0px 16px;
		      background-color: #fafafa;
		      z-index: 2;
		      display: none;
		      color: #333333;
		      border: 3px solid grey;    
		    }
		     
		    #lightbox-close {
		      position: absolute;
		      right: 5px;
			  top: 5px;
			  cursor: pointer;
			  border-radius: 16%;
			  height: 24px;
			  width: 24px;
		    }
		 
		    #reflect_title {
		    	font-size: 12px;
		    	font-weight: bold;
		      margin-top: 0px;
		      margin-bottom: 0px;
		      color: black;
		    }
		 
		    #reflect_mini_title {
		    	font-size:12px;
		      margin-top: 4px;
		      padding-bottom: 12px;
		      color: #7B7A7A;
		    }

		    #lightbox-popup label, textarea {
		    	font-size: 12px;
		    }
		    /*End of Reflections module styles*/

			#footer{
				text-align: center;
			}
			#footer > label{
				font-weight:normal;
			}
		</style>
	</head>

	<body>
		<nav class="navbar navbar-expand-sm navbar-light bg-light">
		  <div id="nav-container" class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header" style="width:390px;">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../oval-master/index.php"><img src="./icons/covva_small.png" alt="CoVAA"></a>
            	<div id="page-title">Collaborative Video Annotation & Analytics</div>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right"> 
					<li><a href="../oval-master/index.php">Watch Videos</a></li>
					<li><a class="nav-link" href="../oval-master/survey.php">Survey</a></li>
					<?php if ($isAdmin == true): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="video_management.php?cid=<?php echo "$CID"?>&gid=<?php echo "$GID"?>">Upload</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../oval-master/teacher_dashboard.php">Teacher Dashboard</a>
                        </li>
                        <!--  <a href="video_management.php" >admin</a> -->
                    <?php endif; ?>
					<li class="active"><a href="#">Learning Dashboard<span class="sr-only">(current)</span></a></li> 
					<li><a href="../oval-master/logout">Logout</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>

		<div id="container" class="container-fluid">
			<div>
				
				<label id="subjectLabel">Subject:</label><select id="subjectSelect">
					<option value="">Choose a Subject</option>
				</select>
				<label id="classLabel">Class:</label><select id="classSelect">
					<option value="">Choose a Class</option>
				</select>
				<label id="videoLabel">Video:</label>
				<select id="videoSelect"></select>
			</div>
			<div id="content" class="container-fluid ">
			    <div class="col-md-12 col-sm-12 col-xs-12">
			        <div class="row">
			            <div id="sidenav"><ul id="sidebar" class="nav nav-pills">
								<li class="active"><a data-toggle="pill" href="#data">MY COVAA LEARNING PROFILE</a></li>
								<li><a data-toggle="pill" href="#21cc">MY 21CC PROFILE</a></li>
							</ul>
			            </div>
			            <div class="dataContainer">
			                <div id="summary_div" class="container-fluid tab-content">
							<?php
								//Creation of grid of individual information
							?>
								<div id="data" class="tab-pane fade in active">
									<div >
										<div class="row">
											<div class="center-block" style="width: 470;float: left;">
												<!--<div class="grid_tile grid_tile_bar">
												<div id="vidann">
													<div class="grid_title">My Videos & Annotations</div><hr>
													<div class="grid_chart_o">
														<div id="video_chart1"></div>
														<div id="eachVideo_chart1"></div>
													</div>
													<div class="grid_chart_o" style="float: right;">
														<div id="annotations_chart1"></div>
														<div id="annotationQns_chart1"></div>
													</div>
												</div></div>-->
												<div class="grid_tile grid_tile_bar1">
													<!--<div class="grid_title">My <strong>THINKING FRAMES</strong> Usage Data</div><hr>-->
													<div class="grid_chart" id="TS_chart"></div>
													<div style="padding-top: 5px;">
														<label data-seq="discussion" data-title="Discussion Frames" class="reflection">Reflect & Set Goals</label>
													</div>
												</div>
												
												
												
											</div> 
											<div class="center-block" style="width: 470;margin-left: 490;">
												<div class="grid_tile grid_tile_sna">
													<div class="grid_title">MY COVAA DISCUSSION NETWORK</div>
													<div id="sna_container"></div>
													<div>
														<label id="sna_info" data-toggle="modal" data-target="#myModal">SNA Info</label>
														<label id="btn_sna_generate">Reset View</label>
														<label data-seq="network" data-title="Discussion Network" class="reflection">Reflect & Set Goals</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--<div id="lens" class="tab-pane fade">
									<div class="center-block col-height col-sm-6 col-md-6">
									<div class="grid_tile grid_tile_bar">

											<div class="grid_chart" id="CL_chart1"></div>
										
										</div>
										</div>
									<div class="center-block col-height col-sm-6 col-md-6">
										<div class="grid_tile grid_tile_bar">-->
											<!--<div class="grid_title">Post replies:</div><hr>
											<div id="replies_num" class="grid_number">0</div>
											<div class="grid_text">student(s) have <strong>not posted</strong> a reply</div>-->
											<!--<div class="grid_chart" id="TS_chart1"></div><hr><hr>
											<div class="grid_link">
												<button type="button" class="btn btn-info btn-xs btn-blue" data-toggle="modal" data-target="#myModal">Posted replies</button>
												<button type="button" class="btn btn-info btn-xs btn-red" data-toggle="modal" data-target="#myModal">No replies posted</button>
											</div>
										</div>
									</div>
								</div>-->
								<!-- <div id="sna" class="tab-pane fade">
								<h3>My CoVAA Network Map</h3>
								<div><button id="btn_sna_generate">Reset View / Generate</button></div>
								<div id="sna_container"></div>
								</div> -->
								<div id="21cc" class="tab-pane fade">
									<div class="row">
										<div class="center-block" style="width:520;float:left">
											<div class="grid_tile grid_tile_bar">
												<div class="grid_chart21cc" id="attitudes_chart"></div>
												<div>
													<label data-seq="learningSS" data-title="My Attitudes Towards Learning SS" class="reflection">Reflect & Set Goals</label>
												</div>
											</div>
											<div class="grid_tile grid_tile_bar">
												<div class="grid_chart21cc" id="feel_chart"></div>
												<div>
													<label data-seq="motivation" data-title="My Learning Motivation" class="reflection">Reflect & Set Goals</label>
												</div>
											</div>
										</div>
										<div class="center-block" style="width: 520;margin-left: 540;">
											<div class="grid_tile grid_tile_bar">
												<div class="grid_chart21cc" id="21cc_chart"></div>
												<div>
													<label data-seq="21CC" data-title="My 21st Century Skills Profile" class="reflection">Reflect & Set Goals</label>
												</div>
											</div>
											<div class="grid_tile grid_tile_bar">
												<div id="learning_mindset_container"  style="text-align: center;">
													<text style="font-size:12px;font-weight:bold">
														<label style="padding-bottom:5px;padding-top:7px">MY LEARNING STRATEGIES</label>
													</text>
												</div>
												<div>
													<label data-seq="strategies" data-title="My Learning Strategies" class="reflection">Reflect & Set Goals</label>
												</div>
												<!-- <div class="grid_chart" id="mindset_chart"></div> -->
												<!-- <div style="text-align: center;">
													<text style="color:#333333;font-size:18px;fill:#333333;" y="24">
														<tspan>HOW HEALTHY IS MY LEARNING MINDSET?</tspan>
													</text>
												</div>
									            <text id="learning_goals_title" class="progress-title">My desire to improve skills and knowledge</text>
									            <div class="progress">
									                <div id="learning_goals_bar" class="progress-bar" style="width: 0%;">
									                    <div class="progress-value"></div>
									                </div>
									            </div>
									            <text id="surface_goals_title" class="progress-title">My desire to prove competence and ability</text>
									            <div class="progress">
									                <div id="surface_goals_bar" class="progress-bar" style="width: 0%;">
									                    <div class="progress-value"></div>
									                </div>
									            </div>
									            <text id="social_goals_title" class="progress-title">My desire to help classmates in  their learning</text>
									            <div class="progress">
									                <div id="social_goals_bar" class="progress-bar" style="width: 0%;">
									                    <div class="progress-value"></div>
									                </div>
									            </div>
									            <text id="deep_learning_title" class="progress-title">Focus on deep understanding and learning for life</text>
									            <div class="progress">
									                <div id="deep_learning_bar" class="progress-bar" style="width: 0%;">
									                    <div class="progress-value"></div>
									                </div>
									            </div>
									            <text id="surface_learning_title" class="progress-title">Focus on surface learning and scoring in exams</text>
									            <div class="progress">
									                <div id="surface_learning_bar" class="progress-bar" style="width: 0%;">
									                    <div class="progress-value"></div>
									                </div>
									            </div>
									            <img src="icons/learning_mindset_legend.png"/> -->
											</div>
										</div>
									</div>
								</div>
							</div>
			            </div>
			        </div>
			    </div>
			</div>	
		</div>
			<div class="container-fluid">
				<div id="filter_div" class="row">
				</div>
				<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
			</div>
			
			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-md">

				<!-- Modal content-->
				<div class="modal-content container-fluid">
				  <div class="modal-header">
					<h4 id="modal-title" class="modal-title">Information on SNA</h4>
				  </div>
				  <div id="modal-body" class="modal-body row">
				  	<p>
				  		- The network map is based on you/your classmates’ replies to one another’s comments.<br>
						- Nodes of people who have replied one another are connected by lines.<br>
						- Arrows point in the direction of replies.<br>
						- Thicker lines mean more replies between nodes.<br>
						- The more you post, the bigger your node.<br>
						- The more you reply, the darker your node.<br>
						- Your node will show as a grey square until you make a post.<br>
						- Your node will be a skewed rectange if you have only posted replies.<br>
						- Click the Reset View button to reset your CoVAA Discussion Network Map.
				  	</p>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				</div>

			  </div>
			</div>
		
			<!-- lightbox -->
			<!--HL-->
			<div id="lightbox-bg"></div>
			<div id="lightbox-popup">
				<img id="lightbox-close" src="icons/close2.png" alt="Close" height="38" width="38">

				<h3 id="reflect_title">Reflect & Goal Set</h3>
				<p id="reflect_mini_title"></p>

				<div id="lightbox-content">
					<form action="reflection_handler.php" method="post">
						<input type="hidden" name="request" id="request" value="insertReflection">
						<input type="hidden" name="chart_id" id="chart_id">
						<input type="hidden" name="user_id" value="<?php echo $userID;?>">
						<input type="hidden" name="video_id" id="video_id">

						<label class='nonSNA'>What are my strengths?</label>
						<textarea class='nonSNA' maxlength="140" id="good" name="good" placeholder="Write something.." style="height:40px"></textarea>

						<label id='label_helpOthers'>How can I help others?</label>
						<textarea maxlength="140" id="helpOthers" name="helpOthers" placeholder="Write something.." style="height:40px"></textarea>

						<label id='label_improve'>How can I improve?</label>
						<textarea maxlength="140" id="improve" name="improve" placeholder="Write something.." style="height:40px"></textarea>    

						<label class='nonSNA'>How can others help me?</label>
						<textarea class='nonSNA' maxlength="140" id="helpMe" name="helpMe" placeholder="Write something.." style="height:40px"></textarea>

						<input id="reflection-submit" type="submit" value="Submit">
					</form>
				</div>
			</div>
		
		<!--Load scripts for any visualizations library, + oval related, + click stream related etc-->
		
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="./cytoscape.js-2.4.8/cola.v3.min.js"></script>
		<script type="text/javascript" src="./cytoscape.js-2.4.8/cytoscape.min.js"></script>
		<script type="text/javascript" src="./cytoscape-cose-bilkent/cytoscape-cose-bilkent.js"></script>
		<script type="text/javascript" src="covaa_sna.js"></script>
		<script type="text/javascript">
		var userID = <?php echo $userID;?>;
		var role = <?php echo $_SESSION['role']?>;
		</script>
		<script type="text/javascript" src="student_dashboard.js"></script>
<div id="footer">
<label>
                            CoVAA beta version. © 2017 Dr. Jennifer Pei-Ling Tan. National Institute of Education. NRF EduLab Grant NRF2015-EDU001-IHL09.<br> 
							<span style="font-size: 11;">Powered by the OVAL/CLAS research initiatives at the Centre for Change and Complexity in Learning (C3L), University of South Australia.</span>
                        </label>
</div>
	</body>
</html>