<?php
    require_once(dirname(__FILE__) . "/includes/global_deploy_config.php");

    //require_once(dirname(__FILE__) . '/includes/kaltura/kaltura_functions.php');

    require_once(dirname(__FILE__) . "/includes/common.inc.php");

    //require_once(dirname(__FILE__) . "/includes/auth.inc.php");

    require_once(dirname(__FILE__) . '/database/users.php');

    require_once(dirname(__FILE__) . '/database/media.php');
     

    if(isset($_POST["request"]))
    {
        startSession();
    
        //Check if user is Student, check subjects taught and classes associated
        $isAdmin  = isAdmin($_SESSION['role']);
        $userName = $_SESSION['name'];
        $userID   = $_SESSION['user_id'];
        
        $conn = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database);

        if (mysqli_connect_errno())
        {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        //$db_selected = mysql_select_db($database, $conn);

        mysqli_set_charset("utf8",$conn);


        if($_POST["request"] == "fetchReflection")
            fetchReflection($conn);

        else if($_POST["request"] == "insertReflection")
            insertReflection($conn);

    }
    else
    {
        return;
    }

    function fetchReflection($conn)
    {
        $chart_id = $_POST['chart_id'];
        $userid = $_POST['user_id'];
        $videoid = $_POST['video_id'];

        $good = "";
        $helpOthers = "";
        $improve = "";
        $helpMe = "";

        $sql = "select * from reflections where chart_id = '$chart_id' and user_id = '$userid' and video_id = '$videoid' order by input_date desc";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            $row = $result->fetch_assoc();

            $good = $row["good"];
            $helpOthers = $row["helpOthers"];
            $improve = $row["improve"];
            $helpMe = $row["helpMe"];

            $output = array($good, $helpOthers, $improve, $helpMe);
            echo json_encode($output);
            //echo "have results";
        } 
        else {
            $output = array("", "", "", "");
            echo json_encode($output);
            //echo "0 results";
        }

        $conn->close();
    }

    function insertReflection($conn)
    {
        // Escape user inputs for security
        $chart_id = mysqli_real_escape_string($conn, $_REQUEST['chart_id']);
        $userid = mysqli_real_escape_string($conn, $_REQUEST['user_id']);
        $videoid = mysqli_real_escape_string($conn, $_REQUEST['video_id']);
        $good = mysqli_real_escape_string($conn, $_REQUEST['good']);
        $helpOthers = mysqli_real_escape_string($conn, $_REQUEST['helpOthers']);
        $improve = mysqli_real_escape_string($conn, $_REQUEST['improve']);
        $helpMe = mysqli_real_escape_string($conn, $_REQUEST['helpMe']);


        $sql = "INSERT INTO reflections (chart_id, user_id, video_id, good, helpOthers, improve, helpMe, input_date)
        VALUES ('$chart_id', '$userid', '$videoid', '$good', '$helpOthers', '$improve', '$helpMe', NOW())";

        if ($conn->query($sql) === TRUE) {
            echo json_encode("New record created successfully");
        } else {
            echo json_encode("Error: " . $sql . "<br>" . $conn->error);
        }

        $conn->close();
    }

?>