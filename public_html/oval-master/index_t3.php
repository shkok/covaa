<?php
/**
 *  OVAL (Online Video Annotation for Learning) is a video annotation tool
 *  that allows users to make annotations on videos.
 *
 *  Copyright (C) 2014  Shane Dawson, University of South Australia, Australia
 *  Copyright (C) 2014  An Zhao, University of South Australia, Australia
 *  Copyright (C) 2014  Dragan Gasevic, University of Edinburgh, United Kingdom
 *  Copyright (C) 2014  Negin Mirriahi, University of New South Wales, Australia
 *  Copyright (C) 2014  Abelardo Pardo, University of Sydney, Australia
 *  Copyright (C) 2014  Alan Kingstone, University of British Columbia, Canada
 *  Copyright (C) 2014  Thomas Dang, , University of British Columbia, Canada
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * CoVAA (Collaborative Online Video Annotation & Analysis) is a modified version of
 * OVAL/CLAS that include learning dashboards. By National Institute of Education (NIE)
 * Modified by Simon Yang and Kok Shi Hui, National Institute of Education, Singapore
 *
 */
// John Bratlien and Thomas Dang, 2011-2012
// An ZHAO started to modify this page since Nov of 2013.
// Simon Yang and Kok Shihui modified this page since April 2016 - 2018

    require_once(dirname(__FILE__) . "/includes/global_deploy_config.php");
    require_once(dirname(__FILE__) . "/includes/common.inc.php");
    require_once(dirname(__FILE__) . "/includes/auth.inc.php");
    require_once(dirname(__FILE__) . "/database/media.php");
    require_once(dirname(__FILE__) . "/database/users.php");
    require_once(dirname(__FILE__) . '/includes/kaltura/kaltura_functions.php');
    //require_once(dirname(__FILE__) . "/MyVideo.php");
    error_reporting(0); //Simon added
    //error_reporting(E_ALL ^ E_WARNING); //Simon added
    $isiPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
    startSession();
    //timeoutSession();
    $userName = $_SESSION['name'];
    $userID   = $_SESSION['user_id'];
    $isAdmin  = isAdmin($_SESSION['role']);

    //Setting up Database connection
    $conn = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database);

    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    //$db_selected = mysql_select_db($database, $conn);

    mysqli_set_charset("utf8",$conn); 


    //Retrieve all Tag Categories
    $query = "SELECT * FROM tag_categories;";
    $result = mysqli_query($conn, $query);
    $tag_category = array();
    while ($row = mysqli_fetch_assoc($result)) {
        //var_dump($row);
        $tag_category[] = $row;
    }
    $tag_category = json_encode($tag_category);

 
    //Retrieve all Tags
    $query = "SELECT * FROM tags;";
    $result = mysqli_query($conn, $query);
    $tags = array();
    while($row = mysqli_fetch_assoc($result)) {
        $tags[] = $row;
    }
    $tags = json_encode($tags);

    


    if (compatibilityModeIE9()) {
?>
	<html>
	   <head></head>
    	<body>
            <!-- Google Tag Manager (noscript) -->
            <noscript>
                <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQLCKQP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
            </noscript>
            <!-- End Google Tag Manager (noscript) -->

            You are viewing this page in compatibility view. 
            Compatibility view causes layout problem, please disable it by 
            <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie-9/features/compatibility-view">
               clicking the compatibility view button.
            </a>
        </body>
	</html>
<?php
        exit();
    }
?>
<?php
    if (! browserSupported()) {
        $browserData = getBrowserData();
        $browser    = $browserData["browser"];
        $version    = $browserData["version"];
?>
        <html>
            <h1>Browser version not supported</h1>
            <?php
            if ("IE" == $browser) {
                print "To use CoVAA you must upgrade to IE 9.";    
            } 
            else {
                print "<p>To use CoVAA you must upgrade to the lastest version of <?php$browser?>.</p>";
                print "current version:$version<br />";
            }
         	?>
        </html>
<?php    
        exit();
    }
?>
<?php
    $media  = new media();
    $users      = new users();
    $uiConfig   = $users->getUI($userID);
    $classes    = $users->getClassesUserBelongsTo($userID);

    foreach ($classes as $key=>$row) {
    	$id[$key] = $row['ID'];
    	$name[$key] = $row['name'];
    }

    array_multisort($name, SORT_ASC, $id, SORT_DESC, $classes);

    if (isset($_GET['cid'])) {
        $CID = $_GET['cid'];
    } 
    else {
        $CID = $classes[0]['ID'];
    }

    $groups     = $users->getMyGroups($userID, $CID);
    $test = $media->getVideosByClassID($CID);
    $globalVID = $test[0][video_id];
    // Show Unassigned Video - Disabled for now, unwieldy interface in practice
    // Has video previewing available in the video management page instead

    if ($isAdmin) {
        if (0 != count($media->getVideosWithNoGroup($userID))) {
    		 $groups['U'] = "-- UNASSIGNED VIDEOS";
        }
    }
    $users->close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Collaborative Video Annotation and Learning Analytics</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<meta content="utf-8" http-equiv="encoding">
    	<!-- Debug version: If using the debug version you can remove all the below js / css references --> 
        <!--  <script type="text/javascript" src="../mwEmbed.js" ></script>  -->

        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!-- bootstrap - Optional theme 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">-->

        <script>
            dataLayer = [{'userID': <?php echo $userID; ?> }];
        </script>

        <!-- Google Tag Manager -->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-WQLCKQP');
        </script>
        <!-- End Google Tag Manager -->

        <script>var yesChecked = false;</script>
        
        <script src="youtube-google-analytics-master/lunametrics-youtube.gtm.min.js"></script>
     	 	
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> -->
        <link rel="stylesheet" type="text/css" href="style_t3.css" />
    	
       

    	<!--  Include jQuery, use a local jQuery to deal with Chrome's conservative same domain policy 	
     	<script type="text/javascript" src="kaltura-html5player-widget/jquery-1.4.2.min.js"></script> -->


     	
     	<!--  Include the local Open Source Kaltura player -->
        <!--	<script type="text/javascript" src="includes/kaltura/mwEmbedLoader.php"></script>   -->
    	<!--<style type="text/css">
        	@import url("kaltura-html5player-widget/skins/jquery.ui.themes/kaltura-dark/jquery-ui-1.7.2.css");
    	</style>  
    	<style type="text/css">
    		@import url("kaltura-html5player-widget/mwEmbed-player-static.css");
    	</style>-->

        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>

    	<script type="text/javascript" src="kaltura-html5player-widget/mwEmbed-player-static.js"></script>
    	<script type="text/javascript">
    		/*<!-- put mw.setConfig calls here -->
    		<!-- mw.setConfig('EmbedPlayer.EnableRightClick', false); -->*/
        </script>

        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
        <script type="text/javascript">
        <?php
        	// This part sets javascript variables from PHP based on user setting &/or access ctrl in DB 
            if ("yes" == $uiConfig['annotations_enabled']) {
                print "\tvar annotationsEnabled = true;\n";
            } 
            else {
                print "\tvar annotationsEnabled = false;\n";
            }
            
            if ("PSYC" == $uiConfig['annotation_mode']) {
                print "\tvar psycMode = true;\n";
            } 
            else {
                print "\tvar psycMode = false;\n";
            }
            
            printSharedClientServerConstants();
            
            if (isset($_GET['gid'])) {
                $GID = $_GET['gid'];
            } 
            else {
                $groupIDs = array_keys($groups);
                $GID = $groupIDs[0];
            }
        	//print "GID:$GID<br />";
            
        	    
    	    if (is_numeric($GID)) {
    	        $videos = $media->getVideosByGroupID($userID, $GID);
    	    } 
            else {
    			$videos = $media->getVideosWithNoGroup($userID);
    	    }
        	    
        	
            // grab the first available video if none set
            if(isset($_GET['vid'])) {
                $VID = $_GET['vid'];
            }
            else {
                $VID = array_shift(array_keys($videos));
            }
            
            // strip out hash
            $VID = (string) str_replace("#", "", $VID);
            $duration = $videos[$VID]['duration'];
            $conversionStatus = $videos[$VID]['conversion_complete'];
            $thumbnailURL = $videos[$VID]['thumbnail_url']; 
            if (is_null($duration)) $duration=0;
        	$flavors = $media->getFlavors($VID);
        	
        	/* sanity check, there are multiple ways that kaltura conversion might fail
        	 * silently, so do a client side, right-before-view check as a last line of defense
        	 */

            /*
            	if (
            		empty($flavors) || 
            		count($flavors) <= MINIMUM_FLAVOR_COUNT ||
            		$conversionStatus != CONVERSION_COMPLETED
            		) {
            		$flavorsFromKMC = getFlavorsFromKMC($VID);
            		
            		if (!empty($flavorsFromKMC)) {
            			foreach ($flavorsFromKMC as $flavor) {
            				if ("" != $flavor['codec_id']) {
            					$media->addFlavor($flavor['flavor_id'],
            							$VID,
            							$flavor['codec_id'], 
            							$flavor['file_ext']);
            				}
            			}
            			$flavors = $media->getFlavors($VID);
            		}
            	}
            	
            	// MIGRATION MODE
            	if (kalturaCdnURL_INTERIM != null && kalturaCdnURL_INTERIM != "") {
            		$flavorsFromKMC = getFlavorsFromKMC($VID);
            	
            		if (empty($flavorsFromKMC)) {
            			$kalturaCdnURL = kalturaCdnURL_INTERIM;
            		}
            	}
            */

            //Retrieve number of questions
            $query = "SELECT qnsNo FROM annotationQns WHERE video_id = '$VID';";
            $result = mysqli_query($conn, $query);
            $row = mysqli_fetch_assoc($result);

            if($row !== null)
                $num_of_qns = $row["qnsNo"];
            else
                $num_of_qns = 0;

            print "\tvar num_of_qns        = \"$num_of_qns\";\n";

        	$media->close();
            print "\tvar mediaDuration = $duration;\n";
            // print "\tvar mediaDuration = 678;\n";
            print "\tvar videoID       = \"$VID\";\n";
            print "\tvar userID        = \"$userID\";\n";

        ?>

            $(document).ready(function() {

                $(function () {
                    $('[data-toggle="popover"]').popover()
                })
                
    			//Simon Added to pause video when annotation item clicked.
    			$('.annotation-list-item').click(function() {
    				player1.pauseVideo();
    			});
    			
                $('#choose-class').change(function() {
                    location.href = getPathFromURL(window.location.href) + "?class_id=" + $(this).val(); 
                });
                
                $('#span-video').click(function() {
                    if ($(this).is(':checked')) {
                        $('#annotation-start-end-time');
                        $('#annotation-end-time');
                        $('#annotation-start-end-time').css('opacity', '0.5');
    					//console.log("span video");
                    }
                    else {
                        $('#annotation-start-end-time').css('opacity', '1.0');
                    }
                });

                //Hide Questions No. if there are no questions.
                if(num_of_qns == 0){
                    $("#qno").hide();
                }

                //Populate Annotation/Comment/Reply forms with tags buttons
                var tag_category = <?php echo $tag_category; ?>;
                var tags = <?php echo $tags; ?>;
                
                for(var i=0; i<tag_category.length; i++)
                {
                    $("#tag_btns").before('<a class="annotation-label tag_category_name" href="icons/thinkingskills.jpg" target="_blank">'+tag_category[i]['category_name']+': </a>');
                    $("#tag_btns").append('<input type="hidden" name="annotation-tags" id="annotation-tags" value="" />');

                    var tag_btn_class_name = tag_category[i]['ID'] + "_tag_button";

                    for(var j=0; j<tags.length; j++)
                    {
                        $("#tag_btns").append('<label class="'+tag_btn_class_name+' tag_btn" value="'+tags[j]['tag']+'" data-toggle="popover" title="'+tags[j]['tag']+'" data-placement="top" data-trigger="hover" data-html="true" data-content="'+tags[j]['description']+'">'+tags[j]['tag_title']+'</label>');
                    }

                    tag_btn_class_name = "."+tag_btn_class_name;

                    $(tag_btn_class_name).click(function() {

                        $(tag_btn_class_name).css('background-color','#d9e2e9');
                        
                        $(this).css('background-color','lightblue');
                        
                        $('#annotation-tags').val($(this).attr("value"));

                        //alert($(this).attr("value"));
                    });
                }
            });

            function getPathFromURL(url) {
                return url.split("?")[0];
            }

            function jumpBox(list) {
                location.href = list.options[list.selectedIndex].value;
            }
        </script>

        <!-- Annotations related JS are here -->
     	<script type="text/javascript">var role = "<?= $_SESSION['role'] ?>";</script>
        <script type="text/javascript">var grpId = "<?=$GID ?>";</script>
        <script type="text/javascript" src="ui_t3.js"></script>
     	<!-- Video players wrapper functions are here -->
     	<script type="text/javascript" src="video_functions.js"></script>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <style>
            .navbar, .well {
                margin-bottom: 0px;
            }
            .navbar-brand img {
                width: 80px;
            }
        </style>

    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQLCKQP"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        
        <nav class="navbar navbar-expand-sm navbar-light bg-light">
            <a class="navbar-brand" href="../oval-master/index_t3.php"><img src="./icons/covaa_logo2_small.png" alt="CoVAA"></a>
            <span>Collaborative Video Annotation & Analytics</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Watch Videos <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../oval-master/student_dashboard_test.php">My Learning Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="logout" onClick="onLogOutEvent()">Logout</a>
                    </li>
                </ul>
            </div>
        </nav>

    	<div id="wrapper">
            <div class="loading">Loading</div>
        	<?php
            	$conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);

                if (!$conn) {
                    die('Not connected : ' . mysql_error());
                }

                $db_selected = mysql_select_db($database, $conn);

                mysql_set_charset("utf8",$conn);

                //print_r($videoID);

                $result = mysql_query("SELECT * FROM commentQns WHERE video_id = '$VID' AND groupId= $GID");

                $commentQns = mysql_fetch_assoc($result);
            ?>
            <div id="comments">
                <h3>Let's Discuss and Learn Together!</h3>
        		<div class="qns">
                    <span class="label" style="font-size:75%; white-space:pre-wrap;"><?php echo $commentQns['user_name'];?></span>
                    <br>
                    <span style="font-size:80%;word-wrap:break-word"><?php echo $commentQns['description'];?></span>
                    <br>

                    <!--<img src="icons/add-comment.png" id="add-comment" style="float:right;margin:8px 6px 8px 0px;cursor:pointer;" alt="add a comment" onClick="javascript:player1.pauseVideo();"/> -->
                </div>
                <div id="add-comment-div"><button type="button" id="add-comment" onClick="javascript:player1.pauseVideo();">Post Comment</button></div>
                <ul>
                    <li></li>
                </ul>
                <img class="logo-placeholder" src="icons/covaa_logo2.png"></img><br>
                <label id="copyright_text">
                    Powered by CLAS/OVAL 2015 Shane Dawnson, Aberlardo Pardo and colleagues.<br>
                    CoVAA Alpha version © 2016, Dr Jennifer Pei-Ling Tan, National Institute of Education<br>
                    Funded by Edulab, National Research Foundation NRF2015-EDU001-IHL09.
                </label>
            </div>

            <div id="admin-bar">
                <label>Hello <?php echo $userName; ?></label><br>
                <table id="admin-bar-table">
                    <tr>
                        <td>
                            <label>Subject:</label>
                        </td>
                        <td>
                            <form id="class" name="class" method="post" action="">
                                
                                <?php 
                                    $noCourses = (0 == count($classes)) ? "disabled" : "";
                                ?>
                                <select name="classJumpMenu" id="classJumpMenu" <?php echo $noCourses?> onChange="jumpBox(this.form.elements[0])" style="width:100%;" >
                                <?php 
                                    if (0 == count($classes)) {
                                        print "<option>-- NO COURSES</option>";
                                    }
                                    else {
                            
                                        for ($i=0; $i<count($classes); $i++): 
                                            $ID     = $classes[$i]['ID'];
                                            $name   = $classes[$i]['name'];
                                            ($CID == $ID) ? $selected = "selected=\"selected\"" : $selected = "";
                                ?>
                                            <!--Simon's comment: Sets the Subject dropdown options as GET variables strings to refresh the page with selected values? But the form is POST?-->
                                            <option value="index_t3.php?cid=<?php echo "$ID"; ?>"<?php echo"$style $selected>$name"?></option>
                                <?php 
                                        endfor; 
                                    }
                                ?>
                                </select>
                            </form>        
                        </td>
                    </tr> 

                    <tr>
                        <td>
                            <label>Class:</label>
                        </td>
                        <td>
                            <form id="group" name="group" method="post" action="">
                                
                                <?php 
                                    $noGroups = (0 == count($groups)) ? "disabled" : "";
                                ?>
                                <select name="groupJumpMenu" id="groupJumpMenu" <?php echo $noGroups?> onChange="jumpBox(this.form.elements[0])" style="width:100%;" >
                                <?php 
                                    if (0 == count($groups)) {
                                        print "<option>-- NO GROUPS</option>";
                                    }
                                    else {
                                        foreach ($groups as $ID=>$name): 
                                            ($GID == $ID) ? $selected = "selected=\"selected\"" : $selected = "";
                                            
                                            // Can't seem to apply style to an input option, what to do?
                                            // ('U' == $ID) ? $style = "style=\"color:red\"" : $style = "";
                                ?>
                                            <option value="index_t3.php?cid=<?php echo "$CID&gid=$ID"; ?>" <?php echo "$style $selected > $name"; ?></option>
                                <?php 
                                        endforeach; 
                                    }
                                ?>
                                </select>
                            </form>        
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Video:</label> 
                        </td>
                        <td>
                            <form id="video" name="video" method="post" action="">
                                  
                                <?php if (count($videos) == 0) $videosDisabledStr='disabled="disabled"'; ?>
                                
                                <select name="jumpMenu" id="jumpMenu" onChange="jumpBox(this.form.elements[0])" style="width:100%;" 
                                <?php echo $videosDisabledStr; ?>>
                                
                                <?php
                                    ("flag" == $uiConfig['annotation_mode']) ? $queryParam = "&flag_mode=1" : $queryParam = "";
                                    
                                    if (0 == count($videos)) {
                                        print "<option>-- NO VIDEOS</option>";
                                    } 
                                    else {
                                        // fetch videos
                                        foreach ($videos as $video) {
                                            if ($isAdmin && sizeof($classes) > 1) {
                                                if ($classID != $video['class_id']) continue;
                                            }
                                            
                                            $videoID = $video['video_id'];
                                            ($VID == $videoID) ? $selected = "selected=\"selected\"" : $selected = "";
                                            $title = $video['title'];
                                            print "\t\t<option value=\"index_t3.php?cid=$CID&gid=$GID&vid=$videoID$queryParam\" $selected>$title</option>\n";
                                        }
                                    }
                                ?>                    
                                </select>
                            </form>        
                        </td>
                    </tr>
                </table>

                <!-- Simon: Why got this redundent if else here? So what if theres more than 1 group? -->
                <?php if (count($groups) > 1): ?>
                <?php else: ?>
                <?php endif; ?>


                <?php if ($isAdmin == true): ?>
                    <a href="video_management.php?cid=<?php echo "$CID"?>&gid=<?php echo "$GID"?>" style="float:right;padding-left:20px;">Upload</a> 
                    <a href="teacher_dashboard.php" style="float:right;padding-left:20px;">Learning Dashboard</a> 
                    <!--  <a href="video_management.php" >admin</a> -->
                <?php endif; ?>            
            </div>
            <?php
             //if (0 == count($videos)) {
             //    print "<img id=\"vp\" src=\"icons/novideo.jpg\">";
             //}
             //else {
             //    if (empty($flavors)) {
             //        print "<img id=\"vp\" src=\"icons/noflavors.jpg\">";
             //    } else {
             //           $isChrome = (stripos($_SERVER['HTTP_USER_AGENT'], "Chrome") !== false);
             //            $isSafari = (stripos($_SERVER['HTTP_USER_AGENT'], "Safari") !== false);
             // Chrome and Safari does not preload some Kaltura videos properly, could be either a Kaltura problem or
             // that Chrome and Safari does not follow the HTML5 <video> standard.
             // note: we explicitly control whether the preload attribute is written out at all as well, since different browser
             // handles omitting this differently
             //            $preloadSetting = ($isChrome || $isSafari) ? "preload=\"none\"" : "";
             //            $videoWidth = 640;
             //            $videoHeight = 480;
             //            global $pid, $spid;
             //            $pid = 1591032;
             //            $spid = 159103200;
             //            echo "-----------This is: $kalturaCdnURL --------";
             //$posterURL = "http://cdnbakmi.kaltura.com/p/1591032/sp/159103200/thumbnail/entry_id/0_sin91lmw/version/100000";
             //$posterURL = $kalturaCdnURL . "/p/$pid/sp/$spid/thumbnail/entry_id/$VID/width/$videoWidth/height/$videoHeight";
             //$posterURL = $kalturaCdnURL . "/p/$pid/sp/$spid/thumbnail/entry_id/$VID/width/$videoWidth/height/$videoHeight";
             // $posterURL = $kalturaCdnURL . "p/$pid/sp/$spid/thumbnail/entry_id/$VID/width/$videoWidth/height/$videoHeight";
                                   
             // $posterURL = $kalturaCdnURL . "watch?v=$VID";
                                    
             //             $posterURL = MyVideo::load("1");   
             //$posterURL = http://cdnbakmi.kaltura.com/p/1591032/sp/159103200/thumbnail/entry_id/0_sin91lmw/width/$videoWidth/height/$videoHeight;
            ?>
            <?php
            //         print "<video id=\"vp\" poster=\"$posterURL\" $preloadSetting>";
            // foreach ($flavors as $fileExt=>$flavorID) {
            //  if ("flv" != $fileExt) {
            // $url = getVideoURL($VID, $flavorID, $fileExt);
            //                           print "\t<source src=\"$posterURL\" />\n";
            // }
            // }
            //           print "</video>";
            //  }
            //  }
            ?>
            <!--</div>-->
            <div id="player"></div>
            <script>
                // 2. This code loads the IFrame Player API code asynchronously.
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                //tag.src = "https://clas.unisa.edu.au/youtube_player_api"; //Original source commented off by Simon Yang
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                
                // 3. This function creates an <iframe> (and YouTube player)
                //    after the API code downloads.
                var youtubeTime;
                var player1;

                window.onYouTubeIframeAPIReady = function() {
                	player1 = new YT.Player('player', {
                	height: '390',
                	width: '550',
                	videoId: '<?php echo $VID ?>',
                	playerVars: {rel: 0, enablejsapi: 1},
                	events: {
                	'onReady': onPlayerReady,
                	'onStateChange': onPlayerStateChange
                	}
                	});
                }
                //youtubeTime = player1.getCurrentTime();
                // 4. The API will call this function when the video player is ready.
            	function onPlayerReady(event)
                {
                    //event.target.playVideo(); // play video once ready (auto play)
                }
                	/* window.onPlayerReady(event) = function() {
                		event.target.pauseVideo();
                	} */
                // 5. The API calls this function when the player's state changes.
                //    The function indicates that when playing a video (state=1),
                //    the player should play for six seconds and then stop.
                var done = false;

                function onPlayerStateChange(event) 
                {
                    if (event.data == YT.PlayerState.PLAYING)
                    {
                        //alert("video is playing now!");
                        insertPlay(player1.getCurrentTime());
                        //alert("The video re-play start at position: " + player1.getCurrentTime());
                        ////_gaq.push(['_trackEvent', 'Videos', 'Play', '<?php echo $title?>']);
                        ////ga('send', 'event', 'Videos', 'Play', '<?php echo $title?>');
                        // _gaq.push(['_trackEvent', 'User', 'Username', '<?php echo $userName?>']);
                        // ga('send', 'User', 'Username', 'Username', '<?php echo $userName?>']);
                        ////_gaq.push(['_setCustomVar', 1, 'Username', '<?php echo $userName?>']);
                        ////_gaq.push(['_setCustomVar', 2, 'UserID', '<?php echo $userID?>']);
                        // setTimeout(stopVideo, 6000);
                        done = true;
                	}
                	else if (event.data == YT.PlayerState.PAUSED) 
                    {
                    	//alert("video is paused!");
                    	//alert("The video pause at position: " + player1.getCurrentTime());
                    	insertPause(player1.getCurrentTime());
                    	////_gaq.push(['_trackEvent', 'Videos', 'Pause', '<?php echo $title?>']);
                    	////ga('send', 'event', 'Videos', 'Pause', '<?php echo $title?>');
                	}
                	else if (event.data == YT.PlayerState.ENDED)
                    {
                    	//alert("watch to end of the video!");
                    	//alert("The video end position is: " + player1.getCurrentTime());
                    	insertEnd(player1.getCurrentTime());
                    	////_gaq.push(['_trackEvent', 'Videos', 'Watch to End', '<?php echo $title?>']);
                    	////ga('send', 'event', 'Videos', 'Watch to End', '<?php echo $title?>');
                	} 
                	else if (event.data == YT.PlayerState.BUFFERING)
                    {
                    	//alert("video is buffering!");
                    	//alert("The video buffering position is: " + player1.getCurrentTime());
                    	insertBuffer(player1.getCurrentTime());
                    	////_gaq.push(['_trackEvent', 'Videos', 'Buffering', '<?php echo $title?>']);
                    	////ga('send', 'event', 'Videos', 'Buffering', '<?php echo $title?>');
                	}
                	else if (event.data == YT.PlayerState.CUED)
                    {
                    	// alert("video is cued!");
                    	insertCue(player1.getCurrentTime());
                    	////_gaq.push(['_trackEvent', 'Videos', 'Cueing', '<?php echo $title?>']);
                    	////ga('send', 'event', 'Videos', 'Cueing', '<?php echo $title?>');
                	}
                }

                function stopVideo()
                {
                	player.stopVideo();
                }
                /* window.stopVideo() = function() { //Simon block commented
                player1.stopVideo();
                } */
            </script>

            <script>
                function insertPlay(t){
                    console.log("insertPlay function called!");
                     $.ajax({
                        type: "POST",
                        url: "ajax/event_record.php",
                        data: {video_id: '<?php echo $VID ?>', play_start_position:t},
                        success: function(data) {
                        debug("data " + data);
                        },
                        async: false
                    });
                }
            </script>

            <script>
                function insertPause(t){
                    console.log("insertPause function called!");
                     $.ajax({
                        type: "POST",
                        url: "ajax/pause_record.php",
                        data: {video_id: '<?php echo $VID ?>', pause_position:t},
                        success: function(data) {
                        debug("data " + data);
                        },
                        async: false
                    });
                }
            </script>

            <script>
                function insertEnd(t){
                    console.log("insertEnd function called!");
                     $.ajax({
                        type: "POST",
                        url: "ajax/end_record.php",
                        data: {video_id: '<?php echo $VID ?>', end_position:t},
                        success: function(data) {
                        debug("data " + data);
                        },
                        async: false
                    });
                }
            </script>

            <script>
                function insertBuffer(t){
                    console.log("insertBuffer function called!");
                     $.ajax({
                        type: "POST",
                        url: "ajax/buffer_record.php",
                        data: {video_id: '<?php echo $VID ?>', buffer_position:t},
                        success: function(data) {
                        debug("data " + data);
                        },
                        async: false
                    });
                }
            </script>

            <script>
                function insertCue(t){
                    console.log("insertCue function called!");
                     $.ajax({
                        type: "POST",
                        url: "ajax/cue_record.php",
                        data: {video_id: '<?php echo $VID ?>', cue_position:t},
                        success: function(data) {
                        debug("data " + data);
                        },
                        async: false
                    });
                }
            </script>

            <script type='text/javascript'>
            	
                $(function(){
                	/*$("a.reply_annotation").click(function() {
                		
                		var id = $(this).attr("id");
                        console.log(id);
                		$("#parent_id").val(id);
                		//$("#name").focus();
                	});*/
                	
                    //ORIGINAL CODE -> $("a.reply_annotation").live('click', function(){
                    $( document ).on('click', 'a.reply_annotation', function() { 
                        var id = $(this).attr("id");
                        console.log(id);
                		$("#parent_id").val(id);
                		//$("#name").focus();
                	});
                });
            </script>

            <div id="message-dialog" title="Alert!" style="display:none;z-index:999;">text goes here...</div>
            <img id="recording-annotation" src="icons/annotating.png" alt="recording annotation" style="display:none;" height="19" width="120" />
            <div id="annotation-form">
                <div id="annotation-title-bar" style="background-color:#3f51b5;color:white;border-bottom:1px solid #888;width:400px;margin-bottom:20px;margin-left:-20px;display:block;height:25px;" >
                   <div id="annotation-title" style="float:left;padding:2px;">Post Annotation</div>
                   <a href="#" id="close-annotation" style="float:right;padding:4px;margin:0;text-decoration:none;border:0;">
                        <img src="icons/close-button.gif" style="padding:0;margin:0;border:0;" width="17" height="17" alt="close annotation dialog button"  onClick="player1.playVideo();"/>
                   </a>
                </div>
                <div id="annotation-start-time" class="annotation-label"></div>
    		    <?php if (! $isiPad) { ?>
                        <img id="start-time-rwd" src="icons/rwd-button.png" style="margin-left:5px;" alt="rewind start time" />
                        <img id="start-time-fwd" src="icons/fwd-button.png" style="margin-left:8px;" alt="fast forward start time" />
    		    <?php } ?>
                <br>
                <form method="post" action="">
                    <!--<div class="annotation-label">type:
                    <select id="annotation-type"> 
                      text
                       <option>video</option>  WEBCAM ANNOTATION 
                     </select>
                        </div>
                    <div class="annotation-label">private
                    <input type="checkbox" name="annotation-privacy" id="annotation-privacy" style="" /><br /></div>
                    <br style="margin-bottom:10px" />
                    <div id="webcam" style="margin-bottom:1em;display:none;"></div>--> <!-- WEBCAM ANNOTATION, turn on and off "display:none;" -->
                     <div class="annotation-label" id="qno" style=""><label>Question:</label><div id="questions_btns"></div></div>
                     <input type="hidden" name="aqns" id="aqns" value="0" />
                     <span style="color:red" id="qnoError"></span><br>
                     <!--<input type="text" name="aqns" id="aqns" size="31.5" value="" />-->
                     <!--<span style="color:red" id="commentError"></span>-->
                    <!--<div class="annotation-label" style="">
                        <a href="icons/criticallens.jpg" target="_blank">Critical Lens &nbsp&nbsp</a>
                    </div>-->
                    <!--<input type="text" name="annotation-tags" id="annotation-tags" size="30" value="" />-->
                    <!-- <select style="width:19em;" name="annotation-tags" id="annotation-tags" >
                          <option value="Message/Issue" selected="true">Message</option>
                          <option value="Purpose">Purpose</option>
                          <option value="Audience">Audience</option>
                          <option value="Viewpoint">Point of View</option>
                          <option value="Assumption">Assumption</option>
                          <option value="Inference/Interpretation">Inference</option>
                          <option value="Consequences/Impact">Impact</option>
                          <option value="Evidence">Evidence</option>
                    </select>-->
                    

                    <!-- <div class="annotation-label" style=""></div> -->
                    <div id="tag_btns"></div>
                    <span style="color:red" id="tagError"></span><br>
                    <!--<select style="width:19em;"name="annotation-tags1" id="annotation-tags1"">
                        <option value="Ideate/Reflect" selected="true">I think that...</option>
                        <option value="Justify/Explain">I think so because...</option>
                        <option value="Agree/Validate">I agree...</option>
                        <option value="Disagree/Challenge">I disagree...</option>
                        <option value="Inquire/Clarify">I need to ask...</option>
                    </select>-->

                    <div id="desc" class="annotation-label" style="float:left;">Description:</div>
                    <br>
                    <textarea id="annotation-description" name="annotation-description" rows="8" cols="48"></textarea><br>
                    <span style="color:red"" id="descError"></span><br>

    				<input type='hidden' name='parent_id' id='parent_id' value='0'/>
                    <?php 
                        print "\t\t<input type='hidden' name='groupId' id='groupId' value=\"$GID\"/>\n";
                        print "\t\t<input type='hidden' name='classId' id='classId' value=\"$CID\"/>\n";
                    ?>
                    <?php
                        $conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);

                        if (!$conn) {
                            die('Not connected : ' . mysql_error());
                        }

                        $db_selected = mysql_select_db($database, $conn);
                        mysql_set_charset("utf8",$conn);
                        //print_r($videoID);
                        $result = mysql_query("SELECT * FROM media WHERE video_id = '$VID'");
                        $student = mysql_fetch_assoc($result);
                    ?>
                            
                    <?php if($student['point_one']){ ?>
                        <script>var yesChecked = true; </script> 
                        <!--
                        <div class="annotation-label" style="display: inline-block;">Level of<br> confidence
                            <select name="confidence_type" id="confidence_type" selected="true">
                                <option>very high</option>
                                <option>high</option>
                                <option>medium</option>
                                <option>low</option>
                                <option>very low</option> 
                            </select
                        </div>
                        -->
                    <?php } ?>
     
                </form>

                <div id="form-buttons">buttons go here...</div>
            </div><!--End of annotation form div-->

            <div id="annotation-form1">
                <div id="annotation-title-bar" style="background-color:#3f51b5;color:white;border-bottom:1px solid #888;width:400px;margin-bottom:20px;margin-left:-20px;display:block;height:25px;" >
                    <div id="annotation-title" style="float:left;padding:2px;">Reflection Points</div>
                    <a href="#" id="close-annotation1" style="float:right;padding:4px;margin:0;text-decoration:none;border:0;">
                        <img src="icons/close-button.gif" style="padding:0;margin:0;border:0;" width="17" height="17" alt="close annotation dialog button" />
                    </a>
                </div>
                <div id="annotation-start-time" class="annotation-label"></div>
                <?php if (! $isiPad) { ?>
                    <img id="start-time-rwd" src="icons/rwd-button.png" style="margin-left:5px;" alt="rewind start time" />
                    <img id="start-time-fwd" src="icons/fwd-button.png" style="margin-left:8px;" alt="fast forward start time" />
                <?php } ?>
                <?php
                    $conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);
                    if (!$conn) {
                        die('Not connected : ' . mysql_error());
                    }

                    $db_selected = mysql_select_db('prod_annotation_tool', $conn);
                    mysql_set_charset("utf8",$conn);
                    //print_r($VID);
                    $result = mysql_query("SELECT * FROM media WHERE video_id = '$VID'");
                    $student = mysql_fetch_assoc($result);
                    //print_r($student);

                    if ($student['point_one']){
                ?>
                        <form method="post" action="javascript:void(0);closeAnnotationDialog1();">
                            <h5>Did your comment/reply meet the Universal Intellectual Standards (ACTS) of: </h5>
                            <table width ="80%">
                        <?php 
                            if (($student['point_two']==NULL || $student['point_two']=='NULL')&&($student['point_three']==NULL || $student['point_three']=='NULL')){
                        ?>
                                <tr>
                                    <td>1.  <?php echo $student['point_one'];?></td>
                                    <td>
                                        <input type="radio" name="choice" value="YES"><font size="2">Yes</font>                
                                        <input type="radio" name="choice" value="NO"><font size="2">No</font>
                                    </td>
                                </tr>
                        <?php
                            }
                            else if ($student['point_three']==NULL || $student['point_three']=='NULL'){
                        ?>
                                <tr>
                                    <td>1.  <?php echo $student['point_one'];?></td>
                                    <td>
                                        <input type="radio" name="choice" value="YES"><font size="2">Yes</font>               
                                        <input type="radio" name="choice" value="NO"><font size="2">No</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2.  <?php echo $student['point_two'];?></td>
                                    <td>
                                        <input type="radio" name="choice1" value="YES"><font size="2">Yes</font>              
                                        <input type="radio" name="choice1" value="NO"><font size="2">No</font>
                                    </td>
                                </tr>
                        <?php
                            }
                            else{
                        ?>  
                                <tr>
                                    <td>1.  <?php echo $student['point_one'];?></td>
                                    <td>
                                        <input type="radio" name="choice" value="YES"><font size="2">Yes</font>
                                        <input type="radio" name="choice" value="NO"><font size="2">No</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2.  <?php echo $student['point_two'];?></td>
                                    <td>
                                        <input type="radio" name="choice1" value="YES"><font size="2">Yes</font>
                                        <input type="radio" name="choice1" value="NO"><font size="2">No</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3.  <?php echo $student['point_three'];?></td>
                                    <td>
                                        <input type="radio" name="choice2" value="YES"><font size="2">Yes</font>
                                        <input type="radio" name="choice1" value="NO"><font size="2">No</font>
                                    </td>
                                </tr>
                        <?php
                            }
                        ?>
                            </table>

                            <h5>If you selected "No", you can go back and edit your comment/reply after you submit this reflection.</h5>
                            <p> 
                                <input type="submit" name="submit" id="submit" value="Submit" />
                            </p>
                        </form>
               <?php             
                    }

                    $answer1 = $_POST['choice'];
                    $answer2 = $_POST['choice1'];
                    $answer3 = NULL;

                    if(isset($_POST['submit'])){
                        $conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);
                        
                        if (!$conn) {
                            die('Not connected : ' . mysql_error());
                        }
                        
                        $db_selected = mysql_select_db($database, $conn);
                        mysql_set_charset("utf8",$conn); 
                        $result = mysql_query("INSERT INTO feedback VALUES (NULL, '$VID', '$userID', '$userName', '$answer1', '$answer2', '$answer3')"); 
                    }
               ?>
            </div>
       
            <?php
                if ($flagMode) {
                    $src        = "icons/download-flags.png";
                    $style      = "background-image:url('icons/tab-flag.png')";
                    $queryStr   = "video_id=$VID&viewAll=1&flag_mode=1";
                }
                else {
                    // annotation mode
                    $src        = "icons/download-annotations.png";
                    $queryStr   = "video_id=$VID&viewAll=1&flag_mode=0&groupId=$GID";
                } 
            ?>

            <div id="buttons">
            	<div style="float:left;display:inline;">
            <!--	<span class="video_download_label" style="font-size:14px;display:inline;">Video Download:</span>  -->   
            <?php
                // TODO: add on off switch in video management page
                //	if ($isAdmin == true) { 
                //      if ($flavors != null && !empty($flavors)) {
                //	    print "<br><span class=\"video_download_link\" style=\"font-size:14px;\"><a href=\"" . getVideoDownloadURL($VID) . "\">Original Format</a></span>";
                //	} else {
                //		print "<br><span>Unavailable</span>";
                //	}	
            ?>		
        		</div>
                <a href="ajax/download_annotations.php?<?php echo $queryStr?>" target="_blank" style="border-style:none;outline-style:none;">
                    <img id="download-annotations" src="<?php echo $src?>" style="border-style:none;outline-style:none;" alt="download annotations" /> 
                  <!--  <img id="download-annotations" src="icons/download-annotations.png" style="border-style:none;outline-style:none;" alt="download annotations" /> -->
                </a>
                <img id="add-annotation" src="icons/add-annotation.png" alt="add an annotation" style="" onClick="javascript:player1.pauseVideo();"/>
            <?php
                $users = new users();
                ($users->trendlineVisible($userID, $VID)) ? $style = "block" : $style = "none";
                $users->close();
            ?>
            </div>
        
            <div id="annotation-container">
                <h3 style="font-weight:normal;">
                    <!-- <div id="annotation-view" style="display:<?php "$style;color:#fff;float:left;"?>">
                        view: <span style="cursor:pointer;">mine</span> / <strong>all</strong> 
                    </div> -->
                    <br><img src="icons/annotation-legend.jpg" style="position:relative;top:-19px;left:253px;" />
                </h3>
                <div id="annotation-list"></div> <!-- Annotations go here -->
            </div>
            <div id="annotation-trends">
                <h3 style="float:left;width:auto;">Trends</h3>
                <form id="search-annotations" style="display:inline;float:right;padding-right:4px;">
                	<label style="color:#fff;font-size:14px;">&nbsp;search</label>
                	<input type="text" size="15em" name="search" style="height:inherit" />
                	<label style="color:#fff;font-size:11px;">&nbsp;by content</label>
                	<input type="checkbox" class="search_criteria" id="search_by_content" style="height:inherit" checked />
                	<label style="color:#fff;font-size:11px;">author</label>
                	<input type="checkbox" class="search_criteria" id="search_by_author" style="height:inherit" checked />
                	<label style="color:#fff;font-size:11px;">tag</label>
                	<input type="checkbox" class="search_criteria" id="search_by_tag" style="height:inherit" checked />
                	<!-- <label style="color:#fff;font-size:11px;">auto-search</label>
                	<input type="checkbox" id="auto_hover_search" style="height:inherit" /> -->
                </form>
                <div style="clear:both"></div>
                <div id="trends" style="clear:left;height:100%;width:100%;overflow:display;cursor:pointer;">Annotation trends</div>
            </div>
            <div style="clear:both"></div>
        </div> <!-- the wrapper, for page centering -->
        <!--  <div id="univbranding"><img src="icons"></img></div>  -->
        <div id="mydiv">
            <b>Please run CoVAA in Chrome or Safari. CoVAA is not compatible with IE.</b>
            <!--<p style="font-size:12px;margin-top:0;"><b>Powered by CLAS/OVAL 2015 Shane Dawson, Aberlardo Pardo and colleagues.<br>
            CoVAA Alpha version &copy; 2016, Dr Jennifer Pei-Ling Tan, National Institute of Education<br>
            Funded by Edulab, National Research Foundation NRF2015-EDU001-IHL09.</b></p>-->
        </div>
        <!-- Put error messages here (after determine all error conditions in the rest of the page) -->
        <script type="text/javascript">
        <?php 
    		if (isset($noCourses) && $noCourses != "") {
        ?>
    			alert("You are not currently enrolled in any course that uses CoVAA.\n\nIf this is in error, please contact your teacher.");
        <?php 
    		}
        ?>
        </script>
    </body>
</html>