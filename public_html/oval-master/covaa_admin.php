<?php

include_once(dirname(__FILE__) . "/includes/global_deploy_config.php");
include_once(dirname(__FILE__) . "/includes/common.inc.php");
include_once(dirname(__FILE__) . "/includes/auth.inc.php");
include_once(dirname(__FILE__) . "/database/users.php");

include_once("$folderPreTrail/config/clas/$configFolderName/db_config.php");

startSession();

$Simon_hashed_id = "1";

$hashedCurrentUser = getCurrentHashedUId();

if ($DEPLOYMENT_NAME == "covaa") {
	if (($hashedCurrentUser != $Simon_hashed_id)&&($hashedCurrentUser != "391"))
	{
		notAuthorized();
	}
}
else
{
	notAuthorized();
}

//=======================Connect to OVAL and Wordpress databases==================================
$ovaldb = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database);

if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: (" . $ovaldb->connect_errno . ") " . $ovaldb->connect_error;
}

$wpdb = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database_wp);

if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: (" . $wpdb->connect_errno . ") " . $wpdb->connect_error;
}
//================================================================================================

//Retrieve data for initial displaying
syncUsers();
?>

<html>
	<head>
	<style>
		body {
			width:100%;
			max-width: 1080px;
		}
		.box {
			width:80%;
			padding: 30px;
			border: 1px solid black;
		}
	</style>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	
	</head>

	<body>
		<h1>COVAA Administrator Page</h1>
		<div>
			<form action="covaa_admin_ajax_handler.php" id="manage_users" method="post">
				<fieldset>
					<legend>Manage Class Users</legend>
					<span>Select Function:</span>
					<select name="request" id="selectUserFunction">
						<option value="addUsersToClass" selected="selected">Add users to class</option>
						<option value="removeUsersFromClass">Remove users from class</option>
					</select></br></br>
					<span>Class:</span>
					<select name="chosenClass" id="selectClass">
						<?php 
							$classes = getClasses($ovaldb);
							while($class = $classes->fetch_assoc()): ?>
							<option value="<?php echo $class["id"]; ?>"><?php echo $class["name"]; ?></option>
						<?php endwhile; ?>					
					</select></br></br>
					<div id="userCheckboxesPlaceholder"></div>
				</fieldset>
			</form>
		</div>
		<div>
			<form action="covaa_admin_ajax_handler.php" id="manage_groups" method="post">
				<fieldset>
					<legend>Manage Groups</legend>
					<span>Class:</span>
					<select name="chosenClass" id="selectGroupClass">
						<option value="" selected="selected"></option>
						<?php 
							$classes = getClasses($ovaldb);
							while($class = $classes->fetch_assoc()): ?>
							<option value="<?php echo $class["id"]; ?>"><?php echo $class["name"]; ?></option>
						<?php endwhile; ?>					
					</select></br></br>
					<span>Select Function:</span>
					<select name="request" id="selectGroupFunction">
						<option value="" selected="selected"></option>
						<option value="addGroup">Add Group</option>
						<option value="removeGroup">Remove Group</option>
						<option value="addOwnersToGroup">Add Owners to Group</option>
						<option value="removeOwnersFromGroup">Remove Owners from Group</option>
						<option value="addUsersToGroup">Add Members to Group</option>
						<option value="removeUsersFromGroup">Remove Members from Group</option>						
					</select></br></br>
					<div id="groupsPlaceholder"></div></br>
					<div id="memberCheckboxesPlaceholder"></div>
				</fieldset>
			</form>
		</div>
		<div>
			<form action="covaa_admin_ajax_handler.php" id="manage_instructors" method="post">
				<fieldset>
					<legend>Manage Instructors</legend>
					<span>Select Function:</span>
					<select name="request" id="selectInstructorFunction">
						<option value="addInstructorsToClass" selected="selected">Add instructors to class</option>
						<option value="removeInstructorsFromClass">Remove instructors from class</option>
					</select></br></br>
					<span>Class:</span>
					<select name="chosenClass" id="selectInstructorClass">
						<?php
							$classes = getClasses($ovaldb);
							while($class = $classes->fetch_assoc()): ?>
							<option value="<?php echo $class["id"]; ?>"><?php echo $class["name"]; ?></option>
						<?php endwhile; ?>					
					</select></br></br>
					<div id="instructorCheckboxesPlaceholder"></div>
				</fieldset>
			</form>
		</div>
		<div>
			<form action="covaa_admin_ajax_handler.php" id="manage_classes" method="post">
				<fieldset>
					<legend>Manage Class</legend>
					<span>Select Function:</span>
					<select name="request" id="selectClassFunction">
						<option value="addClass" selected="selected">Add class</option>
						<option value="removeClass">Remove class</option>
					</select></br></br>
					<div id="classPlaceholder">
						<span>Class Name:</span>
						<input type="text" name="className"/>
						<input type="submit" value="Submit"/>
					</div>
				</fieldset>
			</form>
		</div>
		<div id="response_div"></div>
	</body>
</html>

<?php

//====================functions to retrieve data=================================

//Sync all users from Wordpress DB to CLAS DB
function syncUsers()
{
	global $ovaldb, $wpdb;
	
	$query = "SELECT wp.ID, wp.display_name, wp.user_email, wpum.meta_value
			  FROM
				covaa21_covaawp.wp_users wp
			  JOIN
				covaa21_covaawp.wp_usermeta wpum
			  ON wp.ID = wpum.user_id
				AND wpum.meta_key = 'wp_capabilities'
			  WHERE
				wp.ID NOT IN(SELECT ID FROM covaa21_CLAS.users)";
			  /* AND
				wp.ID != 1"; */
				
	$users = mysqli_query($wpdb, $query);
	
	$rows = mysqli_num_rows($users);
	
	if($rows > 0)
	{
		$query = "INSERT INTO covaa21_CLAS.users ( id, hash_user_id, salt, first_name, role, mail) VALUES ";
		
		for($i=0; $i<$rows; $i++)
		{
			$user = $users->fetch_assoc();
			
			if(strpos($user["meta_value"], 'administrator') === true)
				$role = 5;
			elseif(strpos($user["meta_value"], 'editor') === true)
				$role = 2;
			elseif(strpos($user["meta_value"], 'author') === true)
				$role = 3;
			elseif(strpos($user["meta_value"], 'contributor') === true)
				$role = 4;
			else
				$role = 1;
			
			$query .= "(" . $user["ID"] . "," . $user["ID"] . "," . $user["ID"] . ",'" . $user["display_name"] . "'," . $role . ",'" . $user["user_email"] . "')";
						
			if($i+1<$rows)
				$query .= ",";
		}
		
		echo $query;
		
		$result = mysqli_query($ovaldb, $query);
	}
	if($result)
	{
		echo "New Users Synced to COVAA.";
	}
	else
	{
		echo "Info: No new users to sync.";
	}
}

//getClasses will retrieve all the existing classes in the OVAL database
function getClasses($dbConn)
{	
    //list class that have already been created
    $query  = "SELECT * FROM class ORDER BY name ASC";
    $result = mysqli_query($dbConn, $query);

/*     while ($row = mysql_fetch_assoc($result)) {
        $ID             = $row['id'];
        $classes[$ID]   = $row['name'];
    } */

    return $result;
}

?>

<script type="text/javascript">
$("#selectUserFunction, #selectClass").change(function(event){	
	$( "#userCheckboxesPlaceholder" ).html( "<span>Please wait while we retrieve the users..</span>" );
	
	// Send the option selected using post
	$.ajax({
	  url: "covaa_admin_ajax_handler.php",
	  data: {
				request: "checkClassUsers",
				classChosen: $("#selectClass").val(),
				checkUserType: $("#selectUserFunction").val()
			},
	  type: "POST",
	  dataType : "text",
	  success: function( data ) {
		$( "#userCheckboxesPlaceholder" ).html( data );
		
		//Select all function that allows selection of all users at once
		$("#selectall").change(function(){
		  $(".user_checkbox").prop('checked', $(this).prop("checked"));
		  });
	  },
	  error: function( xhr, status, errorThrown ) {
			alert( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		}
	});
});

$("#manage_users").submit(function(event){
	//Stop form from submitting normally
	event.preventDefault();
	
	$( "#response_div" ).html( "<span>Please wait while we process the request..</span>" );
	
	// Get values from elements on the page:
	var data = $(this).serialize(); 
	
	// Send the data using post
	$.ajax({
	  url: "covaa_admin_ajax_handler.php",
	  data,
	  type: "POST",
	  dataType : "text",
	  success: function( data ) {
		$( "#response_div" ).html( data );
	  },
	  error: function( xhr, status, errorThrown ) {
			alert( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		}
	});
});

//===========================================================================================

$("#selectGroupFunction, #selectGroupClass").change(function(event){
	
	if($("#selectGroupClass").val()==="" || $("#selectGroupFunction").val()==="")
		$( "#groupsPlaceholder" ).html( "<span>Please select valid options.</span>" );
	else
	{
		$( "#groupsPlaceholder" ).html( "<span>Please wait while we retrieve the information..</span>" );
		$( "#memberCheckboxesPlaceholder" ).html( "" );
		
		// Send the option selected using post
		$.ajax({
		  url: "covaa_admin_ajax_handler.php",
		  data: {
					request: "checkGroups",
					chosenClass: $("#selectGroupClass").val(),
					checkGroupType: $("#selectGroupFunction").val()
				},
		  type: "POST",
		  dataType : "text",
		  success: function( data ) {
			$( "#groupsPlaceholder" ).html( data );
			
			$("#selectGroup").change(function(event){
				$( "#memberCheckboxesPlaceholder" ).html( "<span>Please wait while we retrieve the information..</span>" );
					
				// Send the option selected using post
				$.ajax({
				  url: "covaa_admin_ajax_handler.php",
				  data: {
							request: "checkGroups",
							chosenGroup: $("#selectGroup").val(),
							chosenClass: $("#selectGroupClass").val(),
							checkGroupType: $("#selectGroupFunction").val()
						},
				  type: "POST",
				  dataType : "text",
				  success: function( data ) {
					$( "#memberCheckboxesPlaceholder" ).html( data );
					
					//Select all function that allows selection of all users at once
					$("#selectallGroupMembers").change(function(){
					  $(".member_checkbox").prop('checked', $(this).prop("checked"));
					  });
				  },
				  error: function( xhr, status, errorThrown ) {
						alert( "Sorry, there was a problem!" );
						console.log( "Error: " + errorThrown );
						console.log( "Status: " + status );
						console.dir( xhr );
					}
				});
			});
		  },
		  error: function( xhr, status, errorThrown ) {
				alert( "Sorry, there was a problem!" );
				console.log( "Error: " + errorThrown );
				console.log( "Status: " + status );
				console.dir( xhr );
			}
		});
	}
});

$("#manage_groups").submit(function(event){
	//Stop form from submitting normally
	event.preventDefault();
	
	$( "#response_div" ).html( "<span>Please wait while we process the request..</span>" );
	
	// Get values from elements on the page:
	var data = $(this).serialize(); 
	
	// Send the data using post
	$.ajax({
	  url: "covaa_admin_ajax_handler.php",
	  data,
	  type: "POST",
	  dataType : "text",
	  success: function( data ) {
		$( "#response_div" ).html( data );
	  },
	  error: function( xhr, status, errorThrown ) {
			alert( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		}
	});
});

//===========================================================================================

$("#selectInstructorFunction, #selectInstructorClass").change(function(event){
	$( "#instructorCheckboxesPlaceholder" ).html( "<span>Please wait while we retrieve the users..</span>" );
	
	// Send the option selected using post
	$.ajax({
	  url: "covaa_admin_ajax_handler.php",
	  data: {
				request: "checkClassInstructors",
				classChosen: $("#selectInstructorClass").val(),
				checkInstructorType: $("#selectInstructorFunction").val()
			},
	  type: "POST",
	  dataType : "text",
	  success: function( data ) {
		$( "#instructorCheckboxesPlaceholder" ).html( data );
		
		//Select all function that allows selection of all users at once
		$("#selectallInstructor").change(function(){
		  $(".instructor_checkbox").prop('checked', $(this).prop("checked"));
		  });
	  },
	  error: function( xhr, status, errorThrown ) {
			alert( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		}
	});
});

$("#manage_instructors").submit(function(event){
	//Stop form from submitting normally
	event.preventDefault();
	
	$( "#response_div" ).html( "<span>Please wait while we process the request..</span>" );
	
	// Get values from elements on the page:
	var data = $(this).serialize(); 
	
	// Send the data using post
	$.ajax({
	  url: "covaa_admin_ajax_handler.php",
	  data,
	  type: "POST",
	  dataType : "text",
	  success: function( data ) {
		$( "#response_div" ).html( data );
	  },
	  error: function( xhr, status, errorThrown ) {
			alert( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		}
	});
});

$("#selectClassFunction").change(function(event){
	$( "#classPlaceholder" ).html( "<span>Please wait..</span>" );
	
	// Send the option selected using post
	$.ajax({
	  url: "covaa_admin_ajax_handler.php",
	  data: {
				request: "checkClass",
				checkClassType: $("#selectClassFunction").val()
			},
	  type: "POST",
	  dataType : "text",
	  success: function( data ) {
		$( "#classPlaceholder" ).html( data );
	  },
	  error: function( xhr, status, errorThrown ) {
			alert( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		}
	});
});

$("#manage_classes").submit(function(event){
	//Stop form from submitting normally
	event.preventDefault();
	
	$( "#response_div" ).html( "<span>Please wait while we process the request..</span>" );
	
	// Get values from elements on the page:
	var data = $(this).serialize(); 
	
	// Send the data using post
	$.ajax({
	  url: "covaa_admin_ajax_handler.php",
	  data,
	  type: "POST",
	  dataType : "text",
	  success: function( data ) {
		$( "#response_div" ).html( data );
	  },
	  error: function( xhr, status, errorThrown ) {
			alert( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		}
	});
});

</script>