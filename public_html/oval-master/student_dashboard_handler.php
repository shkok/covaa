<?php
	require_once(dirname(__FILE__) . "/includes/global_deploy_config.php");

	require_once(dirname(__FILE__) . '/includes/kaltura/kaltura_functions.php');

	require_once(dirname(__FILE__) . "/includes/common.inc.php");

	require_once(dirname(__FILE__) . "/includes/auth.inc.php");

	require_once(dirname(__FILE__) . '/database/users.php');

	require_once(dirname(__FILE__) . '/database/media.php');
	
	//require_once(dirname(__FILE__) . '/ga.php');

	error_reporting(1); //Simon added for debugging. Remove on final version.
	
	startSession();
	
	//Check if user is Student, check subjects taught and classes associated
	$isAdmin  = isAdmin($_SESSION['role']);
	$userName = $_SESSION['name'];
	$userID   = $_SESSION['user_id'];
	
		$conn = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database);

		if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
        //$db_selected = mysql_select_db($database, $conn);

        mysqli_set_charset("utf8",$conn); 

		//Create overall Array to store all subsequently retrieved data
		$overall_data = array();

		//Retrieve all subjects current students/user is associated with
		$query = "SELECT c.id as Subject_ID, c.name as Subject
					FROM classEnrollmentLists A
					JOIN class c
						ON A.class_id = c.id
					WHERE A.user_id = '$userID'";
		
		$result = mysqli_query($conn, $query);

		while ($row = mysqli_fetch_assoc($result)) {
			//var_dump($row);
			$subject_id = $row["Subject_ID"];
			$subject_name = $row["Subject"];
			$overall_data[$subject_id] = array( 
											"id"=>$subject_id, 
											"name"=>$subject_name, 
											"classes"=>array()
										);
		}

		//print_r($overall_data);

		$subjects_string = "";
		foreach($overall_data as $subject)
		{
			if($subjects_string != "")
				$subjects_string .= ", ";
			$subjects_string .= $subject["id"];
		}
		//echo "<br><br>".$subjects_string."<br><br>";
		
		//Retrieve all Classes associated with current student/user based on each subject
		$query = "SELECT g.id as Class_ID, g.name as Class, g.class_id as Subject_ID
					FROM groups g
					JOIN groupMembers gm
						ON gm.group_id = g.id
						AND gm.user_id = '$userID'
					JOIN class c
						ON g.class_id = c.id
						AND c.id IN ($subjects_string)";
		
		$result = mysqli_query($conn, $query);
		
		while ($row = mysqli_fetch_assoc($result)) {
			//var_dump($row);
			$class_id = $row["Class_ID"];
			$class_name = $row["Class"];
			$subject_id = $row["Subject_ID"];
			//echo "Class ID: " . $class_id , " Class Name: " . $class_name . " Subject ID: " . $subject_id . "<br>";
			$overall_data[$subject_id]["classes"][$class_id] = array( 
																	"id"=>$class_id, 
																	"name"=>$class_name, 
																	"users"=>array(), 
																	"videos"=>array(),
																	"social_links"=>array()
																);
		}
		
		//To populate all classes with student objects
		foreach($overall_data as $subject)//Subject
		{
			$subject_id = $subject["id"];
			//echo "Subject: " . $subject_id . "<br>";
			foreach($subject["classes"] as $class)//Class
			{
				$class_id = $class["id"];
				//echo "Class: " . $class_id . "<br>";
				
				//Retrieve annotation questions count for each video from current class
				$query = "SELECT *
							FROM annotationQns aq
							WHERE groupId = $class_id";
							
				$result = mysqli_query($conn, $query);

				$annotationQns = array(); //Array to store annotation questions count for each video
				
				while ($annotationQnsEntry = mysqli_fetch_assoc($result))
				{
					array_push($annotationQns, $annotationQnsEntry); //Add each row of data to the array
				}
				
				//Retrieve all videos from current class
				$query = "SELECT m.video_id, m.title
							FROM media m
							JOIN videoGroup vg
							ON m.video_id = vg.video_id
							AND vg.group_id = $class_id";
				
				$result = mysqli_query($conn, $query);
				
				while ($video = mysqli_fetch_assoc($result))
				{
					$video_id = $video["video_id"];
					$video_title = $video["title"];
					//echo "Video: " . $video_id . "<br>";
					
					$count_annotation_qns = 0;
					foreach($annotationQns as $annotationQn)//For each annotation question count, look for the one for the current loops video.
					{
						if($annotationQn["video_id"] == $video_id)//If count the annotation question count for current video
							$count_annotation_qns = $annotationQn["qnsNo"];//record the counter into variable, for use next to create video data array
					}
					
					$overall_data[$subject_id]["classes"][$class_id]["videos"][$video_id] = array(
																						"id"=>$video_id,
																						"name"=>$video_title,
																						"qnsNo"=>$count_annotation_qns,
																						"social_links" => array()
																					);
				}//End of videos loop
				
				//Retrieve all login data based on current class
				/*$query = "SELECT DISTINCT ls.user_id
							FROM loginSessions ls
							JOIN users u
								ON u.id = ls.user_id
							JOIN groupMembers m
								ON m.user_id = u.id
							JOIN groups g
								ON g.id = m.group_id
								AND g.id = $class_id";	
				
				$result = mysqli_query($conn, $query);
				
				$logins = array();
				
				while($row = mysqli_fetch_assoc($result)) {
					
					array_push($logins, $row["user_id"]);
				}*/
				


				//Fetch all data from pData table based on current class
				$query = "SELECT pD.userId, pD.action, pD.label
							FROM pData pD
							JOIN users u
								ON u.id = pD.userId
								AND u.role = 1
							JOIN groupMembers m
								ON m.user_id = u.id
							JOIN groups g
								ON g.id = m.group_id
								AND g.id = $class_id
							WHERE pD.action LIKE '%Watch To End%' OR pD.action LIKE '%\%%'
							ORDER BY 
								pD.userId, pD.label, pD.action";
								
				$result = mysqli_query($conn, $query);
				
				$pData = array();
				
				while($row = mysqli_fetch_assoc($result)) {
					array_push($pData, $row);
				}


				
				//Fetch all annotations/comments/replies from current class that is not deleted
				$query = "SELECT *
							FROM annotations a
							JOIN groups g
							ON g.id = a.groupId
							AND g.id = $class_id
							WHERE a.is_deleted = 0
							AND a.video_id IN (SELECT m.video_id FROM media m
							JOIN videoGroup vg
							ON m.video_id = vg.video_id
							AND vg.group_id = $class_id)";

				$result = mysqli_query($conn, $query);
				
				$submissions = array();
				
				while ($submissionEntry = mysqli_fetch_assoc($result))
				{
					array_push($submissions, $submissionEntry);
				}
				
				//Grab all users from current class
				$query = "SELECT u.id as User_ID, u.first_name as User_Name, u.role as Role
							FROM users u
							JOIN groupMembers m
								ON m.user_id = u.id
							JOIN groups g
								ON g.id = m.group_id
							WHERE g.id = $class_id";
				
				$result = mysqli_query($conn, $query);
				
				//Setup array of users
				while ($user = mysqli_fetch_assoc($result))//Looping through all students to populate dataset.
				{
					$user_id = $user["User_ID"];
					$user_name = $user["User_Name"];
					$role = $user["Role"];
					//Create a personal array to store current looped user's pData for Viewed Videos Status
					$user_overall_view_video_array = array();

					foreach($pData as $video_view_status)
					{
						if($video_view_status["userId"] == $user_id)
						{
							if (($pos = strpos($video_view_status["label"], "=")) !== false)
							{ 
							    $video_view_status["label"] = substr($video_view_status["label"], $pos+1); 
							}
							
							$user_overall_view_video_array[] = $video_view_status;
						}
					}

					//&$users_array = $overall_data[$subject_id]["classes"][$class_id]["users"];

					//Create the student data array used for Overall Class level
					$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id] = array(
																		"id"=>$user_id,
																		"name"=>$user_name,
																		"role"=>$role,
																		"annotations"=>0, 
																		//"completeAnnotations"=> array(),
																		//"completeAnnotationsStatus" => array(), 
																		"comments"=>0,
																		"reply"=>0,
																		"ideate" =>0,
																		"justify" =>0, 
																		"agree" => 0, 
																		"disagree" => 0,
																		"inquire" => 0,
																		"message" =>0, 
																		"purpose" =>0,
																		"audience" => 0, 
																		"viewpoint" => 0, 
																		"assumption" => 0,
																		"inference" => 0,
																		"impact" => 0,
																		"evidence" => 0,
																		"submissions"=> array(),
																		"weight" => 0,
																		"faveShape" => "rectangle",
																		"nameColor" => "black",
																		"video_view_status" => $user_overall_view_video_array
																	);

					//Create the student data array used for Video level
					foreach($overall_data[$subject_id]["classes"][$class_id]["videos"] as $video_array)
					{
						$video_id = $video_array["id"];

						$user_single_view_video_array = array();
						foreach($user_overall_view_video_array as $video_view_status)
						{
							if($video_view_status["label"] == $video_id)
							{
								$user_single_view_video_array[] = $video_view_status;
							}
						}

						$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id] = array(
																		"id"=>$user_id,
																		"name"=>$user_name,
																		"role"=>$role,
																		"annotations"=>0, 
																		//"completeAnnotations"=> array(),
																		//"completeAnnotationsStatus" => array(),  
																		"comments"=>0,
																		"reply"=>0,
																		"ideate" =>0,
																		"justify" =>0, 
																		"agree" => 0, 
																		"disagree" => 0,
																		"inquire" => 0,
																		"message" =>0, 
																		"purpose" =>0,
																		"audience" => 0, 
																		"viewpoint" => 0, 
																		"assumption" => 0,
																		"inference" => 0,
																		"impact" => 0,
																		"evidence" => 0,
																		"submissions"=> array(),
																		"weight" => 0,
																		"faveShape" => "rectangle",
																		"nameColor" => "black",
																		"view_video_status" => $user_single_view_video_array
																	);
					}
					
					if($user_id == $userID)//If current looped user is current logged in user, change SNA nameColor to unique color
					{
						$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["nameColor"] = "#37c4a1";
						$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["nameColor"] = "#37c4a1";
					}

					/*if(in_array($student_id, $logins))
						$overall_data[$subject_id]["classes"][$class_id]["students"][$student_id]["login"] = 1;
					
					foreach($pData as $pDataEntry)
					{
						if($pDataEntry["userId"] == $student_id)
						{
							$video_id = $pDataEntry["label"];
							$pos = strpos($video_id, "?v=");
							$video_id = substr($video_id, $pos+3);
							//if($pDataEntry["action"] == "100%")
								$overall_data[$subject_id]["classes"][$class_id]["students"][$student_id]["viewedVideos"][$video_id] = $video_id;
						}
					}
					*/
					foreach($submissions as $submissionEntry)//$submissions is the class level. Loop through all submissions
					{
						if($submissionEntry["user_id"] == $user_id)//If current submission belongs to current user
						{
							array_push($overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["submissions"], $submissionEntry);//Add submission entry to overall level submissions list.

							$video_id = $submissionEntry["video_id"];
							array_push($overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["submissions"], $submissionEntry);//Add submission entry to video level submissions list.

							if($submissionEntry["start_time"] == null)//If its NOT an annotation. AKA (Comment or Reply)
							{
								//Add 1 counter for each comment or reply to the weight of student. Used for SNA
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["weight"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["weight"]++;

								//Set the user SNA shape as ellipse because got at least 1 submission
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["faveShape"] = "ellipse";
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["faveShape"] = "ellipse";

								
								if($submissionEntry["parent_id"] == null)//If its a parent comment
								{
									//Add 1 counter for comment made
									$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["comments"]++;
									$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["comments"]++;
								}
								else//theres parent_id, so it is a reply.
								{
									//Special conditon that if this user has at least 1 reply, but 0 parent comments, they will have a different shape.
									if($overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["comments"] == 0)
										$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["faveShape"] = "rhomboid";

									if($overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["comments"] == 0)
										$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["faveShape"] = "rhomboid";


									//Add 1 counter for reply made
									$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["reply"]++;
									$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["reply"]++;
									
									foreach($submissions as $parent_comment)//Find the parent comment for this reply
									{
										if($parent_comment["annotation_id"] == $submissionEntry["parent_id"])//If parent comment found
										{
											//Populating VIDEO level Social links
											$found = false;

											//Loop through current video social links to see if existing link exist for these 2 users
											foreach($overall_data[$subject_id]["classes"][$class_id]["videos"][$video_id]["social_links"] as &$video_sl)
											{
												if($video_sl["target"] == $parent_comment["user_id"] && $video_sl["source"] == $submissionEntry["user_id"])//If found social link of the same direction, add one to strength.
												{
													$video_sl["strength"] += 1;
													$found = true;
													break;
												}
												else if($video_sl["target"] == $submissionEntry["user_id"] && $video_sl["source"] == $parent_comment["user_id"])//If found social link of opposite direction, means there is 2 way connection, so update both strength and direction.
												{
													$video_sl["strength"] += 1;
													$video_sl["direction"] = 2;
													$found = true;
													break;
												}
											}

											if(!$found)//If SLA doesnt exist yet for this target user, create the SLA
											{
												array_push($overall_data[$subject_id]["classes"][$class_id]["videos"][$video_id]["social_links"], array(
																	"source" => $submissionEntry["user_id"],
																	"target" => $parent_comment["user_id"],
																	"strength" => 1,
																	"direction" => "1",
																	"faveColor" => "#848484"
																));
											}
											//End of Populating VIDEO level Social links

											//Populating CLASS (Overall) level Social links
											$found = false;

											//Loop through class overall social links to see if existing link exist for these 2 users
											foreach($overall_data[$subject_id]["classes"][$class_id]["social_links"] as &$class_sl)
											{
												if($class_sl["target"] == $parent_comment["user_id"] && $class_sl["source"] == $submissionEntry["user_id"])//If found social link of the same direction, add one to strength.
												{
													$class_sl["strength"] += 1;
													$found = true;
													break;
												}
												else if($class_sl["target"] == $submissionEntry["user_id"] && $class_sl["source"] == $parent_comment["user_id"])//If found social link of opposite direction, means there is 2 way connection, so update both strength and direction.
												{
													$class_sl["strength"] += 1;
													$class_sl["direction"] = 2;
													$found = true;
													break;
												}
											}

											if(!$found)//If SLA doesnt exist yet for this target user, create the SLA
											{
												array_push($overall_data[$subject_id]["classes"][$class_id]["social_links"], array(
																	"source" => $submissionEntry["user_id"],
																	"target" => $parent_comment["user_id"],
																	"strength" => 1,
																	"direction" => "1",
																	"faveColor" => "#848484"
																));
											}
											//End of Populating CLASS level Social links
										}
									}
								}
							}
							else //this submission is an Annotation
							{
								//Add 1 counter for annotation made
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["annotations"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["annotations"]++;

								/*if($user_id != $userID)//If current looped user is not current logged in user, change SNA nameColor to unique color for annotated indication
								{
									$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["nameColor"] = "#ffa100";
									$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["nameColor"] = "#ffa100";
								}*/
							}

							/*//check critical lens used and add counters for each lens used
							if($submissionEntry["tags"] == "Message/Issue"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["message"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["message"]++;
							}
							else if($submissionEntry["tags"] == "Purpose"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["purpose"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["purpose"]++;
							}
							else if($submissionEntry["tags"] == "Audience"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["audience"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["audience"]++;
							}
							else if($submissionEntry["tags"] == "Viewpoint"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["viewpoint"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["viewpoint"]++;
							}
							else if($submissionEntry["tags"] == "Assumption"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["assumption"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["assumption"]++;
							}
							else if($submissionEntry["tags"] == "Inference/Interpretation"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["inference"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["inference"]++;
							}
							else if($submissionEntry["tags"] == "Consequences/Impact"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["impact"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["impact"]++;
							}
							else if($submissionEntry["tags"] == "Evidence"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["evidence"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["evidence"]++;
							}
							
							//check thinking skills used and add counters for each skill used
							if($submissionEntry["commentType"] == "Ideate/Reflect"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["ideate"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["ideate"]++;
							}
							else if($submissionEntry["commentType"] == "Justify/Explain"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["justify"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["justify"]++;
							}
							else if($submissionEntry["commentType"] == "Agree/Validate"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["agree"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["agree"]++;
							}
							else if($submissionEntry["commentType"] == "Disagree/Challenge"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["disagree"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["disagree"]++;
							}
							else if($submissionEntry["commentType"] == "Inquire/Clarify"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["inquire"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["inquire"]++;
							}*/

							//check thinking skills used and add counters for each skill used
							if($submissionEntry["tags"] == "Ideate"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["ideate"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["ideate"]++;
							}
							else if($submissionEntry["tags"] == "Justify"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["justify"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["justify"]++;
							}
							else if($submissionEntry["tags"] == "Agree"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["agree"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["agree"]++;
							}
							else if($submissionEntry["tags"] == "Disagree"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["disagree"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["disagree"]++;
							}
							else if($submissionEntry["tags"] == "Inquire"){
								$overall_data[$subject_id]["classes"][$class_id]["users"]["overall"][$user_id]["inquire"]++;
								$overall_data[$subject_id]["classes"][$class_id]["users"][$video_id][$user_id]["inquire"]++;
							}
						}
					}//End of submissions loop
				}//End of student loop

				//Populating VIDEO level Top/Bottom Weights and Strength. 
				//Weights and Replies are used for SNA visualization, to have a balanced proportions (size(weight) and color gradient(reply) of each node) in the drawings.
				foreach($overall_data[$subject_id]["classes"][$class_id]["videos"] as $video_array)//for each video in the class
				{
					//Setup weight and replies variables
					$bottom_weight = 0;
					$top_weight = 0;
					$bottom_reply = 0;
					$top_reply = 0;

					$video_id = $video_array["id"]; //Track video ID

					//Loop through all users for this video, track the highest and lowest weight and reply
					foreach($overall_data[$subject_id]["classes"][$class_id]["users"][$video_id] as $user)
					{
						if($user["weight"] <= $bottom_weight)
							$bottom_weight = $user["weight"];
						
						if($user["weight"] >= $top_weight)
							$top_weight = $user["weight"];
						
						if($user["reply"] <= $bottom_reply)
							$bottom_reply = $user["reply"];
						
						if($user["reply"] >= $top_reply)
							$top_reply = $user["reply"];	
					}

					//Once highest and lowest weight and replies are identified, set the values in the data array for this video.
					$overall_data[$subject_id]["classes"][$class_id]["videos"][$video_id]["weights"] = array("bottom_weight"=>$bottom_weight, "top_weight"=>$top_weight, "bottom_reply"=>$bottom_reply, "top_reply"=>$top_reply);
		

					//Strengths are used for SNA visualization, to have a balanced proportion (thickness of connection lines) between each node in the drawings.
					$bottom_strength = 0;
					$top_strength = 0;

					//Loop through all social links for this video, track highest and lowest strengths
					foreach($overall_data[$subject_id]["classes"][$class_id]["videos"][$video_id]["social_links"] as $social_link)
					{
						if($social_link["strength"] <= $bottom_strength)
							$bottom_strength = $social_link["strength"];
						
						if($social_link["strength"] >= $top_strength)
							$top_strength = $social_link["strength"];
					}

					//Once highest and lowest strength are identified, set the values in the data array for this video.
					$overall_data[$subject_id]["classes"][$class_id]["videos"][$video_id]["strengths"] = array("bottom_strength"=>$bottom_strength, "top_strength"=>$top_strength);
				}
				//End of Populating VIDEO level Top/Bottom Weights and Strength

				//Same as above, but for Class level.
				//Populating CLASS level Top/Bottom Weights and Strength

				//Weights and Replies are used for SNA visualization, to have a balanced proportions (size(weight) and color gradient(reply) of each node) in the drawings.

				//Setup weight and replies variables
				$class_bottom_weight = 0;
				$class_top_weight = 0;
				$class_bottom_reply = 0;
				$class_top_reply = 0;

				//Loop through all users for this class, track the highest and lowest weight and reply
				foreach($overall_data[$subject_id]["classes"][$class_id]["users"]["overall"] as $user)
				{
					if($user["weight"] <= $class_bottom_weight)
						$class_bottom_weight = $user["weight"];
					
					if($user["weight"] >= $class_top_weight)
						$class_top_weight = $user["weight"];
					
					if($user["reply"] <= $class_bottom_reply)
						$class_bottom_reply = $user["reply"];
					
					if($user["reply"] >= $class_top_reply)
						$class_top_reply = $user["reply"];
				}

				//Once highest and lowest weight and replies are identified, set the values in the data array for this class.
				$overall_data[$subject_id]["classes"][$class_id]["weights"] = array("bottom_weight"=>$class_bottom_weight, "top_weight"=>$class_top_weight, "bottom_reply"=>$class_bottom_reply, "top_reply"=>$class_top_reply);

				//Strengths are used for SNA visualization, to have a balanced proportion (thickness of connection lines) between each node in the drawings.
				$class_bottom_strength = 0;
				$class_top_strength = 0;

				//Loop through all social links for this class, track highest and lowest strengths
				foreach($overall_data[$subject_id]["classes"][$class_id]["social_links"] as $class_social_link)
				{
					if($class_social_link["strength"] <= $class_bottom_strength)
						$class_bottom_strength = $class_social_link["strength"];
					
					if($class_social_link["strength"] >= $class_top_strength)
						$class_top_strength = $class_social_link["strength"];
				}

				//Once highest and lowest strength are identified, set the values in the data array for this class.
				$overall_data[$subject_id]["classes"][$class_id]["strengths"] = array("bottom_strength"=>$class_bottom_strength, "top_strength"=>$class_top_strength);
				//End of Populating CLASS level Top/Bottom Weights and Strength

			}//End of class loop
		}//End of subject loop
		
		
		/* print "<pre>";
		print_r($overall_data);
		print "</pre>"; */

		mysqli_free_result($result);
		$key = array();
		
		function string2KeyedArray($string, $delimiter = '&', $kv = '=') {
		  if ($a = explode($delimiter, $string)) { // create parts
			foreach ($a as $s) { // each part
			  if ($s) {
				if ($pos = strpos($s, $kv)) { // key/value delimiter
				  $k = trim(substr($s, 0, $pos));
				  $ka[trim(substr($s, 0, $pos))] = trim(substr($s, $pos + strlen($kv)));
				  array_push($key,$k);
				  print_r($key);
				} else { // key delimiter not found
				  $ka[] = trim($s);
				}
			  }
			}
			return $ka;
		  }
		} 
		
		
		//Grab user's pre survey result
		$query = "SELECT *
					FROM surveyData s
					WHERE s.user_login = $userID
					AND  s.isCompleted =1
					AND s.SurveyType = 'Pre'";
		
		$result1 = mysqli_query($conn, $query);
		while ($row = mysqli_fetch_assoc($result1))
		{
			$cal = string2KeyedArray($row["calResult"]);	
			
		}
		//Grab user's pre survey result
		$query2 = "SELECT *
					FROM surveyData s
					WHERE s.user_login = $userID
					AND  s.isCompleted =1
					AND s.SurveyType = 'Post'";
		
		$result3 = mysqli_query($conn, $query2);
		while ($row = mysqli_fetch_assoc($result3))
		{
			$postcal = string2KeyedArray($row["calResult"]);	
			
		}
		//print_r($cal);	
		$allsurvey = array();
		
		$query1 = "SELECT *
					FROM surveyData s
					WHERE s.isCompleted =1
					AND s.SurveyType='Pre'";		
		
		$result2 = mysqli_query($conn, $query1);
		while ($row = mysqli_fetch_assoc($result2))
		{
			$mykey = array();
			$userid = $row["user_login"];
			$trial = $row["TrialNo"];
			$type = $row["SurveyType"];
			$username = $row["StudentName"];
			$class = $row["StudentClass"];
			$cal1 = string2KeyedArray($row["calResult"]);	
			
			
			
			$allcal = array(
						"id"=>$userid,
						"name"=>$username,
						"trial"=>$trial,
						"type"=>$type,
						"class"=>$class,
						"cal" => $cal1);
			
			array_push($allsurvey,$allcal);
		}
		
		$allpostsurvey = array();
		$query3 = "SELECT *
					FROM surveyData s
					WHERE s.isCompleted =1
					AND s.SurveyType='Post'";		
		
		$result4 = mysqli_query($conn, $query3);
		while ($row = mysqli_fetch_assoc($result4))
		{
			$userid = $row["user_login"];
			$trial = $row["TrialNo"];
			$type = $row["SurveyType"];
			$username = $row["StudentName"];
			$class = $row["StudentClass"];
			$cal1 = string2KeyedArray($row["calResult"]);	

			$allpostcal = array(
						"id"=>$userid,
						"name"=>$username,
						"trial"=>$trial,
						"type"=>$type,
						"class"=>$class,
						"cal" => $cal1);
			
			array_push($allpostsurvey,$allpostcal);
		}
		
		mysqli_close($conn);
		$overall_data = json_encode($overall_data);
		$cal = json_encode($cal);
		$allsurvey = json_encode($allsurvey);
		$postcal = json_encode($postcal);
		$allpostsurvey = json_encode($allpostsurvey);
		$data = '<script type="text/javascript">
		
					var overall_data = '.$overall_data.';
					var cal = '.$cal.';
					var postcal = '.$postcal.';
					var allsurvey = '.$allsurvey.';
					var allpostsurvey = '.$allpostsurvey.';
					console.log(overall_data);
					console.log(postcal);
					console.log(allpostsurvey);
				';
		
		$data .= "</script>";
		
		echo $data; 
		
		
?>