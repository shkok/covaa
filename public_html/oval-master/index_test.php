<?php
/**
 *  OVAL (Online Video Annotation for Learning) is a video annotation tool
 *  that allows users to make annotations on videos.
 *
 *  Copyright (C) 2014  Shane Dawson, University of South Australia, Australia
 *  Copyright (C) 2014  An Zhao, University of South Australia, Australia
 *  Copyright (C) 2014  Dragan Gasevic, University of Edinburgh, United Kingdom
 *  Copyright (C) 2014  Negin Mirriahi, University of New South Wales, Australia
 *  Copyright (C) 2014  Abelardo Pardo, University of Sydney, Australia
 *  Copyright (C) 2014  Alan Kingstone, University of British Columbia, Canada
 *  Copyright (C) 2014  Thomas Dang, , University of British Columbia, Canada
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
// John Bratlien and Thomas Dang, 2011-2012
// An ZHAO started to modify this page since Nov of 2013.
// Simon Yang, 2016
require_once(dirname(__FILE__) . "/includes/global_deploy_config.php");
require_once(dirname(__FILE__) . "/includes/common.inc.php");
require_once(dirname(__FILE__) . "/includes/auth.inc.php");
require_once(dirname(__FILE__) . "/database/media.php");
require_once(dirname(__FILE__) . "/database/users.php");
require_once(dirname(__FILE__) . '/includes/kaltura/kaltura_functions.php');
//require_once(dirname(__FILE__) . "/MyVideo.php");
error_reporting(0); //Simon added
//error_reporting(E_ALL ^ E_WARNING); //Simon added
$isiPad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
startSession();
//timeoutSession();
$userName = $_SESSION['name'];
$userID   = $_SESSION['user_id'];
$isAdmin  = isAdmin($_SESSION['role']);
if (compatibilityModeIE9()) {
?>
	<html>
	<head>
	</head>
	<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQLCKQP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
/*function debug_to_console( $data ) {
    $output = $data;
    if ( is_array( $output ) )
        $output = implode( ',', $output);
    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}
$key = file_get_contents($key_file_location);
$cred = new Google_Auth_AssertionCredentials(
    $service_account_name,
    array('https://www.googleapis.com/auth/analytics.readonly'),
    $key
);
$client->setAssertionCredentials($cred);
$ids = 'ga:1011490414.1489547188'; //your id
$startDate = '2017-03-01';
$endDate = '2017-03-30';
$metrics = 'ga:transactions';
$optParams = array(
    'dimensions' => 'ga:campaign',
    'max-results' => '50'
);
$results = $service->data_ga->get($ids, $startDate, $endDate, $metrics, $optParams);
var_dump($results);
debug_to_console('test');*/
//Dump results
/*echo "<h3>Results Of Call:</h3>";
echo "dump of results";
var_dump($results);
echo "results['totalsForAllResults']";
var_dump($results['totalsForAllResults']);
echo "results['rows']";
foreach ($results['rows'] as $item) {
    var_dump($item);
}*/
?>
	You are viewing this page in compatibility view. 
	Compatibility view causes layout problem, please disable it by 
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie-9/features/compatibility-view">
		clicking the compatibility view button.
	</a>
	</body>
	</html>
<?php
    exit();
}
?>
<?php
if (! browserSupported()) {
    $browserData = getBrowserData();
    $browser    = $browserData["browser"];
    $version    = $browserData["version"];
?>
<html>
<h1>Browser version not supported</h1>
    <?php
    if ("IE" == $browser) {
        print "To use OVAL you must upgrade to IE 9.";    
    } else {
        print "<p>To use OVAL you must upgrade to the lastest version of <?php$browser?>.</p>";
        print "current version:$version<br />";
    }
 	?>
</html>
<?php
    
    exit();
}
?>
<?php
$media  = new media();
$users      = new users();
$uiConfig   = $users->getUI($userID);
$classes    = $users->getClassesUserBelongsTo($userID);
foreach ($classes as $key=>$row) {
	$id[$key] = $row['ID'];
	$name[$key] = $row['name'];
}
array_multisort($name, SORT_ASC, $id, SORT_DESC, $classes);
if (isset($_GET['cid'])) {
    $CID = $_GET['cid'];
} else {
    $CID = $classes[0]['ID'];
}
$groups     = $users->getMyGroups($userID, $CID);
$test = $media->getVideosByClassID($CID);
$globalVID = $test[0][video_id];
// Show Unassigned Video - Disabled for now, unwieldy interface in practice
// Has video previewing available in the video management page instead
if ($isAdmin) {
    if (0 != count($media->getVideosWithNoGroup($userID))) {
		 $groups['U'] = "-- UNASSIGNED VIDEOS";
    }
}
$users->close();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Online Video Annotation for Learning</title>
	
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta content="utf-8" http-equiv="encoding">
	<!-- Debug version: If using the debug version you can remove all the below js / css references --> 
  <!--  <script type="text/javascript" src="../mwEmbed.js" ></script>  -->
<script>
  dataLayer = [{'userID': <?php echo $userID; ?> }];
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQLCKQP');</script>
<!-- End Google Tag Manager -->
  <script>var yesChecked = false;</script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="youtube-google-analytics-master/lunametrics-youtube.gtm.min.js"></script>
 <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!--Load the AJAX API-->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
			 	
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style_test.css" />
	
	<!--  Include jQuery, use a local jQuery to deal with Chrome's conservative same domain policy --> 	
 	<script type="text/javascript" src="kaltura-html5player-widget/jquery-1.4.2.min.js"></script>
 	
 	<!--  Include the local Open Source Kaltura player -->
 <!--	<script type="text/javascript" src="includes/kaltura/mwEmbedLoader.php"></script>   -->
	<style type="text/css">
    	@import url("kaltura-html5player-widget/skins/jquery.ui.themes/kaltura-dark/jquery-ui-1.7.2.css");
	</style>  
	<style type="text/css">
		@import url("kaltura-html5player-widget/mwEmbed-player-static.css");
	</style>	
	
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="kaltura-html5player-widget/mwEmbed-player-static.js"></script>
	<script type="text/javascript">
		<!-- put mw.setConfig calls here -->
		<!-- mw.setConfig('EmbedPlayer.EnableRightClick', false); -->
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
<script type="text/javascript">
<?php
	// This part sets javascript variables from PHP based on user setting &/or access ctrl in DB 
    if ("yes" == $uiConfig['annotations_enabled']) {
        print "\tvar annotationsEnabled = true;\n";
    } else {
        print "\tvar annotationsEnabled = false;\n";
    }
    
    if ("PSYC" == $uiConfig['annotation_mode']) {
        print "\tvar psycMode = true;\n";
    } else {
        print "\tvar psycMode = false;\n";
    }
    printSharedClientServerConstants();
    if (isset($_GET['gid'])) {
        $GID = $_GET['gid'];
    } else {
        $groupIDs = array_keys($groups);
        $GID = $groupIDs[0];
    }
	//print "GID:$GID<br />";
    
	    
	    if (is_numeric($GID)) {
	        $videos = $media->getVideosByGroupID($userID, $GID);
	    } else {
			$videos = $media->getVideosWithNoGroup($userID);
	    }
	    
	
    // grab the first available video if none set
    (isset($_GET['vid'])) ? $VID = $_GET['vid'] : $VID = array_shift(array_keys($videos));
    
    // strip out hash
    $VID = (string) str_replace("#", "", $VID);
    $duration = $videos[$VID]['duration'];
    $conversionStatus = $videos[$VID]['conversion_complete'];
    $thumbnailURL = $videos[$VID]['thumbnail_url']; 
    if (is_null($duration)) $duration=0;
	$flavors = $media->getFlavors($VID);
	
	/* sanity check, there are multiple ways that kaltura conversion might fail
	 * silently, so do a client side, right-before-view check as a last line of defense
	 */
/*
	if (
		empty($flavors) || 
		count($flavors) <= MINIMUM_FLAVOR_COUNT ||
		$conversionStatus != CONVERSION_COMPLETED
		) {
		$flavorsFromKMC = getFlavorsFromKMC($VID);
		
		if (!empty($flavorsFromKMC)) {
			foreach ($flavorsFromKMC as $flavor) {
				if ("" != $flavor['codec_id']) {
					$media->addFlavor($flavor['flavor_id'],
							$VID,
							$flavor['codec_id'], 
							$flavor['file_ext']);
				}
			}
			$flavors = $media->getFlavors($VID);
		}
	}
	
	// MIGRATION MODE
	if (kalturaCdnURL_INTERIM != null && kalturaCdnURL_INTERIM != "") {
		$flavorsFromKMC = getFlavorsFromKMC($VID);
	
		if (empty($flavorsFromKMC)) {
			$kalturaCdnURL = kalturaCdnURL_INTERIM;
		}
	}
*/	
	$media->close();
    print "\tvar mediaDuration = $duration;\n";
// print "\tvar mediaDuration = 678;\n";
    print "\tvar videoID       = \"$VID\";\n";
    print "\tvar userID        = \"$userID\";\n";
?>
        $(document).ready(function() {
            
			//Simon Added to pause video when annotation item clicked.
			$('.annotation-list-item').click(function() {
				player1.pauseVideo();
			});
			
            $('#choose-class').change(function() {
                location.href = getPathFromURL(window.location.href) + "?class_id=" + $(this).val(); 
            });
            
            $('#span-video').click(function() {
                if ($(this).is(':checked')) {
                    $('#annotation-start-end-time');
                    $('#annotation-end-time');
                    $('#annotation-start-end-time').css('opacity', '0.5');
					//console.log("span video");
                } else {
                    $('#annotation-start-end-time').css('opacity', '1.0');
                }
            });
            
        });
        function getPathFromURL(url) {
            return url.split("?")[0];
        }
        function jumpBox(list) {
              location.href = list.options[list.selectedIndex].value;
        }
    </script>
    <!-- Annotations related JS are here -->
 	 <script type="text/javascript">var role = "<?= $_SESSION['role'] ?>";</script>
    <script type="text/javascript">var grpId = "<?=$GID ?>";</script>
    <script type="text/javascript" src="ui_test.js"></script>
 	<!-- Video players wrapper functions are here -->
 	<script type="text/javascript" src="video_functions.js"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<style>
			.navbar, .well {
				margin-bottom: 0px;
			}
			.navbar-brand img {
				width: 80px;
			}
			.nav>li {
				width: 120px;
			}
			.nav .active{
				font-size:14px;
			}
			
</style>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQLCKQP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<nav class="navbar navbar-default">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="../oval-master"><img src="./icons/covaa_250px.png" alt="CoVAA"></a>
			  <span style="
    vertical-align: middle;
    line-height: 50px;
"> Welcome back, <?php echo "$userName"; ?> </span> 
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
				 
        
					<li class="active"><a href="../oval-master">Watch Videos<span class="sr-only">(current)</span></a></li>
					<?php if (count($groups) > 1): ?>
        <?php else: ?>
<?php endif; ?>
<?php if ($isAdmin == true):?>
		<li><a href="video_management.php?cid=<?php echo "$CID"?>&gid=<?php echo "$GID"?>">Admin</a></li>
		<li><a href="teacher_dashboard.php">Dashboard</a></li>
		<?php endif; ?>  
					
					<li><a href="logout" onClick="onLogOutEvent()">Logout</a></li>

				</ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	<div id="wrapper">
	<div style="margin-bottom:5px;">
	<form id="class" name="class" method="post" action="" style="float:left;">
            &nbsp;Subject:&nbsp;
            <?php 
				$noCourses = (0 == count($classes)) ? "disabled" : "";
            ?>
            <select name="classJumpMenu" id="classJumpMenu" <?php echo $noCourses?> onChange="jumpBox(this.form.elements[0])" style="width:11em;" >
            <?php 
            if (0 == count($classes)) {
            	print "<option>-- NO COURSES</option>";
            } else {
    
            	for ($i=0; $i<count($classes); $i++): ?>
                <?php
	                $ID     = $classes[$i]['ID'];
	                $name   = $classes[$i]['name'];
	                ($CID == $ID) ? $selected = "selected=\"selected\"" : $selected = "";
                ?>
                	<option value="index.php?cid=<?php echo "$ID"; ?>"<?php echo"$style $selected>$name"?></option>
			<?php endfor; 
			}
			?>
            </select>
        </form>
		<form id="group" name="group" method="post" action="" style="float:left;">
            &nbsp;Class:&nbsp;
            <?php 
				$noGroups = (0 == count($groups)) ? "disabled" : "";
            ?>
            <select name="groupJumpMenu" id="groupJumpMenu" <?php echo $noGroups?> onChange="jumpBox(this.form.elements[0])" style="width:8em;" >
            <?php 
            if (0 == count($groups)) {
				print "<option>-- NO GROUPS</option>";
			} else {
            	foreach ($groups as $ID=>$name): ?>
                <?php
	                ($GID == $ID) ? $selected = "selected=\"selected\"" : $selected = "";
	                
	                // Can't seem to apply style to an input option, what to do?
	                // ('U' == $ID) ? $style = "style=\"color:red\"" : $style = "";
                ?>
                	<option value="index.php?cid=<?php echo "$CID&gid=$ID"; ?>" <?php echo "$style $selected > $name"; ?></option>
            <?php endforeach; 
            }
            ?>
            </select>
        </form>
		
        <form id="video" name="video" method="post" action="">
            &nbsp;Video:&nbsp;    
            <?php if (0 == count($videos)) $videosDisabledStr='disabled="disabled"'; ?>
            <select name="jumpMenu" id="jumpMenu" onChange="jumpBox(this.form.elements[0])" style="width:41em;" <?php echo $videosDisabledStr?>>
<?php
("flag" == $uiConfig['annotation_mode']) ? $queryParam = "&flag_mode=1" : $queryParam = "";
if (0 == count($videos)) {
    print "<option>-- NO VIDEOS</option>";
} else {
    // fetch videos
    foreach ($videos as $video) {
		if ($isAdmin && sizeof($classes) > 1) {
            if ($classID != $video['class_id']) continue;
        }
        
        $videoID = $video['video_id'];
        ($VID == $videoID) ? $selected = "selected=\"selected\"" : $selected = "";
        $title = $video['title'];
        print "\t\t<option value=\"index.php?cid=$CID&gid=$GID&vid=$videoID$queryParam\" $selected>$title</option>\n";
    }
}
?>                    
            </select>
        </form>
		</div>
	<div id="left" style="width: 75%; float:left;">
	          

	
  <div id="player"></div>
  
<script>
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
      //tag.src = "https://clas.unisa.edu.au/youtube_player_api"; //Original source commented off by Simon Yang
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      var youtubeTime;
      var player1;
		window.onYouTubeIframeAPIReady = function() {
			player1 = new YT.Player('player', {
			height: '340',
			width: '980',
			videoId: '<?php echo $VID ?>',
			playerVars: {rel: 0, enablejsapi: 1},
			events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
			}
			});
		}
		//youtubeTime = player1.getCurrentTime();
// 4. The API will call this function when the video player is ready.
	function onPlayerReady(event) {
        //event.target.playVideo(); // play video once ready (auto play)
      }
	/* window.onPlayerReady(event) = function() {
		event.target.pauseVideo();
	} */
// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event){
	if (event.data == YT.PlayerState.PLAYING) {
	//alert("video is playing now!");
	insertPlay(player1.getCurrentTime());
	//alert("The video re-play start at position: " + player1.getCurrentTime());
	////_gaq.push(['_trackEvent', 'Videos', 'Play', '<?php echo $title?>']);
	////ga('send', 'event', 'Videos', 'Play', '<?php echo $title?>');
	// _gaq.push(['_trackEvent', 'User', 'Username', '<?php echo $userName?>']);
	// ga('send', 'User', 'Username', 'Username', '<?php echo $userName?>']);
	////_gaq.push(['_setCustomVar', 1, 'Username', '<?php echo $userName?>']);
	////_gaq.push(['_setCustomVar', 2, 'UserID', '<?php echo $userID?>']);
	 // setTimeout(stopVideo, 6000);
	  done = true;
	}
	else if (event.data == YT.PlayerState.PAUSED) {
	//alert("video is paused!");
	//alert("The video pause at position: " + player1.getCurrentTime());
	insertPause(player1.getCurrentTime());
	////_gaq.push(['_trackEvent', 'Videos', 'Pause', '<?php echo $title?>']);
	////ga('send', 'event', 'Videos', 'Pause', '<?php echo $title?>');
	}
	else if (event.data == YT.PlayerState.ENDED){
	//alert("watch to end of the video!");
	//alert("The video end position is: " + player1.getCurrentTime());
	insertEnd(player1.getCurrentTime());
	////_gaq.push(['_trackEvent', 'Videos', 'Watch to End', '<?php echo $title?>']);
	////ga('send', 'event', 'Videos', 'Watch to End', '<?php echo $title?>');
	} 
	else if (event.data == YT.PlayerState.BUFFERING) {
	//alert("video is buffering!");
	//alert("The video buffering position is: " + player1.getCurrentTime());
	insertBuffer(player1.getCurrentTime());
	////_gaq.push(['_trackEvent', 'Videos', 'Buffering', '<?php echo $title?>']);
	////ga('send', 'event', 'Videos', 'Buffering', '<?php echo $title?>');
	}
	else if (event.data == YT.PlayerState.CUED) {
	 // alert("video is cued!");
	insertCue(player1.getCurrentTime());
	////_gaq.push(['_trackEvent', 'Videos', 'Cueing', '<?php echo $title?>']);
	////ga('send', 'event', 'Videos', 'Cueing', '<?php echo $title?>');
	}
}
function stopVideo() {
	player.stopVideo();
}
/* window.stopVideo() = function() { //Simon block commented
player1.stopVideo();
} */
</script>
<script>
function insertPlay(t){
    console.log("insertPlay function called!");
     $.ajax({
        type: "POST",
        url: "ajax/event_record.php",
        data: {video_id: '<?php echo $VID ?>', play_start_position:t},
        success: function(data) {
        debug("data " + data);
        },
        async: false
    });
    }
    </script>
<script>
function insertPause(t){
    console.log("insertPause function called!");
     $.ajax({
        type: "POST",
        url: "ajax/pause_record.php",
        data: {video_id: '<?php echo $VID ?>', pause_position:t},
        success: function(data) {
        debug("data " + data);
        },
        async: false
    });
    }
    </script>
<script>
function insertEnd(t){
    console.log("insertEnd function called!");
     $.ajax({
        type: "POST",
        url: "ajax/end_record.php",
        data: {video_id: '<?php echo $VID ?>', end_position:t},
        success: function(data) {
        debug("data " + data);
        },
        async: false
    });
    }
    </script>
<script>
function insertBuffer(t){
    console.log("insertBuffer function called!");
     $.ajax({
        type: "POST",
        url: "ajax/buffer_record.php",
        data: {video_id: '<?php echo $VID ?>', buffer_position:t},
        success: function(data) {
        debug("data " + data);
        },
        async: false
    });
    }
    </script>
<script>
function insertCue(t){
    console.log("insertCue function called!");
     $.ajax({
        type: "POST",
        url: "ajax/cue_record.php",
        data: {video_id: '<?php echo $VID ?>', cue_position:t},
        success: function(data) {
        debug("data " + data);
        },
        async: false
    });
    }
    </script>
<script type='text/javascript'>
	
$(function(){
	/*$("a.reply_annotation").click(function() {
		
		var id = $(this).attr("id");
        console.log(id);
		$("#parent_id").val(id);
		//$("#name").focus();
	});*/
	
	$("a.reply_annotation").live('click', function(){
	var id = $(this).attr("id");
        console.log(id);
		$("#parent_id").val(id);
		//$("#name").focus();
	});
});
</script>
    <div id="message-dialog" title="Alert!" style="display:none;z-index:999;">text goes here...</div>
    <img id="recording-annotation" src="icons/annotating.png" alt="recording annotation" style="display:none;" height="19" width="120" />
    
   
<div id="annotation-form">
        <div id="annotation-title-bar" style="background-color:#8EA9C2;color:white;border-bottom:1px solid #888;width:400px;margin-bottom:20px;margin-left:-20px;display:block;height:25px;" >
           <div id="annotation-title" style="float:left;padding:2px;">Add Annotation</div>
           <a href="#" id="close-annotation" style="float:right;padding:4px;margin:0;text-decoration:none;border:0;">
                <img src="icons/close-button.gif" style="padding:0;margin:0;border:0;" width="17" height="17" alt="close annotation dialog button"  onClick="player1.playVideo();"/>
           </a>
        </div>
                <div id="annotation-start-time" class="annotation-label"></div>
		    <?php if (! $isiPad) { ?>
		                    <img id="start-time-rwd" src="icons/rwd-button.png" style="margin-left:5px;" alt="rewind start time" />
		                    <img id="start-time-fwd" src="icons/fwd-button.png" style="margin-left:8px;" alt="fast forward start time" />
		    <?php } ?>            
	            <form method="post" action="">
	                <!--<div class="annotation-label">type:
	                <select id="annotation-type"> 
	                  text
	                   <option>video</option>  WEBCAM ANNOTATION 
	                 </select>
                        </div>
	                <div class="annotation-label">private
	                <input type="checkbox" name="annotation-privacy" id="annotation-privacy" style="" /><br /></div>
	                <br style="margin-bottom:10px" />
	                <div id="webcam" style="margin-bottom:1em;display:none;"></div>--> <!-- WEBCAM ANNOTATION, turn on and off "display:none;" -->
                     <div class="annotation-label" style="">Question No. &nbsp</div><input type="text" name="aqns" id="aqns" size="31.5" value="" /><br>
                     <span style="color:red" id="commentError"></span><br />
	                <div class="annotation-label" style=""><a href="icons/criticallens.jpg" target="_blank">Critical Lens</a>&nbsp&nbsp</a></div>
	                <!--<input type="text" name="annotation-tags" id="annotation-tags" size="30" value="" />-->
                     <select style="width:19em;" name="annotation-tags" id="annotation-tags" >
                          <option value="Message/Issue" selected="true">Message/Issue</option>
                          <option value="Purpose">Purpose</option>
                          <option value="Audience">Audience</option>
                          <option value="Viewpoint">Viewpoint</option>
                          <option value="Assumption">Assumption</option>
                          <option value="Inference/Interpretation">Inference/Interpretation</option>
                          <option value="Consequences/Impact">Consequences/Impact</option>
                          <option value="Evidence">Evidence</option>
</select><br><span style="color:red" id="tagError"></span><br />
<div class="annotation-label" style=""><a href="icons/thinkingskills.jpg" target="_blank">Thinking Skill</a></div>
<select style="width:19em;"name="annotation-tags1" id="annotation-tags1"">
                          <option value="Ideate/Reflect" selected="true">Ideate/Reflect</option>
                          <option value="Justify/Explain">Justify/Explain</option>
                          <option value="Agree/Validate">Agree/Validate</option>
                          <option value="Disagree/Challenge">Disagree/Challenge</option>
                          <option value="Inquire/Clarify">Inquire/Clarify</option>
</select><br><span style="color:red" id="commentError"></span><br />
	                <div class="annotation-label" style="float:left;">Description</div><br />
	                <textarea id="annotation-description" name="annotation-description" rows="8" cols="48"></textarea><br><span style="color:red"" id="descError"></span><br />
					<input type='hidden' name='parent_id' id='parent_id' value='0'/>
                    <?php 
                    print "\t\t<input type='hidden' name='groupId' id='groupId' value=\"$GID\"/>\n";
                    print "\t\t<input type='hidden' name='classId' id='classId' value=\"$CID\"/>\n";
                    ?>
<?php
                    $conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);
    if (!$conn) {
    die('Not connected : ' . mysql_error());
}
    $db_selected = mysql_select_db($database, $conn);
    mysql_set_charset("utf8",$conn);
    //print_r($videoID);
    $result = mysql_query("SELECT * FROM media WHERE video_id = '$VID'");
    $student = mysql_fetch_assoc($result);
?>
                        
                        <?php if($student['point_one']){ ?>
                        <script>var yesChecked = true; </script> 
                        <!--<div class="annotation-label" style="display: inline-block;">Level of<br> confidence
                        <select name="confidence_type" id="confidence_type" selected="true">
                        <option>very high</option>
                        <option>high</option>
                        <option>medium</option>
                        <option>low</option>
                        <option>very low</option> 
                        
                        </select>
                         
                        </div>-->
                        <?php } ?>
 
	            </form>
            <div id="form-buttons">buttons go here...</div>
    </div>
<div id="annotation-form1">
        <div id="annotation-title-bar" style="background-color:#8EA9C2;color:white;border-bottom:1px solid #888;width:400px;margin-bottom:20px;margin-left:-20px;display:block;height:25px;" >
           <div id="annotation-title" style="float:left;padding:2px;">Reflection Points</div>
           <a href="#" id="close-annotation1" style="float:right;padding:4px;margin:0;text-decoration:none;border:0;">
                <img src="icons/close-button.gif" style="padding:0;margin:0;border:0;" width="17" height="17" alt="close annotation dialog button" />
           </a>
        </div>
                <div id="annotation-start-time" class="annotation-label"></div>
                    <?php if (! $isiPad) { ?>
                                    <img id="start-time-rwd" src="icons/rwd-button.png" style="margin-left:5px;" alt="rewind start time" />
                                    <img id="start-time-fwd" src="icons/fwd-button.png" style="margin-left:8px;" alt="fast forward start time" />
                    <?php } ?>
<?php
                    $conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);
    if (!$conn) {
    die('Not connected : ' . mysql_error());
}
    $db_selected = mysql_select_db('prod_annotation_tool', $conn);
    mysql_set_charset("utf8",$conn);
    //print_r($VID);
    $result = mysql_query("SELECT * FROM media WHERE video_id = '$VID'");
    $student = mysql_fetch_assoc($result);
    //print_r($student);
if ($student['point_one']){
?>
                    <form method="post" action="">
                       
                       <!--  <select id="annotation-type">  -->
                         
                            <!-- <option>video</option> --> <!-- WEBCAM ANNOTATION -->
                       <!-- </select> -->
                        <h5>Did your comment/reply meet the Universal Intellectual Standards of: </h5>
                        <table width ="80%">
                        <?php 
                            if (($student['point_two']==NULL || $student['point_two']=='NULL')&&($student['point_three']==NULL || $student['point_three']=='NULL')){?>
                            <tr>
                            <td>1.  <?php echo $student['point_one'];?></td>
                            <td> <input type="radio" name="choice" value="YES"><font size="2">Yes</font>                      
                        <input type="radio" name="choice" value="NO"><font size="2">No</font></td>
                          </tr>
                            <?php
                        }
                            else if ($student['point_three']==NULL || $student['point_three']=='NULL'){
                                ?>
                                 <tr>
                            <td>1.  <?php echo $student['point_one'];?></td>
                            <td> <input type="radio" name="choice" value="YES"><font size="2">Yes</font>                      
                        <input type="radio" name="choice" value="NO"><font size="2">No</font></td>
                          </tr>
                          <tr>
                           <td>2.  <?php echo $student['point_two'];?></td>
                            <td> <input type="radio" name="choice1" value="YES"><font size="2">Yes</font>                      
                        <input type="radio" name="choice1" value="NO"><font size="2">No</font></td>
                          </tr>
                          <?php
                            }
                            else{
                            ?>  
                             <tr>
                            <td>1.  <?php echo $student['point_one'];?></td>
                            <td> <input type="radio" name="choice" value="YES"><font size="2">Yes</font>                      
                        <input type="radio" name="choice" value="NO"><font size="2">No</font></td>
                          </tr>
                          <tr>
                           <td>2.  <?php echo $student['point_two'];?></td>
                            <td> <input type="radio" name="choice1" value="YES"><font size="2">Yes</font>                      
                        <input type="radio" name="choice1" value="NO"><font size="2">No</font></td>
                          </tr>
                          <tr>
                           <td>3.  <?php echo $student['point_three'];?></td>
                            <td> <input type="radio" name="choice2" value="YES"><font size="2">Yes</font>                      
                        <input type="radio" name="choice1" value="NO"><font size="2">No</font></td>
                          </tr>
                          <?php
                            }
                        ?>
                         
               
                          </table>
<h5>If you selected "No", you can go back and edit your comment/reply after you submit this reflection.</h5>
                        <p> 
    <input type="submit" name="submit" id="submit" value="Submit" /> 
  </p>
                    </form>
           <?php             
           }
             $answer1 = $_POST['choice'];
             $answer2 = $_POST['choice1'];
             $answer3 = NULL;
             if(isset($_POST['submit'])){
                 $conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);
                 if (!$conn) {
                 die('Not connected : ' . mysql_error());
                 }
                 $db_selected = mysql_select_db($database, $conn);
                 mysql_set_charset("utf8",$conn); 
                $result = mysql_query("INSERT INTO feedback VALUES (NULL, '$VID', '$userID', '$userName', '$answer1', '$answer2', '$answer3'    )  "); 
             }
           ?>
    </div>
   
<?php
    if ($flagMode) {
        $src        = "icons/download-flags.png";
        $style      = "background-image:url('icons/tab-flag.png')";
        $queryStr   = "video_id=$VID&viewAll=1&flag_mode=1";
    } else {
        // annotation mode
        $src        = "icons/download-annotations.png";
        $queryStr   = "video_id=$VID&viewAll=1&flag_mode=0&groupId=$GID";
    } 
?>
    <div id="buttons">
    	<div style="float:left;display:inline;">
      <!--	<span class="video_download_label" style="font-size:14px;display:inline;">Video Download:</span>  -->   
<?php
		// TODO: add on off switch in video management page
	     //	if ($isAdmin == true) { 
             //			if ($flavors != null && !empty($flavors)) {
	     //	        print "<br><span class=\"video_download_link\" style=\"font-size:14px;\"><a href=\"" . getVideoDownloadURL($VID) . "\">Original Format</a></span>";
	     //		} else {
	     //			print "<br><span>Unavailable</span>";
            //			}
	     //	} else {
	     //		print "<br><span>Unavailable</span>";
	     //	}
 			
?>		
		</div>
        
        <a href="ajax/download_annotations.php?<?php echo $queryStr?>" target="_blank" style="border-style:none;outline-style:none;">
            <img id="download-annotations" src="<?php echo $src?>" style="border-style:none;outline-style:none;" alt="download annotations" /> 
          <!--  <img id="download-annotations" src="icons/download-annotations.png" style="border-style:none;outline-style:none;" alt="download annotations" /> -->
        </a>
        <img id="add-annotation" src="icons/add-annotation.png" alt="add an annotation" style="" onClick="javascript:player1.pauseVideo();"/>
<?php
    $users = new users();
    ($users->trendlineVisible($userID, $VID)) ? $style = "block" : $style = "none";
    $users->close();
?>
    </div>
    
    <div id="annotation-container">
        <h3 style="font-weight:normal;">
            <div id="annotation-view" style="display:<?php "$style;color:#fff;float:left;"?>">
                view: <span style="cursor:pointer;">mine</span> / <strong>all</strong> 
            </div>
            <img src="icons/annotation-legend.jpg" style="position:relative;top:-15px;left:325px;" />
        </h3>
        <div id="annotation-list"></div> <!-- Annotations go here -->
    </div>
    <div id="annotation-trends">
        <h3 style="float:left;width:auto;">Trends</h3>
        <form id="search-annotations" style="display:inline;float:right;padding-right:4px;">
        	<label style="color:#fff;font-size:14px;">&nbsp;search</label>
        	<input type="text" size="15em" name="search" style="height:inherit" />
        	<label style="color:#fff;font-size:11px;">&nbsp;by content</label>
        	<input type="checkbox" class="search_criteria" id="search_by_content" style="height:inherit" checked />
        	<label style="color:#fff;font-size:11px;">author</label>
        	<input type="checkbox" class="search_criteria" id="search_by_author" style="height:inherit" checked />
        	<label style="color:#fff;font-size:11px;">tag</label>
        	<input type="checkbox" class="search_criteria" id="search_by_tag" style="height:inherit" checked />
        	<label style="color:#fff;font-size:11px;">auto-search</label>
        	<input type="checkbox" id="auto_hover_search" style="height:inherit" />
        </form>
        <div style="clear:both"></div>
        <div id="trends" style="clear:left;height:100%;width:100%;overflow:display;cursor:pointer;">Annotation trends</div>
    </div>
    <div style="clear:both"></div>
	</div>
	<?php
	$conn = mysql_connect('localhost', $mysqlUser, $mysqlPassword);
    if (!$conn) {
    die('Not connected : ' . mysql_error());
}
    $db_selected = mysql_select_db($database, $conn);
    mysql_set_charset("utf8",$conn);
    //print_r($videoID);
    $result = mysql_query("SELECT * FROM commentQns WHERE video_id = '$VID' AND groupId= $GID");
    $commentQns = mysql_fetch_assoc($result);?>
	<div id="comments">
        <h3>Let's Discuss and Learn Together!</h3>
		<div class="qns"><span class="label" style="font-size:75%; white-space:pre-wrap;margin-top:5px;"><?php echo $commentQns['user_name'];?></span><br/><span style="font-size:80%;word-wrap:break-word">
<?php echo $commentQns['description'];?>
</span>
<br>
<button type="button" id="add-comment" onClick="javascript:player1.pauseVideo();">Post Comment</button>
<!--<img src="icons/add-comment.png" id="add-comment" style="float:right;margin:8px 6px 8px 0px;cursor:pointer;" alt="add a comment" onClick="javascript:player1.pauseVideo();"/> -->
</div>
        <ul>
            <li></li>
        </ul>
        
    </div>
	
    </div> <!-- the wrapper, for page centering -->
  <!--  <div id="univbranding"><img src="icons"></img></div>  -->
  <div style="clear:both"></div>
  <br>
<div id="mydiv">
        <span style="font-size:12px;margin-top:0;"><b>Please run CoVAA in Chrome. CoVAA is not compatible with IE and Safari.</b></span>
        <p style="font-size:9px;margin-top:0;"><b>CoVAA Alpha version &copy; 2016, Dr Jennifer Pei-Ling Tan, National Institute of Education. Powered by CLAS/OVAL 2015 Shane Dawson, Aberlardo Pardo and colleagues.Funded by Edulab, National Research Foundation NRF2015-EDU001-IHL09.
        </b></p>
</div>
    <!-- Put error messages here (after determine all error conditions in the rest of the page) -->
    <script type="text/javascript">
<?php 
		if (isset($noCourses) && $noCourses != "") {
?>
			alert("You are not currently enrolled in any course that uses OVAL.\n\nIf this is in error, please contact your teacher.");
<?php 
		}
?>
    </script>
</body>
</html>