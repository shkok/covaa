<?php
	require_once(dirname(__FILE__) . "/includes/global_deploy_config.php");

	require_once(dirname(__FILE__) . '/includes/kaltura/kaltura_functions.php');

	require_once(dirname(__FILE__) . "/includes/common.inc.php");

	require_once(dirname(__FILE__) . "/includes/auth.inc.php");

	require_once(dirname(__FILE__) . '/database/users.php');

	require_once(dirname(__FILE__) . '/database/media.php');
	
	//require_once(dirname(__FILE__) . '/ga.php');

	error_reporting(1); //Simon added
	
	startSession();
	
	//Check if user is teacher, check subjects taught and classes associated
	$isAdmin  = isAdmin($_SESSION['role']);
	$userName = $_SESSION['name'];
	$userID   = $_SESSION['user_id'];
	
	//If not valid user, redirect to video page
	if(!$isAdmin)
	{
		header('location: ./index.php');
		exit();
	}
	//If valid user, database call to retrieve data based on user details
	else
	{
		$conn = new mysqli('localhost', $mysqlUser, $mysqlPassword, $database);

		if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
        //$db_selected = mysql_select_db($database, $conn);

        mysqli_set_charset("utf8",$conn); 

		//Create overall Array to store all subsequently retrieved data
		$overall_data = array();
		
		//Retrieve all subjects current teacher is associated with
		$query = "SELECT c.id as Subject_ID, c.name as Subject
					FROM classInstructorsAndTAs A
					JOIN class c
						ON A.class_id = c.id
					WHERE A.user_id = '$userID'";
		
		$result = mysqli_query($conn, $query);

		while ($row = mysqli_fetch_assoc($result)) {
			//var_dump($row);
			$subject_id = $row["Subject_ID"];
			$subject_name = $row["Subject"];
			$overall_data[$subject_id] = array( "id"=>$subject_id, "name"=>$subject_name, "classes"=>array() );
		}

		//print_r($overall_data);

		$subjects_string = "";
		foreach($overall_data as $subject)
		{
			if($subjects_string != "")
				$subjects_string .= ", ";
			$subjects_string .= $subject["id"];
		}
		//echo "<br><br>".$subjects_string."<br><br>";
		
		//Retrieve all Classes associated with current teacher based on each subject
		$query = "SELECT g.id as Class_ID, g.name as Class, g.class_id as Subject_ID
					FROM groups g
					JOIN groupOwners go
						ON go.group_id = g.id
						AND go.user_id = '$userID'
					JOIN class c
						ON g.class_id = c.id
						AND c.id IN ($subjects_string)";
		
		$result = mysqli_query($conn, $query);
		
		while ($row = mysqli_fetch_assoc($result)) {
			//var_dump($row);
			$class_id = $row["Class_ID"];
			$class_name = $row["Class"];
			$subject_id = $row["Subject_ID"];
			//echo "Class ID: " . $class_id , " Class Name: " . $class_name . " Subject ID: " . $subject_id . "<br>";
			$overall_data[$subject_id]["classes"][$class_id] = array( "id"=>$class_id, "name"=>$class_name, "students"=>array(), "videos"=>array() );
		}
		
		//To populate all classes with student objects
		foreach($overall_data as $subject)//Subject
		{
			$subject_id = $subject["id"];
			//echo "Subject: " . $subject_id . "<br>";
			foreach($subject["classes"] as $class)//Class
			{
				$class_id = $class["id"];
				//echo "Class: " . $class_id . "<br>";
				
				//Retrieve number of annotation questions from current class
				$query = "SELECT *
							FROM annotationQns aq
							WHERE groupId = $class_id";
							
				$result = mysqli_query($conn, $query);
				$annotationQns = array();
				
				while ($annotationQnsEntry = mysqli_fetch_assoc($result))
				{
					array_push($annotationQns, $annotationQnsEntry);
				}
				
				//Retrieve all videos from current class
				$query = "SELECT m.video_id, m.title
							FROM media m
							JOIN videoGroup vg
							ON m.video_id = vg.video_id
							AND vg.group_id = $class_id";
				
				$result = mysqli_query($conn, $query);
				
				while ($video = mysqli_fetch_assoc($result))
				{
					$video_id = $video["video_id"];
					$video_title = $video["title"];
					//echo "Video: " . $video_id . "<br>";
					
					$count_annotation_qns = 0;
					foreach($annotationQns as $annotationQn)
					{
						if($annotationQn["video_id"] == $video_id)
							$count_annotation_qns = $annotationQn["qnsNo"];
					}
					
					$overall_data[$subject_id]["classes"][$class_id]["videos"][$video_id] = array(
																						"id"=>$video_id,
																						"name"=>$video_title,
																						"qnsNo"=>$count_annotation_qns
																					);
				}
				
				//Retrieve all login data based on current class
				$query = "SELECT DISTINCT ls.user_id
							FROM loginSessions ls
							JOIN users u
								ON u.id = ls.user_id
							JOIN groupMembers m
								ON m.user_id = u.id
							JOIN groups g
								ON g.id = m.group_id
								AND g.id = $class_id";	
				
				$result = mysqli_query($conn, $query);
				
				$logins = array();
				
				while($row = mysqli_fetch_assoc($result)) {
					
					array_push($logins, $row["user_id"]);
				}
				
				//All data from pData table based on current class
				$query = "SELECT pD.userId, pD.action, pD.label
							FROM pData pD
							JOIN users u
								ON u.id = pD.userId
								AND u.role = 1
							JOIN groupMembers m
								ON m.user_id = u.id
							JOIN groups g
								ON g.id = m.group_id
								AND g.id = $class_id
							WHERE pD.action LIKE '%Watch To End%'";
								
				$result = mysqli_query($conn, $query);
				
				$pData = array();
				
				while($row = mysqli_fetch_assoc($result)) {
					array_push($pData, $row);
				}
				
				//Fetch all annotations/comments/replies from current class
				$query = "SELECT *
							FROM annotations a
							JOIN groups g
							ON g.id = a.groupId
							AND g.id = $class_id";
				$result = mysqli_query($conn, $query);
				
				$submissions = array();
				
				while ($submissionEntry = mysqli_fetch_assoc($result))
				{
					array_push($submissions, $submissionEntry);
				}
				
				//Grab all students from current class
				$query = "SELECT u.id as Student_ID, u.first_name as Student_Name
							FROM users u
							JOIN groupMembers m
								ON m.user_id = u.id
							JOIN groups g
								ON g.id = m.group_id
							WHERE g.id = $class_id
							AND u.role = 1";
				
				$result = mysqli_query($conn, $query);
				
				//Setup array of students with each student having the following:
					//login status
					//videos viewed
					//Made Annotation
					//Annotations Completed
					//Made Comments
					//Made Replies
				while ($student = mysqli_fetch_assoc($result))
				{
					
					$student_id = $student["Student_ID"];
					$student_name = $student["Student_Name"];
					//echo "Student: " . $student_id . "<br>";
					$overall_data[$subject_id]["classes"][$class_id]["students"][$student_id] = array(
																		"id"=>$student_id,
																		"name"=>$student_name,
																		"login"=>0, 
																		"viewedVideos"=> array(), 
																		"annotated"=>0, 
																		"completeAnnotations"=>0, 
																		"commented"=>0,
																		"replied"=>0,
																		"submissions"=> array()																		
																	);
					
					if(in_array($student_id, $logins))
						$overall_data[$subject_id]["classes"][$class_id]["students"][$student_id]["login"] = 1;
					
					foreach($pData as $pDataEntry)
					{
						if($pDataEntry["userId"] == $student_id)
						{
							$video_id = $pDataEntry["label"];
							$pos = strpos($video_id, "?v=");
							$video_id = substr($video_id, $pos+3);
							//if($pDataEntry["action"] == "100%")
								$overall_data[$subject_id]["classes"][$class_id]["students"][$student_id]["viewedVideos"][$video_id] = $video_id;
						}
					}
					
					foreach($submissions as $submissionEntry)
					{
						if($submissionEntry["user_id"] == $student_id)
							array_push($overall_data[$subject_id]["classes"][$class_id]["students"][$student_id]["submissions"], $submissionEntry);
					}
				}
			}
		}
		
		/* print "<pre>";
		print_r($overall_data);
		print "</pre>"; */

		mysqli_free_result($result);
		mysqli_close($conn);
		
		$overall_data = json_encode($overall_data);
		
		$data = '<script type="text/javascript">
		
					var overall_data = '.$overall_data.';
					//console.log(overall_data);
				';
		
		$data .= "</script>";
		
		echo $data; 
	}	
?>

<!doctype html>

<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>Teacher Dashboard</title>
		<meta name="description" content="Detailed info for students participation">
		<meta name="author" content="Simon">

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!--Load the AJAX API-->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		
	  <!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	  <![endif]-->
	  
		<style>
			/*Top menu bar css*/
			.bg-light {
				background-color: #26418F !important; /*#2962ff #3f51b5*/
			    color: white;
			    border-radius: 0px;
			}

			#nav-container {
				padding-right: 0px;
			}

			#page-title {
			    color: #83C8F6;
			    font-size: 16px !important;
			    line-height: 1.5;
			    padding-top: 5px;
			}

			.navbar-nav a {
				color: white;
			}

			.navbar-light .navbar-nav .active>.nav-link,
			.navbar-light .navbar-nav .nav-link {
			    color: white;
			}
			
			.nav>li>a {
				padding: 7px 7px;
			}

			.nav>li>a:hover {
			    text-decoration: underline;
			    color: white;
			    background-color: transparent;
			}

			#bs-example-navbar-collapse-1 > ul > li > a {
				font-size: 14px;
			   /* font-weight: 400;
			    line-height: 1.5;*/
			}

			.navbar-nav > .active {
			    background-color: grey;
			}

			.navbar, .well {
				/*margin-top: -4px;
				margin-bottom: 0px;*/
				/*padding: .5rem 1rem;*/
				height: 35px;
				font-size: 14px;
				min-height: 20px;
			    padding: 0px 10px;
			    border: 0px;
			}

			.navbar-brand img {
				margin: -12px 0 0 -14px;
				width: 80px;
			}

			.navbar-right {
			    /*margin-top: 4px;*/
			}
			/*End of Top menu bar css*/


			#container {
				margin: 0px;
				padding: 10px;
				background-color: #F4F5F9;
			}
			body {

				font-family: 'Roboto', sans-serif;

				background-color: #F4F5F9;
			}
			#page_title {
				margin: 1px auto 7px auto;
			}
			hr {
				margin: 1px;
			}
			/* #summary_div, #selection_div, #filter_div {
				padding: 15px;
			} */
			#subjectLabel, #classLabel, #classSelect, #videoLabel, #videoSelect {
				/*display: none;*/
				margin-left: 5px;
			}
			.grid {
			}
			.grid_tile {
				/* width: 250px; */
				max-width: 450px;
				background-color: #FFFFFF;
				margin: 10px 0;
				color: #2c292d;
				font-size: 2vw;
				text-align: center;
				border-radius: 4px;
				border: solid 1px #676A7C;
				background-image: -moz-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
				background-image: -webkit-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
			}
			.grid_tile_bar {
				height: 295px;
			}
			.grid_tile_annotations {
				height: 600px;
			}
			.grid_title {
				font-weight: bold;
				font-size: 18px;
				text-align: center;
				vertical_align: center;
				padding: 2px 5px 0px 5px;
			}
			.grid_number {
				font-size: 60px;
			}
			.grid_text {
				font-size: 15px;
				padding: 0 10px 0 10px;
			}
			.grid_chart {
				display: inline-block; /*To center align the chart*/
				height: 150px;
				width: 90%;
			}
			#annotationQns_chart, annotated_chart {
				display: inline-block; /*To center align the chart*/
				height: 200px;
				width: 100%;
			}
			.grid_link {
				text-align: center; 
				padding: 0 10px 5px 10px;
			}
			/* .btn-red, .btn-red:hover, btn-red:visited, btn-red:active {
				background: #e44332 !important;
				border-color: inherit !important;
				color: #2c292d;
			}
			.btn-blue, .btn-blue:hover, btn-blue:visited, btn-blue:active {
				background: #32d3e4 !important;
				border-color: inherit !important;
				font-weight: bold;
				color: #2c292d;
			} */
			.grid_tile_student {
				background-color: #FFFFFF;
				margin: 10px;
				padding: 10px;
				color: #676A7C;
				border-radius: 4px;
				border: solid 1px #676A7C;
				background-image: -moz-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
				background-image: -webkit-linear-gradient( 0deg, rgb( 255, 255, 255 ) 0%, rgb( 255, 255, 255 ) 100%);
			}
			.grid_tile_student > button {
				float: right;
			}
			.back-to-top {
				cursor: pointer;
				position: fixed;
				bottom: 20px;
				right: 20px;
				display:none;
			}
			.modal-body {
				min-height: 500px;
			}
			.student-label {
				margin: 5px;
				padding: 5px;
				border: 3px solid;
				border-radius: 5px;
				text-align: center; 
			}
			.student-label a {
				font-weight: bold;
			}
			.positive {
				border-color: #00ACEC;
			}
			.positive-bg {
				background-color: #00ACEC;
			}
			.negative {
				border-color: #FF1655;
			}
			.negative-bg {
				background-color: #FF1655;
			}
			.list-group {
				font-weight: normal;
				text-align: left;
			}
		</style>
	</head>

	<body>
		<nav class="navbar navbar-expand-sm navbar-light bg-light">
		  <div id="nav-container" class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header" style="width:425px;">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="../oval-master/index.php"><img src="./icons/covva_small.png" alt="CoVAA"></a>
            	<div id="page-title">Collaborative Video Annotation & Analytics</div>
			</div>
			
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right"> 
					<li><a href="../oval-master/index.php">Watch Videos</a></li>
					<?php if ($isAdmin == true): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="video_management.php?cid=<?php echo "$CID"?>&gid=<?php echo "$GID"?>">Upload</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="../oval-master/teacher_dashboard.php">Teacher Dashboard</a>
                        </li>
                        <!--  <a href="video_management.php" >admin</a> -->
                    <?php endif; ?>
					<li><a class="nav-link" href="../oval-master/student_dashboard_test.php">Learning Dashboard</a></li> 
					<li><a class="nav-link" href="../oval-master/survey.php">Survey</a></li>
					<li><a href="../oval-master/logout">Logout</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<div id="container" class="container-fluid">
			<div>
				<h4 id="page_title">Teacher Dashboard</h4>
				<label id="subjectLabel">Subject:</label><select id="subjectSelect">
					<option value="">Choose a Subject</option>
				</select>
				<label id="classLabel">Class:</label><select id="classSelect">
					<option value="">Choose a Class</option>
				</select>
				<label id="videoLabel">Video:</label><select id="videoSelect">
					<option value="">Choose a Video</option>
				</select>
			</div>
			<div id="summary_div" class="container-fluid">
				<?php
					//Creation of grid of individual information
				?>
				<div class="row">
					<div class="center-block col-height col-sm-4 col-md-4">
						<div class="grid_tile grid_tile_bar" id="login_info_div">
							<div class="grid_title">LOGINS</div><hr>
							<div id="login_num" class="grid_number">0</div>
							<div class="grid_text">student(s) have yet to log in.</div>
							<div class="grid_chart" id="login_chart"></div><!--<hr>
							<div class="grid_link">
								<button type="button" class="btn btn-info btn-xs btn-blue" data-toggle="modal" data-target="#myModal">Logged in</button>
								<button type="button" class="btn btn-info btn-xs btn-red" data-toggle="modal" data-target="#myModal">Never logged in</button>
							</div>-->
						</div>
					
						<div class="grid_tile grid_tile_bar">
							<div class="grid_title">POST COMMENTS</div><hr>
							<div id="commented_num" class="grid_number">0</div>
							<div class="grid_text">student(s) have <strong>not posted</strong> a comment</div>
							<div class="grid_chart" id="comment_chart"></div>
							<!--<div class="grid_link">
								<button type="button" class="btn btn-info btn-xs btn-blue" data-toggle="modal" data-target="#myModal">Posted comments</button>
								<button type="button" class="btn btn-info btn-xs btn-red" data-toggle="modal" data-target="#myModal">No comments posted</button>
							</div>-->
						</div>
					</div>
					<div class="center-block col-height col-sm-4 col-md-4">
						<div class="grid_tile grid_tile_bar">
							<div class="grid_title">VIEW VIDEOS</div><hr>
							<div id="viewed_num" class="grid_number">0</div>
							<div class="grid_text">student(s) have <strong>not finished watching</strong> the video.</div>
							<div class="grid_chart" id="view_video_chart"></div>
							<!--<div class="grid_link">
								<button type="button" class="btn btn-info btn-xs btn-blue" data-toggle="modal" data-target="#myModal">Viewed Videos</button>
								<button type="button" class="btn btn-info btn-xs btn-red" data-toggle="modal" data-target="#myModal">No videos viewed</button>
							</div>-->
						</div>
						
						<div class="grid_tile grid_tile_bar">
							<div class="grid_title">POST REPLIES</div><hr>
							<div id="replied_num" class="grid_number">0</div>
							<div class="grid_text">student(s) have <strong>not posted</strong> a reply</div>
							<div class="grid_chart" id="reply_chart"></div><!--<hr><hr>
							<div class="grid_link">
								<button type="button" class="btn btn-info btn-xs btn-blue" data-toggle="modal" data-target="#myModal">Posted replies</button>
								<button type="button" class="btn btn-info btn-xs btn-red" data-toggle="modal" data-target="#myModal">No replies posted</button>
							</div>-->
						</div>
					</div>
					<div class="center-block col-height col-sm-4 col-md-4">
						<div class="grid_tile grid_tile_annotations">
							<div class="grid_title">COMPLETE ANNOTATIONS</div><hr>
							<div id="annotated_num" class="grid_number">0</div>
							<div class="grid_text">student(s) have <strong>not posted</strong> an annotation </div>
							<div id="annotated_chart"></div>
							<div id="annotationQns_chart">There are no annotation questions.</div>
						</div>
						<!--
						<div class="grid_tile grid_tile_bar">
							<div class="grid_title">Annotations: Completed:</div><hr>
							<div class="grid_text"><text class="grid_number">10</text> student(s) have <strong>not completed</strong> the annotation questions</div>
							<div class="grid_chart" id="annotation_questions_chart"></div><hr><hr>
							<div class="grid_link">
								<button type="button" class="btn btn-info btn-xs btn-blue" data-toggle="modal" data-target="#myModal">Completed</button>
								<button type="button" class="btn btn-info btn-xs btn-red" data-toggle="modal" data-target="#myModal">Have not completed</button>
							</div>
						</div>-->
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div id="filter_div" class="row">
				</div>
				<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
			</div>
			
			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content container-fluid">
				  <div class="modal-header">
					<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
					<button type="button" class="close" data-toggle="collapse" data-parent="#accordion" href=".collapseStudents" aria-expanded="false" aria-controls="collapseStudents">Toggle Details</button>
					<h4 id="modal-title" class="modal-title">Category Title</h4>
				  </div>
				  <div id="modal-body" class="modal-body row">				
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				</div>

			  </div>
			</div>
		</div>
		
		<!--Load scripts for any visualizations library, + oval related, + click stream related etc-->
		
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		
			function round(value, decimals) {
				return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
			}
			
			var selectedSubject = "";
			var selectedClass = "";
			var selectedVideo = "";
			
			var selectedStudents;
			var total_students_count = 0;
			
			var logged_in = 0;
			var never_logged_in = 0;
				
			var watched_video = 0;
			var never_watched_video = 0;
			
			var annotated = 0;
			var commented = 0;
			var replied = 0;
			
			var completedQnsTracker = [];
		
			$(document).ready(function(){
				 $(window).scroll(function () {
					if ($(this).scrollTop() > 10) {
						$('#back-to-top').fadeIn();
					} else {
						$('#back-to-top').fadeOut();
					}
				});
				// scroll body to 0px on click
				$('#back-to-top').click(function () {
					$('#back-to-top').tooltip('hide');
					$('body,html').animate({
						scrollTop: 0
					}, 800);
					return false;
				});
				
				$('#back-to-top').tooltip('show');
				
				
				//Populating Subject/Class/Video Dropdown on default
					var subjectOptions = '';
					
					$.each( overall_data, function( i, l ){
						subjectOptions += '<option value="'+ overall_data[i]["id"] + '">' + overall_data[i]["name"] + '</option>';
						
					});
					$('#subjectSelect').append(subjectOptions);
					$("#subjectSelect option:last").prop("selected", "selected");
					
					selectedSubject = $("#subjectSelect").val();
					var classOptions = '<option value="">Choose a Class</option>';		
					$.each( overall_data[selectedSubject]["classes"], function( i, l ){
						classOptions += '<option value="'+ overall_data[selectedSubject]["classes"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][i]["name"] + '</option>';
						
					});
					$('#classSelect').empty().append(classOptions);
					$("#classSelect option:last").prop("selected", "selected");
					
					selectedClass = $('#classSelect').val();
					var videoOptions = '<option value="">Choose a Video</option>';
							
					$.each( overall_data[selectedSubject]["classes"][selectedClass]["videos"], function( i, l ){
						videoOptions += '<option value="'+ overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["name"] + '</option>';
					});
					
					$('#videoSelect').empty().append(videoOptions);
					$("#videoSelect option:last").prop("selected", "selected");
					drawAllCharts();
				//End of populated subject/class/video dropdowns on default
				
				
				//Populate Class Dropdown when Subject chosen
				$('#subjectSelect').change(function(){
					selectedSubject = $("#subjectSelect").val();
					
					$('#videoLabel').fadeOut();
						$('#videoSelect').fadeOut();
					
					if(selectedSubject === "")
					{
						$('#classLabel').fadeOut();
						$('#classSelect').fadeOut();
					}
					else
					{
						var classOptions = '<option value="">Choose a Class</option>';
						
						$.each( overall_data[selectedSubject]["classes"], function( i, l ){
							classOptions += '<option value="'+ overall_data[selectedSubject]["classes"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][i]["name"] + '</option>';
						});
						$('#classSelect').empty().append(classOptions);
						
						$('#classLabel').fadeIn();
						$('#classSelect').fadeIn();
					}
				});
				
				//Populate Video Dropdown when class chosen
				$('#classSelect').change(function(){
					selectedClass = $('#classSelect').val();
					
					if(selectedClass === "")
					{
						$('#videoLabel').fadeOut();
						$('#videoSelect').fadeOut();
					}
					else
					{
						var videoOptions = '<option value="">Choose a Video</option>';
						
						$.each( overall_data[selectedSubject]["classes"][selectedClass]["videos"], function( i, l ){
							videoOptions += '<option value="'+ overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["id"] + '">' + overall_data[selectedSubject]["classes"][selectedClass]["videos"][i]["name"] + '</option>';
						});
						
						$('#videoSelect').empty().append(videoOptions);
						
						$('#videoLabel').fadeIn();
						$('#videoSelect').fadeIn();
					}
				});				
				
				//Key function that tabulates the visualizations values
				$('#videoSelect').change(function(){drawAllCharts()});
				
				function drawAllCharts()
				{
					selectedVideoID = $('#videoSelect').val();

					//Where all the javascript magic to show the detail happens.
					selectedStudents = overall_data[selectedSubject]["classes"][selectedClass]["students"];
					selectedVideo = overall_data[selectedSubject]["classes"][selectedClass]["videos"][selectedVideoID];
					
					var qnsNo = parseInt(selectedVideo["qnsNo"]);
					
					//console.log(selectedStudents);
					console.log(selectedVideo);
					
					//Reset Global Variables
						logged_in = 0;
						never_logged_in = 0;
						
						watched_video = 0;
						never_watched_video = 0;
						
						annotated = 0;
						commented = 0;
						replied = 0;
										
						completedQnsTracker = [];
						
					console.log("qnsNo: "+ qnsNo);
					
					for(var i=0; i<qnsNo; i++)//Question number starts from 1, thus the +1
					{
						completedQnsTracker[i] = 0;//Setup each question as 0 student answered
					}
					
					console.log("completedQnsTracker: "+completedQnsTracker);
					
					$.each(selectedStudents, function(i, l){
						
						var completedAnnotations = [];
						
						$.each(selectedStudents[i]["submissions"], function(j, k){
							
							var video_id = selectedStudents[i]["submissions"][j]["video_id"];
							
							if(video_id === selectedVideo["id"])
							{
								var questionNum = parseInt(selectedStudents[i]["submissions"][j]["qnsNo"]);
								var parent_id = selectedStudents[i]["submissions"][j]["parent_id"];
								var child_id = selectedStudents[i]["submissions"][j]["child_id"];
								
								if(questionNum > 0 && questionNum <= qnsNo)//If its an annotation answering a question within the question num range
								{//"questionNum-1" is because qnsNo starts from 1, while array starts from 0.
									if(completedAnnotations[questionNum-1] == null)
										completedQnsTracker[questionNum-1] += 1;//To add 1 more completed marker to overall class-video completed counter.
									
									if(completedAnnotations[questionNum-1] == null)//To make sure record is not NaN
										completedAnnotations[questionNum-1] = 0;
									
									completedAnnotations[questionNum-1] += 1;//To store annotation question answered for this video
									selectedStudents[i]["annotated"] = 1;//To mark that this student has at least 1 annotation
								}
								else if(parent_id !== null)//else if it is a general reply
									selectedStudents[i]["replied"] = 1;	//To mark that this student has at least 1 reply
								else if(questionNum === 0)//else if it is a general comment
									selectedStudents[i]["commented"] = 1;//To mark that this student has at least 1 comment	 
								/*else if(questionNum === 0)//Just a random annotation not answering a question
									selectedStudents[i]["annotated"] = 1;//To mark that this student has at least 1 annotation */
								
								
							}
						});//End of loop for each submission of current student
						
						if(completedAnnotations.length === qnsNo && qnsNo > 0)//If number of unique question annotation is the same as total number of questions asked
							selectedStudents[i]["completeAnnotations"] = 1;//To mark that this student has completed all annotation questions
						
						selectedStudents[i]["completedAnnotations"] = completedAnnotations;//To attach array to the student object
						
						
						if(selectedStudents[i]["login"] === 1)
							logged_in++;
						else
							never_logged_in++;
						
						if(selectedStudents[i]["viewedVideos"][selectedVideo["id"]])
							watched_video++;
						else
							never_watched_video++;
						
						if(selectedStudents[i]["annotated"] === 1)
							annotated++;
						if(selectedStudents[i]["commented"] === 1)
							commented++;
						if(selectedStudents[i]["replied"] === 1)
							replied++;
						
					});//End of loop for each student
					
					total_students_count = Object.keys(selectedStudents).length;
					
					console.log(selectedStudents);
					console.log("total_students_count: "+total_students_count);
					console.log("logged_in: "+logged_in);
					console.log("watched_video: "+watched_video);
					console.log("annotated: "+annotated);
					console.log("commented: "+commented);
					console.log("replied: "+replied);
					console.log("qnsNo: "+qnsNo);
					console.log("completedQnsTracker: "+completedQnsTracker);
					
					google.charts.setOnLoadCallback(drawLoginChart);
					google.charts.setOnLoadCallback(drawViewVideosChart);
					google.charts.setOnLoadCallback(drawCommentChart);
					google.charts.setOnLoadCallback(drawReplyChart);
					google.charts.setOnLoadCallback(drawAnnotationChart);
					
					if(qnsNo > 0)
						google.charts.setOnLoadCallback(drawAnnotationQnsChart);	
					else
						$("#annotationQns_chart").html("There are no annotation questions.");
				}
			
			});
		
			// Load the Visualization API and the corechart package.
			google.charts.load('current', {'packages':['corechart']});

			// Set a callback to run when the Google Visualization API is loaded.
			 /*google.charts.setOnLoadCallback(drawLoginChart);
			google.charts.setOnLoadCallback(drawViewVideosChart);
			google.charts.setOnLoadCallback(drawAnnotationChart);
			google.charts.setOnLoadCallback(drawAnnotationQuestionsChart);
			google.charts.setOnLoadCallback(drawCommentChart);
			google.charts.setOnLoadCallback(drawReplyChart); */

			// Set BAR chart options
			var options = {
						   //colors:['#32d3e4','#e44332'],//colors:['#109618','#3366CC'],
						   colors:['#00ACEC','#FF1655'],
						   legend: {
								position: 'bottom', 
								cursor: 'pointer', 
								textStyle: {fontSize: 11}
							},
							animation:{
								startup: true,
								duration: 1000,
								easing: 'out',
							},
							//enableInteractivity: false,
							isStacked: 'percent',
							'chartArea': {'top': '-20','width': '100%', 'height': '60%'},
							hAxis: {
								gridlines: {
									count: 0,
									color: 'transparent'
								},
								scaleType: 'linear',
								minValue: 0,
								baselineColor: 'transparent'
							},
							vAxis: {
								gridlines: {
									color: 'transparent'
								},
								scaleType: 'linear',
								minValue: 0,
								baselineColor: 'transparent'
							},
							annotations: {
								alwaysOutside: false,
							  textStyle: {
								  //fontName: 'Times-Roman',
								  color: '#FFFFFF',
								  fontSize: 12,
								  bold: true,
								  auraColor: 'none',
								  offset: 100
								  /* ,
								  italic: true,
								  color: '#871b47',     // The color of the text.
								  auraColor: '#d799ae', // The color of the text outline.
								  opacity: 0.8          // The transparency of the text. */
								}
							}
						  };
			
			function drawLoginChart() {

				var logged_in_percentage = round(logged_in/total_students_count*100, 1);
				var never_logged_in_percentage = round(never_logged_in/total_students_count*100, 1);
				
				// Create the data table.			
				var data = google.visualization.arrayToDataTable([
						['', 'Logged In', { role: 'annotation', value: 'Logged In'}, 'Not Logged In Yet', { role: 'annotation', value: 'Not Logged In Yet'} ],
						['', logged_in, logged_in_percentage+'%', never_logged_in, never_logged_in_percentage+'%']
					]);

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.BarChart(document.getElementById('login_chart'));
				chart.draw(data, options);
				
				$("#login_num").html(never_logged_in); //Reflect the not logged in value as the big number in the grid box
				
				google.visualization.events.addListener(chart, 'onmouseover', function(){
					$('#login_chart').css('cursor','pointer');
				});
				
				google.visualization.events.addListener(chart, 'onmouseout', function(){
					$('#login_chart').css('cursor','default');
				});
				
				google.visualization.events.addListener(chart, 'select', function () {
					selectHandler(chart, data);
				});
			}
			
			function drawViewVideosChart() {				
				var watched_video_percentage = round(watched_video/total_students_count*100, 1);
				var never_watched_video_percentage = round(never_watched_video/total_students_count*100, 1);
				
				// Create the data table.			
				var data = google.visualization.arrayToDataTable([
						['', 'Watched', { role: 'Watched'}, 'Not Yet Watched', { role: 'Not Yet Watched'} ],
						['', watched_video, watched_video_percentage+'%', never_watched_video, never_watched_video_percentage+'%']
					]);

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.BarChart(document.getElementById('view_video_chart'));
				chart.draw(data, options);
				
				$("#viewed_num").html(never_watched_video); //Reflect the never watched video in value as the big number in the grid box
				
				google.visualization.events.addListener(chart, 'onmouseover', function(){
					$('#view_video_chart').css('cursor','pointer');
				});
				
				google.visualization.events.addListener(chart, 'onmouseout', function(){
					$('#view_video_chart').css('cursor','default');
				});
				
				google.visualization.events.addListener(chart, 'select', function () {
					selectHandler(chart, data);
				});
			}
			
			function drawCommentChart() {
				var commented_percentage = round(commented/total_students_count*100, 1);
				var never_commented = total_students_count - commented;
				var never_commented_percentage = round(never_commented/total_students_count*100, 1);
				
				// Create the data table.			
				var data = google.visualization.arrayToDataTable([
						['', 'Commented', { role: 'annotation'}, 'Not Yet Commented', { role: 'annotation'} ],
						['', commented, commented_percentage+'%', never_commented, never_commented_percentage+'%']
					]);

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.BarChart(document.getElementById('comment_chart'));
				chart.draw(data, options);
				
				$("#commented_num").html(never_commented); //Reflect the never commented value as the big number in the grid box
				
				google.visualization.events.addListener(chart, 'onmouseover', function(){
					$('#comment_chart').css('cursor','pointer');
				});
				
				google.visualization.events.addListener(chart, 'onmouseout', function(){
					$('#comment_chart').css('cursor','default');
				});
				
				google.visualization.events.addListener(chart, 'select', function () {
					selectHandler(chart, data);
				});
			}
			
			function drawReplyChart() {
				var replied_percentage = round(replied/total_students_count*100, 1);
				var never_replied = total_students_count - replied;
				var never_replied_percentage = round(never_replied/total_students_count*100, 1);
				
				// Create the data table.			
				var data = google.visualization.arrayToDataTable([
						['', 'Replied', { role: 'annotation'}, 'Not Yet Replied', { role: 'annotation'} ],
						['', replied, replied_percentage+'%', never_replied, never_replied_percentage+'%']
					]);

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.BarChart(document.getElementById('reply_chart'));
				chart.draw(data, options);
				
				$("#replied_num").html(never_replied); //Reflect the never replied value as the big number in the grid box
				
				google.visualization.events.addListener(chart, 'onmouseover', function(){
					$('#reply_chart').css('cursor','pointer');
				});
				
				google.visualization.events.addListener(chart, 'onmouseout', function(){
					$('#reply_chart').css('cursor','default');
				});
				
				google.visualization.events.addListener(chart, 'select', function () {
					selectHandler(chart, data);
				});
			}
			
			function drawAnnotationChart() {

				var not_annotated = total_students_count - annotated;
				
				// Create the data table.
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Status');
				data.addColumn('number', 'Students');
				data.addRows([
				  ['Annotated', annotated],
				  ['Not Yet Annotated', not_annotated]
				]);

				// Set chart options
				var options = {
					width:'100%',
					height:'100%',
					pieHole: 0.4,
					pieSliceTextStyle: {
						color: '#FFFFFF',
						fontSize: 12,
						bold: true
					},
					/*legend: {position: 'none'},*/
					 legend: {
						position: 'bottom', 
						cursor: 'pointer', 
						textStyle: {fontSize: 11}}, 
					animation: {
						startup: true,
						duration: 1000,
						easing: 'out',
					},
					colors: ['#00ACEC','#FF1655'], //colors:['#32d3e4','#e44332'],
					'chartArea': {'left':'0', 'top': '5', 'width': '100%', 'height': '80%'},
					pieStartAngle: 180,
				};

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.PieChart(document.getElementById('annotated_chart'));
				chart.draw(data, options);
				
				$("#annotated_num").html(not_annotated); //Reflect the never annotated value as the big number in the grid box
				
				google.visualization.events.addListener(chart, 'onmouseover', function(){
					$('#annotated_chart').css('cursor','pointer');
				});
				
				google.visualization.events.addListener(chart, 'onmouseout', function(){
					$('#annotated_chart').css('cursor','default');
				});
				
				function selectPieHandler() 
				{
					var selectedItem = chart.getSelection()[0];

					if (selectedItem) 
					{
						var selectedValue = data.getValue(selectedItem.row, 0);
						var studentListHTML = "<div class='container-fluid'><div class='row'>";
						
						if(selectedValue == "Annotated")
						{
							$("#modal-title").html("Students who have annotated at least once");
						
							$.each(selectedStudents, function(i, l){
								
								if(selectedStudents[i]["annotated"] === 1)
								{
									studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
									//studentListHTML += '<div class="col-sm-4 col-md-2 student-label positive">'+selectedStudents[i]["name"]+'</div>';
								}
							});
						}
							
						else if(selectedValue == "Not Annotated")
						{
							$("#modal-title").html("Students who have not annotated");
						
							$.each(selectedStudents, function(i, l){
								
								if(selectedStudents[i]["annotated"] === 0)
								{
									studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
									//studentListHTML += '<div class="col-sm-4 col-md-2 student-label negative">'+selectedStudents[i]["name"]+'</div>';
								}
							});
						}
						
						studentListHTML+= '</div></div>';
						$("#modal-body").html(studentListHTML);
							
						$('#myModal').modal('toggle');
					}
				}
				
				google.visualization.events.addListener(chart, 'select', selectPieHandler);
			}
			
			function drawAnnotationQnsChart() {
				
				console.log("completedQnsTracker length: " + completedQnsTracker.length);
				
				var dataArray = [];
				
				dataArray.push(['Question', 'Annotated', { role: 'annotation' }, 'Not Yet Annotated', { role: 'annotation' } ]);
				
				$.each(completedQnsTracker, function(i, l){
					var qNum = i+1;
					var done_this_qn = completedQnsTracker[i];
					var done_this_qn_percentage = round(done_this_qn/total_students_count*100, 1);
					var num_not_done_this_qn = total_students_count - done_this_qn;
					var num_not_done_this_qn_percentage = round(num_not_done_this_qn/total_students_count*100, 1);
					
					var tempArray = ['Q'+qNum, done_this_qn, done_this_qn_percentage+'%', num_not_done_this_qn, num_not_done_this_qn_percentage+'%'];
					dataArray.push(tempArray);
				});
				
				/* var data = google.visualization.arrayToDataTable([
					['Question', 'Annotated', { role: 'annotation' }, 'Never Annotated', { role: 'annotation' } ],
					['Q1', 10, '', 24, '']
				  ]); */
				
				// Create the data table.			
				var data = google.visualization.arrayToDataTable(dataArray);

				var options = {
						   colors: ['#00ACEC','#FF1655'],//colors:['#32d3e4','#e44332'],//colors:['#109618','#3366CC'],
						   legend: {
								position: 'none', 
								cursor: 'pointer', 
								textStyle: {fontSize: 11}
							},
							animation:{
								startup: true,
								duration: 1000,
								easing: 'out',
							},
							isStacked: 'percent',
							'chartArea': {'width': '80%', 'height': '80%'},
							hAxis: {
								gridlines: {
									count: 0,
									color: 'transparent'
								},
								scaleType: 'linear',
								minValue: 0,
								baselineColor: 'transparent'
							},
							vAxis: {
								gridlines: {
									color: 'transparent'
								},
								scaleType: 'linear',
								minValue: 0,
								baselineColor: 'transparent' 
							},
							annotations: {
							  textStyle: {
								  //fontName: 'Times-Roman',
								  color: '#FFFFFF',
								  fontSize: 12,
								  bold: true,
								  auraColor: 'none' ,
								  /*italic: true,
								  color: '#871b47',     // The color of the text.
								  auraColor: '#d799ae', // The color of the text outline.
								  opacity: 0.8          // The transparency of the text. */
								}
							}
						  };
					
				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.BarChart(document.getElementById('annotationQns_chart'));
				chart.draw(data, options);
				
				google.visualization.events.addListener(chart, 'onmouseover', function(){
					$('#annotationQns_chart').css('cursor','pointer');
				});
				
				google.visualization.events.addListener(chart, 'onmouseout', function(){
					$('#annotationQns_chart').css('cursor','default');
				});
				
				function selectHandler() {
					var selectedItem = chart.getSelection()[0];
					if (selectedItem) {
						var question = data.getValue(selectedItem.row, 0);
						question = question.substring(1);
						question = parseInt(question);
						
						//var value = data.getValue(selectedItem.row, selectedItem.column);
						var studentListHTML = "<div class='container-fluid'><div class='row'>";

						if(selectedItem.column === 1 || selectedItem.column === 2)//Selected annotated value
						{
							//alert('selected annotated people for '+question);

							$("#modal-title").html("Students who answered Annotation Question "+question);

							$.each(selectedStudents, function(i, l){
																
								if(selectedStudents[i]["completedAnnotations"][question-1])
								{
									//studentListHTML += '<div class="col-sm-4 col-md-2 student-label positive">'+selectedStudents[i]["name"]+'</div>';
									studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
								}
							});
						}
					  
						else if(selectedItem.column === 3 || selectedItem.column === 4)//Selected never annotated value
						{
							//alert('selected never annotated people for '+question);
							$("#modal-title").html("Students who have not answered Annotation Question "+question);

							$.each(selectedStudents, function(i, l){
																
								if(!selectedStudents[i]["completedAnnotations"][question-1])
								{
									//studentListHTML += '<div class="col-sm-4 col-md-2 student-label negative">'+selectedStudents[i]["name"]+'</div>';
									studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
								}
							});
						}
						
						studentListHTML+= '</div></div>';
					
						$("#modal-body").html(studentListHTML);
							
						$('#myModal').modal('toggle');
					}
				  }
				
				google.visualization.events.addListener(chart, 'select', selectHandler);
			}
			
			function setupIndividualStudent(status, current_student, HTML_string)
			{
				var studentListHTML = HTML_string;
				
				var has_login = '<span class="badge negative-bg">No</span>Logged In';
				var has_viewed = '<span class="badge negative-bg">No</span>Watched Full Video';
				var has_completed = '<span class="badge negative-bg">No</span>All Annotation Questions Answered';
				var has_annotated = '<span class="badge negative-bg">No</span>Made Annotation';
				var has_commented = '<span class="badge negative-bg">No</span>Posted Comment';
				var has_replied = '<span class="badge negative-bg">No</span>Posted Reply';
				
				if(current_student["login"] > 0)has_login = '<span class="badge positive-bg">Yes</span>Logged In';
				if(Object.keys(current_student["viewedVideos"]).length > 0)has_viewed = '<span class="badge positive-bg">Yes</span>Watched Full Video';
				if(current_student["completeAnnotations"] > 0)has_completed = '<span class="badge positive-bg">Yes</span>All Annotation Questions Answered';
				if(current_student["annotated"] > 0)has_annotated = '<span class="badge positive-bg">Yes</span>Made Annotation';
				if(current_student["commented"] > 0)has_commented = '<span class="badge positive-bg">Yes</span>Posted Comment';
				if(current_student["replied"] > 0)has_replied = '<span class="badge positive-bg">Yes</span>Posted Reply';
				
				studentListHTML += '<div class="col-sm-4 col-md-3 student-label '+status+' panel-group" id="accordion" role="tablist" aria-multiselectable="true">'+
														'<div class="panel-heading" role="tab" id="headingOne">'+
															'<h4 class="panel-title">'+
																'<a role="button" data-toggle="collapse" data-parent="#accordion" href=".collapseStudents" aria-expanded="false" aria-controls="collapseStudents">'+
																	current_student["name"]+
																'</a>'+
															'</h4>'+
														'</div>'+
														'<div class="panel-collapse collapseStudents collapse" role="tabpanel" aria-labelledby="headingOne">'+
															'<ul class="list-group">'+
																'<li class="list-group-item">'+
																	has_login+
																'</li>'+
																'<li class="list-group-item">'+
																	has_viewed+
																'</li>'+
																'<li class="list-group-item">'+
																	has_completed+
																'</li>'+
																'<li class="list-group-item">'+
																	has_annotated+
																'</li>'+
																'<li class="list-group-item">'+
																	has_commented+
																'</li>'+
																'<li class="list-group-item">'+
																	has_replied+
																'</li>'+
															'</ul>'+
														'</div>'+
													'</div>';
				
				return studentListHTML;
			}
			
			function selectHandler(chart, data) {
				var selection = chart.getSelection();
				if (selection.length) {
					
					var selectedValue = data.getColumnLabel(selection[0].column);
					var studentListHTML = "<div class='container-fluid'><div class='row'>";
					
					if(selection[0].column === 2 || selection[0].column === 4)//In the case where the user selects the annotation labels (Percentage labels)
					{
						selectedValue = data.getColumnLabel(selection[0].column-1);
					}
					
					//console.log("selectedValue: "+ selectedValue);
					//console.log("selection[0].column: "+selection[0].column);
					
					$.each(selectedStudents, function(i, l){
							
						if(selectedValue == "Logged In")
						{
							$("#modal-title").html("Students who have logged in");
							
							if(selectedStudents[i]["login"] === 1)
							{
								studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
							}
						}
						else if(selectedValue == "Never Logged In")
						{
							$("#modal-title").html("Students who have never logged in");

							if(selectedStudents[i]["login"] === 0)
							{
								studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
							}
						}
						
						else if(selectedValue == "Watched Video")
						{
							$("#modal-title").html("Students who finished watching video");
						
							if(selectedStudents[i]["viewedVideos"][selectedVideo["id"]])
							{
								studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
							}
						}
						
						else if(selectedValue == "Never Watched Video")
						{
							$("#modal-title").html("Students who never watched video");

							if(!selectedStudents[i]["viewedVideos"][selectedVideo])
							{
								studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
							}
						}
						
						else if(selectedValue == "Commented")
						{
							$("#modal-title").html("Students who commented");

							if(selectedStudents[i]["commented"] === 1)
							{
								studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
							}
						}
						
						else if(selectedValue == "Never Commented")
						{
							$("#modal-title").html("Students who never commented");
						
							if(selectedStudents[i]["commented"] === 0)
							{
								studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
							}
						}
						
						else if(selectedValue == "Replied")
						{
							$("#modal-title").html("Students who replied to comments");
							
							if(selectedStudents[i]["replied"] === 1)
							{
								studentListHTML = setupIndividualStudent("positive", selectedStudents[i], studentListHTML);
							}
						}
						
						else if(selectedValue == "Never Replied")
						{
							$("#modal-title").html("Students who never replied to comments");
							
							if(selectedStudents[i]["replied"] === 0)
							{
								studentListHTML = setupIndividualStudent("negative", selectedStudents[i], studentListHTML);
							}
						}	
					});
					
					studentListHTML+= '</div></div>';
					
					$("#modal-body").html(studentListHTML);
						
					$('#myModal').modal('toggle');
				}
			}
		</script>
	</body>
</html>