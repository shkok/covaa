<?php

require_once __DIR__ . '/google-api-php-client-2.1.1/vendor/autoload.php';
require_once(dirname(__FILE__) . '/includes/common.inc.php');
require_once(dirname(__FILE__) . "/database/participation.php");

$pIDs = array();
$pDB = new participationDB();
$key = 'My Project-857404b424b1.json';
 
// create the client object
$client = new Google_Client();
 
// use the service account for authorization
putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $key);
$client->useApplicationDefaultCredentials();
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
 
// create the service object
$analytics = new Google_Service_AnalyticsReporting($client);


$param = array('view_id'    => '135599848',
               'startDate'  => '2018-04-11',
               'endDate'    => '2018-04-11',
               'dimensions' => array('ga:dimension2' => 'user_id',
                                     'ga:dimension1' => 'time',
									 'ga:eventAction' => 'action',
									 'ga:eventLabel' => 'label'),
               'metrics'    => array('ga:sessionDuration' => 'session_duration'),
               'orderField' => 'ga:dimension1',
               'orderType'  => 'DESCENDING',
               'pageSize'   => 500);
 
$report = getReport($analytics, $param);


function getReport($analytics, $param) {
    /* set view_id - required */
    if (isset($param['view_id'] ))
         $view_id = $param['view_id'];
    else {
        echo 'Error: Google Analytics View ID must be set';
        return;
    }
 
    /* set date -required */
    $dateRange = new Google_Service_AnalyticsReporting_DateRange();
    if (isset($param['startDate']) && isset($param['endDate'])) {
        $dateRange->setStartDate($param['startDate']);
        $dateRange->setEndDate($param['endDate']);
    }
    else {
        echo 'Error: Google Analytics date range must be set';
        return;
    }
 
    /* Set dimensions - required */
    if (!isset($param['dimensions']) || empty ($param['dimensions'])) {
        echo 'Error: No dimensions have been set';
        return;
    }
    else {
        $dimensions = array();
        $dimension_names = array();
        foreach ($param['dimensions'] as $k => $v) {
            $dimension = new Google_Service_AnalyticsReporting_Dimension();
            $dimension->setName($k);
            $dimensions[] = $dimension;
            $dimension_names[] = $v;
        }
    }
 
    /* set metrics - required */
    if (!isset($param['metrics']) || empty ($param['metrics'])) {
        echo 'Error: No metrics have been set';
        return;
    }
    else {
        $metrics = array();
        $metric_names = array();
        foreach ($param['metrics'] as $k => $v) {
            $metric = new Google_Service_AnalyticsReporting_Metric();
            $metric->setExpression($k);
            $metric->setAlias($v);
            $metrics[] = $metric;
            $metric_names[] = $v;
        }
    }
 
    /* set dimension filter - optional */
    if (isset($param['dimFilter']) && isset($param['dimFilter']['dimensionName']) && isset($param['dimFilter']['operator']) && isset($param['dimFilter']['expression'])) {
        $filter = new Google_Service_AnalyticsReporting_DimensionFilter();
        $filter->setDimensionName($param['dimFilter']['dimensionName']);
        $filter->setOperator($param['dimFilter']['operator']);
        $filter->setExpressions($param['dimFilter']['expression']);
        if (isset($param['dimFilter']['exclude'] ))
            $filter->setNot($param['dimFilter']['exclude']);
        $filter_clause = new Google_Service_AnalyticsReporting_DimensionFilterClause();
        $filter_clause->setFilters($filter);
    }
 
    /* set order by - optional */
    if (isset($param['orderField'])) {
        $orderby = new Google_Service_AnalyticsReporting_OrderBy();
        $orderby->setFieldName($param['orderField']);
        if (isset( $param['orderType']))
            $orderby->setSortOrder($param['orderType']);
     }
 
 
 
 
    /* Create the ReportRequest object */
    $request = new Google_Service_AnalyticsReporting_ReportRequest();
    $request->setViewId($view_id);
    $request->setDateRanges($dateRange);
    $request->setDimensions($dimensions);
    $request->setMetrics($metrics);
    if (isset($orderby))
        $request->setOrderBys($orderby);
    if (isset($filter_clause))
         $request->setDimensionFilterClauses($filter_clause);
    if (isset($param['pageSize']))
        $request->setPageSize($param['pageSize']);
 
 
 
    /* set the request body */
    $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
 
 
 
 
    /* iterate to call the API and store the response data, 500 a time */
    $pageToken;
    $report = array();
     do {
         /* fire the request */
         $body->setReportRequests(array($request));
         $response = $analytics->reports->batchGet($body);
 
         /* get rows from the response */
         $rows = getRows($response, $report, $dimension_names);
		
         /* merge rows into the report */
         $report = array_merge($report, $rows);
		 
 
         /* set next page token to get the next 500 rows */
         $pageToken = $response[0]->getNextPageToken();
         $request->setPageToken($pageToken);
 
     } while ( $pageToken != null );
 
 
     return $report;
}

function getRows($response, $dimension_names) {
    /* get dimension headers and metric headers from the API response */
    for ($i = 0; $i < count($response); $i++) {
        $header = $response[$i]->getColumnHeader();
        $dimensionHeaders = $header->getDimensions();
        $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
        $rows = $response[$i]->getData()->getRows();
 
        /* iterate row by row */
        $report_rows = array();
        for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
            $row = $rows[$rowIndex];
            $dimensions = $row->getDimensions();
            $metrics = $row->getMetrics();
            $result_row = array();
 
            /* get dimension data */
            for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
				
               $result_row[$dimensionHeaders[$i]] = $dimensions[$i];
			  
            }
 
            /* get metric data */
            for ($i = 0; $i < count($metricHeaders); $i++) {
                $result_row[$metricHeaders[$i]->getName()] = $metrics[0]->getValues()[$i];
            }
 
            /* add the row to the report */
            $report_rows[] = $result_row;
        }
    }
	
    return $report_rows;
}
print_r ($report);
for ($d =0; $d < count($report); $d++)
{
	$userID = $report[$d]['ga:dimension2'];
	$timestamp = $report[$d]['ga:dimension1'];
	$action = $report[$d]['ga:eventAction'];
	$label = $report[$d]['ga:eventLabel'];
	$duration = $report[$d]['session_duration'];
	
	$msg = $pDB->addParticipation($userID ,$timestamp, $action, $label, $duration);
	//$pID = mysql_insert_id();

}

$annotationDB->close();
?>
