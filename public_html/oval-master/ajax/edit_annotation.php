<?php
/**
 *  OVAL (Online Video Annotation for Learning) is a video annotation tool
 *  that allows users to make annotations on videos.
 *
 *  Copyright (C) 2014  Shane Dawson, University of South Australia, Australia
 *  Copyright (C) 2014  An Zhao, University of South Australia, Australia
 *  Copyright (C) 2014  Dragan Gasevic, University of Edinburgh, United Kingdom
 *  Copyright (C) 2014  Negin Mirriahi, University of New South Wales, Australia
 *  Copyright (C) 2014  Abelardo Pardo, University of Sydney, Australia
 *  Copyright (C) 2014  Alan Kingstone, University of British Columbia, Canada
 *  Copyright (C) 2014  Thomas Dang, , University of British Columbia, Canada
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */


require_once(dirname(__FILE__) . '/../includes/common.inc.php');
require_once(dirname(__FILE__) . "/../database/annotations.php");


startSession();
$userID     = $_SESSION['user_id'];
$userName   = $_SESSION['name'];

//print "hello";
//print_r($_POST);
$annotationIDs = array();
$annotationDB = new annotationsDB();

//$annotations->truncateTable();
//$annotationsCount = $annotations->countAnnotations();
//print "There are $annotationsCount annotation(s) in total.<br /><br />";

$id                 = $_POST['id'];
$videoID            = $_POST['video_id'];
$qnsNo            	= $_POST['qnsNo'];
$startTime          = $_POST['start_time'];
$endTime            = $_POST['end_time'];
$tags               = $_POST['tags'];
$commentType	    = $_POST['commentType'];
$isPrivate          = $_POST['is_private'];
$description        = $_POST['description'];
$videoAnnotationID  = $_POST['video_annotation_id'];
("true" == $isPrivate) ? $isPrivate = 1 : $isPrivate = 0;

//Added for inserting as new submission upon update, instead of really update.
$isDeleted          = "0";
$confidence_type = $_POST['confidence_level'];
if(isset($_POST['parent_id']) && $_POST['parent_id'] != "")
	$parent_id = $_POST['parent_id'];
else
	$parent_id = "NULL";
$classID =  $_POST['classId'];
$groupID = $_POST['groupId'];
$endTime = 0; //Since recording is not used anymore, hardcode to 0
//End of adding new fields data

//echo "qnsNo b4 update: " . $qnsNo;

//Modified edit annotation/comment/reply to be a new insertion, so as to keep track of past submissions.
//First, using all edited data, insert into table as a new row
$annotationDB->addAnnotation($videoID, $userID, $userName, $qnsNo, $startTime, $endTime, $description, $tags, $commentType, $isPrivate, $isDeleted, $videoAnnotationID, $confidence_type, $parent_id, $classID, $groupID);
$newAnnotationID = mysql_insert_id();

//Then set the old annotation id_deleted to 1 and tag the new annotation ID for reference
$annotationDB->deleteAnnotation($id, $userID, $newAnnotationID);

//Finally, ensure that all existing replies to the comment points to the new comment ID
$annotationDB->updateParentID($newAnnotationID, $id);

/*$msg = $annotationDB->updateAnnotation($id, $videoID, $userID, $userName, $qnsNo, $startTime, $endTime, $description, $tags, $commentType, $isPrivate, 0, $videoAnnotationID, $parent_id);*/


$annotationDB->close();
?>
