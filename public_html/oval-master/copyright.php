<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>Video &amp; User Management</title>



<script type="text/javascript" src="kaltura-html5player-widget/jquery-1.4.2.min.js"></script>



<script type="text/javascript" src="http://gdata.youtube.com/feeds/api/videos/<?php echo $description?>?v=2&alt=jsonc&callback=youtubeFeedCallback&prettyprint=true"></script>
    <link rel="stylesheet" type="text/css" href="style.css" />

    <link rel="stylesheet" type="text/css" href="admin-page.css" />

</head>



<body>

<div id="wrapper">

<h2>Copyright</h2>

As a University staff member or student you are responsible for ensuring you comply with copyright. Staff and students who infringe copyright using the University’s facilities and networks may face disciplinary action.</br>
</br>

When uploading content sourced from the internet to your course online site, please ensure that you:</br>
</br>

1.	Source your content from reputable sites;</br> 
2.	 Link to content rather than copy it; and</br>
3.	 Familiarise yourself with the Terms and Conditions of the web site you are copying the content from.</br>
</br>

Content sourced from the web is more likely to be legitimate if:</br>
</br>

1.	The content been uploaded by the copyright owner; <b>and</b></br>
2.	There is a link back to the original source website of the material.

</div>
</body>
</html>
