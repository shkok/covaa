<?php
/** Template Name: Auth Page
 * The template for authentication before starting COVAA
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

//get_header();
?>
<?php

$newdb = new wpdb('covaa21_CLAS', 'covaa@nie5b367', 'covaa21_CLAS', 'localhost');
$newdb->show_errors();
	//error_reporting(E_ALL ^ E_WARNING);

	if(!is_user_logged_in()) {
		auth_redirect();
	}
	else
	{
		$current_user = wp_get_current_user();
		
		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		}
	
		// Set session variables
		$_SESSION["name"] = $current_user->first_name;
		$_SESSION["user_id"] = $current_user->ID;

		if(current_user_can( "create_users" )) {
			$role = 5; //Admins (Sysadmin)
		}
		else if(current_user_can( "moderate_comments" )) {
			$role = 2; //Editors (TA)
		}
		else if(current_user_can( "edit_published_posts" )) {
			$role = 3; //Authors (Instructor)
		}
		else if(current_user_can( "edit_posts" )) {
			$role = 4; //Contributors (ALL)
		}
		else {
			$role = 1; //Subscribers (Student)
		}

		$_SESSION["role"] = $role;
		
		if($_SESSION["role"] ==1 && ($_SESSION["user_id"] <381 || $_SESSION["user_id"] > 390 ))
		{
			$rows = $newdb->get_results("select * from surveyData WHERE user_login =".$current_user->ID);
			
			if(empty($rows))
			{
				//wp_redirect(home_url().'/oval-master/survey.php');
				wp_redirect(home_url().'/oval-master');
				exit;
			}
			else
			{
				wp_redirect(home_url().'/oval-master');
				exit;
			}
		}
		elseif($_SESSION["user_id"] >=381 && $_SESSION["user_id"] <= 390)
		{
			wp_redirect(home_url().'/oval-master/index_t2.php');
			exit;
		}
		else
		{
			wp_redirect(home_url().'/oval-master');
			exit;
		}
		//echo "Session Variables set - </br>Name: " . $_SESSION["name"] . "</br>ID: " . $_SESSION["user_id"] . "</br>Role: " . $_SESSION["role"];
		//Self Note: echo before wp_redirect will cause header already sent issue and stop redirect.	
	}
?>

<?php //get_footer(); ?>