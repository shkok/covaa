<?php
/** Template Name: Logout Page
 * The template for logging out of COVAA
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

error_reporting(E_ALL);

//get_header();
?>
<?php
	if (session_status() == PHP_SESSION_ACTIVE) {
		   session_destroy();
	}
	
	//echo "Before logout: user_id: " . $_SESSION["user_id"];

	wp_logout();
	
	echo "<div>You have successfully logged out.</div>";

	if (session_status() == PHP_SESSION_ACTIVE) {
		echo "Session still active! : " . $_SESSION["user_id"];
	}

	//echo "</br>After logout: user_id: " . $_SESSION["user_id"];
	wp_redirect('http://www.covaa21.com');
	exit;
	
?>

<?php get_footer(); ?>
