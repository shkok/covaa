<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'covaa21_covaawp');

/** MySQL database username */
define('DB_USER', 'covaa21_covaawp');

/** MySQL database password */
define('DB_PASSWORD', '3PS6)!nuJ1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hgpzbtbv6nu9c3a37cgzkzkysygikrg78p3mwhjhknfddkq3ew9xl7ee3dnsbx6r');
define('SECURE_AUTH_KEY',  'nekkr4mfweun5ybimby0dmhjkwoaxr89jvwl3fsvnduu61xw5ng3kcwynoc6zywt');
define('LOGGED_IN_KEY',    'lkccnsv4gkkzbb4ztqwecbv4opsyy3ozqzcduxwqciajyxxghhdwn4nsfizy9fia');
define('NONCE_KEY',        'z96k8jkvehw3uj2t1xkricaalmdhoee4fcoauhf8s6pzewfpb2ngcsgdyadilon4');
define('AUTH_SALT',        '5mqjx8qboidgcpcvdnv4jcbymyepxr4lhocx0gtgqaqaepkouthpm3s0ws03zz0k');
define('SECURE_AUTH_SALT', 'ugxnomdpqvbo4kglpwg6lyu4kbk1zjrj7viyndnhyhqfpapdnioxpksxiijeicf8');
define('LOGGED_IN_SALT',   'rzjnj7owqjcbyuiuiibxrn6rmyxqsexdmtygfgzu1aew3rmad6mpdqjp2lqyx22f');
define('NONCE_SALT',       '9r1xxbqy10zavajsu75fbfkgdeoh4kpmvqmlyqs1xix0i24cgxtco1cvsske3wq6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT', '128M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

# Disables all core updates. Added by SiteGround Autoupdate:
define( 'WP_AUTO_UPDATE_CORE', false );
